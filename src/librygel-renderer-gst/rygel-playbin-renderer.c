/* rygel-playbin-renderer.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-playbin-renderer.vala, do not modify */

/*
 * Copyright (C) 2012 Openismus GmbH.
 * Copyright (C) 2012 Intel Corporation.
 *
 * Author: Jens Georg <jensg@openismus.com>
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <rygel-renderer.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <gst/gst.h>

#define RYGEL_PLAYBIN_TYPE_RENDERER (rygel_playbin_renderer_get_type ())
#define RYGEL_PLAYBIN_RENDERER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_PLAYBIN_TYPE_RENDERER, RygelPlaybinRenderer))
#define RYGEL_PLAYBIN_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_PLAYBIN_TYPE_RENDERER, RygelPlaybinRendererClass))
#define RYGEL_PLAYBIN_IS_RENDERER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_PLAYBIN_TYPE_RENDERER))
#define RYGEL_PLAYBIN_IS_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_PLAYBIN_TYPE_RENDERER))
#define RYGEL_PLAYBIN_RENDERER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_PLAYBIN_TYPE_RENDERER, RygelPlaybinRendererClass))

typedef struct _RygelPlaybinRenderer RygelPlaybinRenderer;
typedef struct _RygelPlaybinRendererClass RygelPlaybinRendererClass;
typedef struct _RygelPlaybinRendererPrivate RygelPlaybinRendererPrivate;
enum  {
	RYGEL_PLAYBIN_RENDERER_0_PROPERTY,
	RYGEL_PLAYBIN_RENDERER_NUM_PROPERTIES
};
static GParamSpec* rygel_playbin_renderer_properties[RYGEL_PLAYBIN_RENDERER_NUM_PROPERTIES];

#define RYGEL_PLAYBIN_TYPE_PLAYER (rygel_playbin_player_get_type ())
#define RYGEL_PLAYBIN_PLAYER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_PLAYBIN_TYPE_PLAYER, RygelPlaybinPlayer))
#define RYGEL_PLAYBIN_PLAYER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_PLAYBIN_TYPE_PLAYER, RygelPlaybinPlayerClass))
#define RYGEL_PLAYBIN_IS_PLAYER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_PLAYBIN_TYPE_PLAYER))
#define RYGEL_PLAYBIN_IS_PLAYER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_PLAYBIN_TYPE_PLAYER))
#define RYGEL_PLAYBIN_PLAYER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_PLAYBIN_TYPE_PLAYER, RygelPlaybinPlayerClass))

typedef struct _RygelPlaybinPlayer RygelPlaybinPlayer;
typedef struct _RygelPlaybinPlayerClass RygelPlaybinPlayerClass;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))

struct _RygelPlaybinRenderer {
	RygelMediaRenderer parent_instance;
	RygelPlaybinRendererPrivate * priv;
};

struct _RygelPlaybinRendererClass {
	RygelMediaRendererClass parent_class;
};

static gpointer rygel_playbin_renderer_parent_class = NULL;

GType rygel_playbin_renderer_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelPlaybinRenderer, g_object_unref)
RygelPlaybinRenderer* rygel_playbin_renderer_new (const gchar* title);
RygelPlaybinRenderer* rygel_playbin_renderer_construct (GType object_type,
                                                        const gchar* title);
GType rygel_playbin_player_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelPlaybinPlayer, g_object_unref)
RygelPlaybinPlayer* rygel_playbin_player_instance (GError** error);
GstElement* rygel_playbin_renderer_get_playbin (RygelPlaybinRenderer* self);
GstElement* rygel_playbin_player_get_playbin (RygelPlaybinPlayer* self);

/**
     * Create a new instance of Renderer.
     *
     * Renderer will instantiate its own instance of GstPlayBin.
     * The GstPlayBin can be accessed by using rygel_playbin_player_get_playbin().
     *
     * @param title Friendly name of the new UPnP renderer on the network.
     */
RygelPlaybinRenderer*
rygel_playbin_renderer_construct (GType object_type,
                                  const gchar* title)
{
	RygelPlaybinRenderer * self = NULL;
	GError* _inner_error0_ = NULL;
	g_return_val_if_fail (title != NULL, NULL);
	{
		RygelPlaybinPlayer* _tmp0_ = NULL;
		RygelPlaybinPlayer* _tmp1_;
		_tmp1_ = rygel_playbin_player_instance (&_inner_error0_);
		_tmp0_ = _tmp1_;
		if (G_UNLIKELY (_inner_error0_ != NULL)) {
			goto __catch4_g_error;
		}
		self = (RygelPlaybinRenderer*) g_object_new (object_type, "title", title, "player", _tmp0_, NULL);
		_g_object_unref0 (_tmp0_);
	}
	goto __finally4;
	__catch4_g_error:
	{
		GError* _error_ = NULL;
		GError* _tmp2_;
		const gchar* _tmp3_;
		_error_ = _inner_error0_;
		_inner_error0_ = NULL;
		_tmp2_ = _error_;
		_tmp3_ = _tmp2_->message;
		g_warning ("rygel-playbin-renderer.vala:57: %s", _tmp3_);
		g_return_val_if_fail (FALSE, NULL);
		_g_error_free0 (_error_);
	}
	__finally4:
	if (G_UNLIKELY (_inner_error0_ != NULL)) {
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
		g_clear_error (&_inner_error0_);
		return NULL;
	}
	return self;
}

RygelPlaybinRenderer*
rygel_playbin_renderer_new (const gchar* title)
{
	return rygel_playbin_renderer_construct (RYGEL_PLAYBIN_TYPE_RENDERER, title);
}

/**
     * Get the GstPlayBin used by this Renderer.
     */
static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

GstElement*
rygel_playbin_renderer_get_playbin (RygelPlaybinRenderer* self)
{
	GError* _inner_error0_ = NULL;
	GstElement* result = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	{
		RygelPlaybinPlayer* player = NULL;
		RygelPlaybinPlayer* _tmp0_;
		GstElement* _tmp1_;
		GstElement* _tmp2_;
		GstElement* _tmp3_;
		_tmp0_ = rygel_playbin_player_instance (&_inner_error0_);
		player = _tmp0_;
		if (G_UNLIKELY (_inner_error0_ != NULL)) {
			goto __catch5_g_error;
		}
		_tmp1_ = rygel_playbin_player_get_playbin (player);
		_tmp2_ = _tmp1_;
		_tmp3_ = _g_object_ref0 (_tmp2_);
		result = _tmp3_;
		_g_object_unref0 (player);
		return result;
	}
	goto __finally5;
	__catch5_g_error:
	{
		GError* _error_ = NULL;
		GError* _tmp4_;
		const gchar* _tmp5_;
		_error_ = _inner_error0_;
		_inner_error0_ = NULL;
		_tmp4_ = _error_;
		_tmp5_ = _tmp4_->message;
		g_warning ("rygel-playbin-renderer.vala:72: %s", _tmp5_);
		result = NULL;
		_g_error_free0 (_error_);
		return result;
	}
	__finally5:
	g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
	g_clear_error (&_inner_error0_);
	return NULL;
}

static void
rygel_playbin_renderer_class_init (RygelPlaybinRendererClass * klass,
                                   gpointer klass_data)
{
	rygel_playbin_renderer_parent_class = g_type_class_peek_parent (klass);
}

static void
rygel_playbin_renderer_instance_init (RygelPlaybinRenderer * self,
                                      gpointer klass)
{
}

/**
 * An in-process UPnP renderer that uses a GStreamer Playbin element.
 *
 * Using GstPlayBin as a model, it reflects any changes done externally, such as
 * changing the currently played URI, volume, pause/play etc., to UPnP.
 *
 * Likewise, the playbin can be modified externally using UPnP.
 *
 * You can retrieve the GstPlayBin by calling rygel_playbin_renderer_get_playbin().
 * You should then set the "video-sink" and "audio-sink" properties of the
 * playbin.
 *
 * Call rygel_media_device_add_interface() on the Renderer to allow it
 * to be controlled by a control point and to retrieve data streams via that
 * network interface.
 *
 * See the <link linkend="implementing-renderers-gst">Implementing GStreamer-based Renderers</link> section.
 */
GType
rygel_playbin_renderer_get_type (void)
{
	static volatile gsize rygel_playbin_renderer_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_playbin_renderer_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelPlaybinRendererClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_playbin_renderer_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelPlaybinRenderer), 0, (GInstanceInitFunc) rygel_playbin_renderer_instance_init, NULL };
		GType rygel_playbin_renderer_type_id;
		rygel_playbin_renderer_type_id = g_type_register_static (RYGEL_TYPE_MEDIA_RENDERER, "RygelPlaybinRenderer", &g_define_type_info, 0);
		g_once_init_leave (&rygel_playbin_renderer_type_id__volatile, rygel_playbin_renderer_type_id);
	}
	return rygel_playbin_renderer_type_id__volatile;
}

