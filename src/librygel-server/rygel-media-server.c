/* rygel-media-server.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-media-server.vala, do not modify */

/*
 * Copyright (C) 2012 Openismus GmbH.
 * Copyright (C) 2012 Intel Corporation.
 *
 * Author: Jens Georg <jensg@openismus.com>
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <rygel-core.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#define RYGEL_TYPE_MEDIA_SERVER_PLUGIN (rygel_media_server_plugin_get_type ())
#define RYGEL_MEDIA_SERVER_PLUGIN(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_SERVER_PLUGIN, RygelMediaServerPlugin))
#define RYGEL_MEDIA_SERVER_PLUGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_SERVER_PLUGIN, RygelMediaServerPluginClass))
#define RYGEL_IS_MEDIA_SERVER_PLUGIN(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_SERVER_PLUGIN))
#define RYGEL_IS_MEDIA_SERVER_PLUGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_SERVER_PLUGIN))
#define RYGEL_MEDIA_SERVER_PLUGIN_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_SERVER_PLUGIN, RygelMediaServerPluginClass))

typedef struct _RygelMediaServerPlugin RygelMediaServerPlugin;
typedef struct _RygelMediaServerPluginClass RygelMediaServerPluginClass;
typedef struct _RygelMediaServerPluginPrivate RygelMediaServerPluginPrivate;

#define TYPE_PLUGIN (plugin_get_type ())
#define PLUGIN(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_PLUGIN, Plugin))
#define PLUGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_PLUGIN, PluginClass))
#define IS_PLUGIN(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_PLUGIN))
#define IS_PLUGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_PLUGIN))
#define PLUGIN_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_PLUGIN, PluginClass))

typedef struct _Plugin Plugin;
typedef struct _PluginClass PluginClass;
typedef struct _PluginPrivate PluginPrivate;
enum  {
	PLUGIN_0_PROPERTY,
	PLUGIN_NUM_PROPERTIES
};
static GParamSpec* plugin_properties[PLUGIN_NUM_PROPERTIES];

#define RYGEL_TYPE_MEDIA_OBJECT (rygel_media_object_get_type ())
#define RYGEL_MEDIA_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObject))
#define RYGEL_MEDIA_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObjectClass))
#define RYGEL_IS_MEDIA_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_OBJECT))
#define RYGEL_IS_MEDIA_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_OBJECT))
#define RYGEL_MEDIA_OBJECT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObjectClass))

typedef struct _RygelMediaObject RygelMediaObject;
typedef struct _RygelMediaObjectClass RygelMediaObjectClass;

#define RYGEL_TYPE_MEDIA_CONTAINER (rygel_media_container_get_type ())
#define RYGEL_MEDIA_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainer))
#define RYGEL_MEDIA_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainerClass))
#define RYGEL_IS_MEDIA_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_CONTAINER))
#define RYGEL_IS_MEDIA_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_CONTAINER))
#define RYGEL_MEDIA_CONTAINER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainerClass))

typedef struct _RygelMediaContainer RygelMediaContainer;
typedef struct _RygelMediaContainerClass RygelMediaContainerClass;

#define RYGEL_TYPE_MEDIA_SERVER (rygel_media_server_get_type ())
#define RYGEL_MEDIA_SERVER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_SERVER, RygelMediaServer))
#define RYGEL_MEDIA_SERVER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_SERVER, RygelMediaServerClass))
#define RYGEL_IS_MEDIA_SERVER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_SERVER))
#define RYGEL_IS_MEDIA_SERVER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_SERVER))
#define RYGEL_MEDIA_SERVER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_SERVER, RygelMediaServerClass))

typedef struct _RygelMediaServer RygelMediaServer;
typedef struct _RygelMediaServerClass RygelMediaServerClass;
typedef struct _RygelMediaServerPrivate RygelMediaServerPrivate;
enum  {
	RYGEL_MEDIA_SERVER_0_PROPERTY,
	RYGEL_MEDIA_SERVER_ROOT_CONTAINER_PROPERTY,
	RYGEL_MEDIA_SERVER_NUM_PROPERTIES
};
static GParamSpec* rygel_media_server_properties[RYGEL_MEDIA_SERVER_NUM_PROPERTIES];
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

struct _RygelMediaServerPlugin {
	RygelPlugin parent_instance;
	RygelMediaServerPluginPrivate * priv;
};

struct _RygelMediaServerPluginClass {
	RygelPluginClass parent_class;
	const gchar* (*get_search_caps) (RygelMediaServerPlugin* self);
};

struct _Plugin {
	RygelMediaServerPlugin parent_instance;
	PluginPrivate * priv;
};

struct _PluginClass {
	RygelMediaServerPluginClass parent_class;
};

struct _RygelMediaServer {
	RygelMediaDevice parent_instance;
	RygelMediaServerPrivate * priv;
};

struct _RygelMediaServerClass {
	RygelMediaDeviceClass parent_class;
};

struct _RygelMediaServerPrivate {
	RygelMediaContainer* _root_container;
};

static gpointer plugin_parent_class = NULL;
static gint RygelMediaServer_private_offset;
static gpointer rygel_media_server_parent_class = NULL;

GType rygel_media_server_plugin_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaServerPlugin, g_object_unref)
GType plugin_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (Plugin, g_object_unref)
GType rygel_media_object_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaObject, g_object_unref)
GType rygel_media_container_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaContainer, g_object_unref)
Plugin* plugin_new (RygelMediaContainer* root_container,
                    RygelPluginCapabilities capabilities);
Plugin* plugin_construct (GType object_type,
                          RygelMediaContainer* root_container,
                          RygelPluginCapabilities capabilities);
RygelMediaServerPlugin* rygel_media_server_plugin_construct (GType object_type,
                                                             RygelMediaContainer* root_container,
                                                             const gchar* name,
                                                             const gchar* description,
                                                             RygelPluginCapabilities capabilities);
GType rygel_media_server_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaServer, g_object_unref)
RygelMediaServer* rygel_media_server_new (const gchar* title,
                                          RygelMediaContainer* root_container,
                                          RygelPluginCapabilities capabilities);
RygelMediaServer* rygel_media_server_construct (GType object_type,
                                                const gchar* title,
                                                RygelMediaContainer* root_container,
                                                RygelPluginCapabilities capabilities);
static void rygel_media_server_real_constructed (GObject* base);
static RygelMediaContainer* rygel_media_server_get_root_container (RygelMediaServer* self);
static void rygel_media_server_set_root_container (RygelMediaServer* self,
                                            RygelMediaContainer* value);
static void rygel_media_server_finalize (GObject * obj);
static void _vala_rygel_media_server_get_property (GObject * object,
                                            guint property_id,
                                            GValue * value,
                                            GParamSpec * pspec);
static void _vala_rygel_media_server_set_property (GObject * object,
                                            guint property_id,
                                            const GValue * value,
                                            GParamSpec * pspec);

Plugin*
plugin_construct (GType object_type,
                  RygelMediaContainer* root_container,
                  RygelPluginCapabilities capabilities)
{
	Plugin * self = NULL;
	g_return_val_if_fail (root_container != NULL, NULL);
	self = (Plugin*) rygel_media_server_plugin_construct (object_type, root_container, "LibRygelServer", NULL, capabilities);
	return self;
}

Plugin*
plugin_new (RygelMediaContainer* root_container,
            RygelPluginCapabilities capabilities)
{
	return plugin_construct (TYPE_PLUGIN, root_container, capabilities);
}

static void
plugin_class_init (PluginClass * klass,
                   gpointer klass_data)
{
	plugin_parent_class = g_type_class_peek_parent (klass);
}

static void
plugin_instance_init (Plugin * self,
                      gpointer klass)
{
}

GType
plugin_get_type (void)
{
	static volatile gsize plugin_type_id__volatile = 0;
	if (g_once_init_enter (&plugin_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (PluginClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) plugin_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (Plugin), 0, (GInstanceInitFunc) plugin_instance_init, NULL };
		GType plugin_type_id;
		plugin_type_id = g_type_register_static (RYGEL_TYPE_MEDIA_SERVER_PLUGIN, "Plugin", &g_define_type_info, 0);
		g_once_init_leave (&plugin_type_id__volatile, plugin_type_id);
	}
	return plugin_type_id__volatile;
}

static inline gpointer
rygel_media_server_get_instance_private (RygelMediaServer* self)
{
	return G_STRUCT_MEMBER_P (self, RygelMediaServer_private_offset);
}

/**
     * Create a MediaServer to serve the media in the RygelMediaContainer.
     * For instance, you might use a RygelSimpleContainer. Alternatively,
     * you might use your own RygelMediaContainer implementation.
     *
     * Assuming that the RygelMediaContainer is correctly implemented,
     * the RygelMediaServer will respond appropriately to changes in the
     * RygelMediaContainer. 
     */
RygelMediaServer*
rygel_media_server_construct (GType object_type,
                              const gchar* title,
                              RygelMediaContainer* root_container,
                              RygelPluginCapabilities capabilities)
{
	RygelMediaServer * self = NULL;
	g_return_val_if_fail (title != NULL, NULL);
	g_return_val_if_fail (root_container != NULL, NULL);
	self = (RygelMediaServer*) g_object_new (object_type, "title", title, "root-container", root_container, "capabilities", capabilities, NULL);
	return self;
}

RygelMediaServer*
rygel_media_server_new (const gchar* title,
                        RygelMediaContainer* root_container,
                        RygelPluginCapabilities capabilities)
{
	return rygel_media_server_construct (RYGEL_TYPE_MEDIA_SERVER, title, root_container, capabilities);
}

static void
rygel_media_server_real_constructed (GObject* base)
{
	RygelMediaServer * self;
	RygelPlugin* _tmp0_;
	RygelPlugin* _tmp1_;
	RygelPlugin* _tmp7_;
	RygelPlugin* _tmp8_;
	const gchar* _tmp9_;
	const gchar* _tmp10_;
	self = (RygelMediaServer*) base;
	G_OBJECT_CLASS (rygel_media_server_parent_class)->constructed ((GObject*) G_TYPE_CHECK_INSTANCE_CAST (self, RYGEL_TYPE_MEDIA_DEVICE, RygelMediaDevice));
	_tmp0_ = rygel_media_device_get_plugin ((RygelMediaDevice*) self);
	_tmp1_ = _tmp0_;
	if (_tmp1_ == NULL) {
		RygelMediaContainer* _tmp2_;
		RygelPluginCapabilities _tmp3_;
		RygelPluginCapabilities _tmp4_;
		Plugin* _tmp5_;
		Plugin* _tmp6_;
		_tmp2_ = self->priv->_root_container;
		_tmp3_ = rygel_media_device_get_capabilities ((RygelMediaDevice*) self);
		_tmp4_ = _tmp3_;
		_tmp5_ = plugin_new (_tmp2_, _tmp4_);
		_tmp6_ = _tmp5_;
		rygel_media_device_set_plugin ((RygelMediaDevice*) self, (RygelPlugin*) _tmp6_);
		_g_object_unref0 (_tmp6_);
	}
	_tmp7_ = rygel_media_device_get_plugin ((RygelMediaDevice*) self);
	_tmp8_ = _tmp7_;
	_tmp9_ = rygel_media_device_get_title ((RygelMediaDevice*) self);
	_tmp10_ = _tmp9_;
	rygel_plugin_set_title (_tmp8_, _tmp10_);
}

static RygelMediaContainer*
rygel_media_server_get_root_container (RygelMediaServer* self)
{
	RygelMediaContainer* result;
	RygelMediaContainer* _tmp0_;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->_root_container;
	result = _tmp0_;
	return result;
}

static void
rygel_media_server_set_root_container (RygelMediaServer* self,
                                       RygelMediaContainer* value)
{
	g_return_if_fail (self != NULL);
	if (rygel_media_server_get_root_container (self) != value) {
		self->priv->_root_container = value;
		g_object_notify_by_pspec ((GObject *) self, rygel_media_server_properties[RYGEL_MEDIA_SERVER_ROOT_CONTAINER_PROPERTY]);
	}
}

static void
rygel_media_server_class_init (RygelMediaServerClass * klass,
                               gpointer klass_data)
{
	rygel_media_server_parent_class = g_type_class_peek_parent (klass);
	g_type_class_adjust_private_offset (klass, &RygelMediaServer_private_offset);
	((GObjectClass *) klass)->constructed = (void (*) (GObject*)) rygel_media_server_real_constructed;
	G_OBJECT_CLASS (klass)->get_property = _vala_rygel_media_server_get_property;
	G_OBJECT_CLASS (klass)->set_property = _vala_rygel_media_server_set_property;
	G_OBJECT_CLASS (klass)->finalize = rygel_media_server_finalize;
	g_object_class_install_property (G_OBJECT_CLASS (klass), RYGEL_MEDIA_SERVER_ROOT_CONTAINER_PROPERTY, rygel_media_server_properties[RYGEL_MEDIA_SERVER_ROOT_CONTAINER_PROPERTY] = g_param_spec_object ("root-container", "root-container", "root-container", RYGEL_TYPE_MEDIA_CONTAINER, G_PARAM_STATIC_STRINGS | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
}

static void
rygel_media_server_instance_init (RygelMediaServer * self,
                                  gpointer klass)
{
	self->priv = rygel_media_server_get_instance_private (self);
}

static void
rygel_media_server_finalize (GObject * obj)
{
	RygelMediaServer * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, RYGEL_TYPE_MEDIA_SERVER, RygelMediaServer);
	G_OBJECT_CLASS (rygel_media_server_parent_class)->finalize (obj);
}

/**
 * This class may be used to implement in-process UPnP-AV media servers.
 *
 * Call rygel_media_device_add_interface() on the RygelMediaServer to allow it
 * to serve media via that network interface.
 *
 * See the
 * <link linkend="implementing-servers">Implementing Servers</link> section.
 */
GType
rygel_media_server_get_type (void)
{
	static volatile gsize rygel_media_server_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_media_server_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelMediaServerClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_media_server_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelMediaServer), 0, (GInstanceInitFunc) rygel_media_server_instance_init, NULL };
		GType rygel_media_server_type_id;
		rygel_media_server_type_id = g_type_register_static (RYGEL_TYPE_MEDIA_DEVICE, "RygelMediaServer", &g_define_type_info, 0);
		RygelMediaServer_private_offset = g_type_add_instance_private (rygel_media_server_type_id, sizeof (RygelMediaServerPrivate));
		g_once_init_leave (&rygel_media_server_type_id__volatile, rygel_media_server_type_id);
	}
	return rygel_media_server_type_id__volatile;
}

static void
_vala_rygel_media_server_get_property (GObject * object,
                                       guint property_id,
                                       GValue * value,
                                       GParamSpec * pspec)
{
	RygelMediaServer * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (object, RYGEL_TYPE_MEDIA_SERVER, RygelMediaServer);
	switch (property_id) {
		case RYGEL_MEDIA_SERVER_ROOT_CONTAINER_PROPERTY:
		g_value_set_object (value, rygel_media_server_get_root_container (self));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
_vala_rygel_media_server_set_property (GObject * object,
                                       guint property_id,
                                       const GValue * value,
                                       GParamSpec * pspec)
{
	RygelMediaServer * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (object, RYGEL_TYPE_MEDIA_SERVER, RygelMediaServer);
	switch (property_id) {
		case RYGEL_MEDIA_SERVER_ROOT_CONTAINER_PROPERTY:
		rygel_media_server_set_root_container (self, g_value_get_object (value));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

