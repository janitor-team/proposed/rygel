/* rygel-samsung-tv-hacks.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-samsung-tv-hacks.vala, do not modify */

/*
 * Copyright (C) 2012 Choe Hwanjin <choe.hwanjin@gmail.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <gio/gio.h>
#include <gee.h>
#include <libsoup/soup.h>
#include <rygel-core.h>

#define RYGEL_TYPE_CLIENT_HACKS (rygel_client_hacks_get_type ())
#define RYGEL_CLIENT_HACKS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_CLIENT_HACKS, RygelClientHacks))
#define RYGEL_CLIENT_HACKS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_CLIENT_HACKS, RygelClientHacksClass))
#define RYGEL_IS_CLIENT_HACKS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_CLIENT_HACKS))
#define RYGEL_IS_CLIENT_HACKS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_CLIENT_HACKS))
#define RYGEL_CLIENT_HACKS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_CLIENT_HACKS, RygelClientHacksClass))

typedef struct _RygelClientHacks RygelClientHacks;
typedef struct _RygelClientHacksClass RygelClientHacksClass;
typedef struct _RygelClientHacksPrivate RygelClientHacksPrivate;

#define RYGEL_TYPE_MEDIA_QUERY_ACTION (rygel_media_query_action_get_type ())
#define RYGEL_MEDIA_QUERY_ACTION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_QUERY_ACTION, RygelMediaQueryAction))
#define RYGEL_MEDIA_QUERY_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_QUERY_ACTION, RygelMediaQueryActionClass))
#define RYGEL_IS_MEDIA_QUERY_ACTION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_QUERY_ACTION))
#define RYGEL_IS_MEDIA_QUERY_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_QUERY_ACTION))
#define RYGEL_MEDIA_QUERY_ACTION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_QUERY_ACTION, RygelMediaQueryActionClass))

typedef struct _RygelMediaQueryAction RygelMediaQueryAction;
typedef struct _RygelMediaQueryActionClass RygelMediaQueryActionClass;

#define RYGEL_TYPE_MEDIA_OBJECT (rygel_media_object_get_type ())
#define RYGEL_MEDIA_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObject))
#define RYGEL_MEDIA_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObjectClass))
#define RYGEL_IS_MEDIA_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_OBJECT))
#define RYGEL_IS_MEDIA_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_OBJECT))
#define RYGEL_MEDIA_OBJECT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObjectClass))

typedef struct _RygelMediaObject RygelMediaObject;
typedef struct _RygelMediaObjectClass RygelMediaObjectClass;

#define RYGEL_TYPE_HTTP_REQUEST (rygel_http_request_get_type ())
#define RYGEL_HTTP_REQUEST(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_HTTP_REQUEST, RygelHTTPRequest))
#define RYGEL_HTTP_REQUEST_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_HTTP_REQUEST, RygelHTTPRequestClass))
#define RYGEL_IS_HTTP_REQUEST(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_HTTP_REQUEST))
#define RYGEL_IS_HTTP_REQUEST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_HTTP_REQUEST))
#define RYGEL_HTTP_REQUEST_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_HTTP_REQUEST, RygelHTTPRequestClass))

typedef struct _RygelHTTPRequest RygelHTTPRequest;
typedef struct _RygelHTTPRequestClass RygelHTTPRequestClass;

#define RYGEL_TYPE_SEARCHABLE_CONTAINER (rygel_searchable_container_get_type ())
#define RYGEL_SEARCHABLE_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_SEARCHABLE_CONTAINER, RygelSearchableContainer))
#define RYGEL_IS_SEARCHABLE_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_SEARCHABLE_CONTAINER))
#define RYGEL_SEARCHABLE_CONTAINER_GET_INTERFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), RYGEL_TYPE_SEARCHABLE_CONTAINER, RygelSearchableContainerIface))

typedef struct _RygelSearchableContainer RygelSearchableContainer;
typedef struct _RygelSearchableContainerIface RygelSearchableContainerIface;

#define RYGEL_TYPE_MEDIA_CONTAINER (rygel_media_container_get_type ())
#define RYGEL_MEDIA_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainer))
#define RYGEL_MEDIA_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainerClass))
#define RYGEL_IS_MEDIA_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_CONTAINER))
#define RYGEL_IS_MEDIA_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_CONTAINER))
#define RYGEL_MEDIA_CONTAINER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainerClass))

typedef struct _RygelMediaContainer RygelMediaContainer;
typedef struct _RygelMediaContainerClass RygelMediaContainerClass;

#define RYGEL_TYPE_SEARCH_EXPRESSION (rygel_search_expression_get_type ())
#define RYGEL_SEARCH_EXPRESSION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_SEARCH_EXPRESSION, RygelSearchExpression))
#define RYGEL_SEARCH_EXPRESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_SEARCH_EXPRESSION, RygelSearchExpressionClass))
#define RYGEL_IS_SEARCH_EXPRESSION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_SEARCH_EXPRESSION))
#define RYGEL_IS_SEARCH_EXPRESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_SEARCH_EXPRESSION))
#define RYGEL_SEARCH_EXPRESSION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_SEARCH_EXPRESSION, RygelSearchExpressionClass))

typedef struct _RygelSearchExpression RygelSearchExpression;
typedef struct _RygelSearchExpressionClass RygelSearchExpressionClass;

#define RYGEL_TYPE_MEDIA_OBJECTS (rygel_media_objects_get_type ())
#define RYGEL_MEDIA_OBJECTS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_OBJECTS, RygelMediaObjects))
#define RYGEL_MEDIA_OBJECTS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_OBJECTS, RygelMediaObjectsClass))
#define RYGEL_IS_MEDIA_OBJECTS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_OBJECTS))
#define RYGEL_IS_MEDIA_OBJECTS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_OBJECTS))
#define RYGEL_MEDIA_OBJECTS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_OBJECTS, RygelMediaObjectsClass))

typedef struct _RygelMediaObjects RygelMediaObjects;
typedef struct _RygelMediaObjectsClass RygelMediaObjectsClass;

#define RYGEL_TYPE_SAMSUNG_TV_HACKS (rygel_samsung_tv_hacks_get_type ())
#define RYGEL_SAMSUNG_TV_HACKS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_SAMSUNG_TV_HACKS, RygelSamsungTVHacks))
#define RYGEL_SAMSUNG_TV_HACKS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_SAMSUNG_TV_HACKS, RygelSamsungTVHacksClass))
#define RYGEL_IS_SAMSUNG_TV_HACKS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_SAMSUNG_TV_HACKS))
#define RYGEL_IS_SAMSUNG_TV_HACKS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_SAMSUNG_TV_HACKS))
#define RYGEL_SAMSUNG_TV_HACKS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_SAMSUNG_TV_HACKS, RygelSamsungTVHacksClass))

typedef struct _RygelSamsungTVHacks RygelSamsungTVHacks;
typedef struct _RygelSamsungTVHacksClass RygelSamsungTVHacksClass;
typedef struct _RygelSamsungTVHacksPrivate RygelSamsungTVHacksPrivate;
enum  {
	RYGEL_SAMSUNG_TV_HACKS_0_PROPERTY,
	RYGEL_SAMSUNG_TV_HACKS_NUM_PROPERTIES
};
static GParamSpec* rygel_samsung_tv_hacks_properties[RYGEL_SAMSUNG_TV_HACKS_NUM_PROPERTIES];
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

#define RYGEL_TYPE_MEDIA_RESOURCE (rygel_media_resource_get_type ())
#define RYGEL_MEDIA_RESOURCE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_RESOURCE, RygelMediaResource))
#define RYGEL_MEDIA_RESOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_RESOURCE, RygelMediaResourceClass))
#define RYGEL_IS_MEDIA_RESOURCE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_RESOURCE))
#define RYGEL_IS_MEDIA_RESOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_RESOURCE))
#define RYGEL_MEDIA_RESOURCE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_RESOURCE, RygelMediaResourceClass))

typedef struct _RygelMediaResource RygelMediaResource;
typedef struct _RygelMediaResourceClass RygelMediaResourceClass;

#define RYGEL_TYPE_MEDIA_ITEM (rygel_media_item_get_type ())
#define RYGEL_MEDIA_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_ITEM, RygelMediaItem))
#define RYGEL_MEDIA_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_ITEM, RygelMediaItemClass))
#define RYGEL_IS_MEDIA_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_ITEM))
#define RYGEL_IS_MEDIA_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_ITEM))
#define RYGEL_MEDIA_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_ITEM, RygelMediaItemClass))

typedef struct _RygelMediaItem RygelMediaItem;
typedef struct _RygelMediaItemClass RygelMediaItemClass;

#define RYGEL_TYPE_MEDIA_FILE_ITEM (rygel_media_file_item_get_type ())
#define RYGEL_MEDIA_FILE_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_FILE_ITEM, RygelMediaFileItem))
#define RYGEL_MEDIA_FILE_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_FILE_ITEM, RygelMediaFileItemClass))
#define RYGEL_IS_MEDIA_FILE_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_FILE_ITEM))
#define RYGEL_IS_MEDIA_FILE_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_FILE_ITEM))
#define RYGEL_MEDIA_FILE_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_FILE_ITEM, RygelMediaFileItemClass))

typedef struct _RygelMediaFileItem RygelMediaFileItem;
typedef struct _RygelMediaFileItemClass RygelMediaFileItemClass;

#define RYGEL_TYPE_VISUAL_ITEM (rygel_visual_item_get_type ())
#define RYGEL_VISUAL_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_VISUAL_ITEM, RygelVisualItem))
#define RYGEL_IS_VISUAL_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_VISUAL_ITEM))
#define RYGEL_VISUAL_ITEM_GET_INTERFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), RYGEL_TYPE_VISUAL_ITEM, RygelVisualItemIface))

typedef struct _RygelVisualItem RygelVisualItem;
typedef struct _RygelVisualItemIface RygelVisualItemIface;

#define RYGEL_TYPE_THUMBNAIL (rygel_thumbnail_get_type ())
#define RYGEL_THUMBNAIL(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_THUMBNAIL, RygelThumbnail))
#define RYGEL_THUMBNAIL_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_THUMBNAIL, RygelThumbnailClass))
#define RYGEL_IS_THUMBNAIL(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_THUMBNAIL))
#define RYGEL_IS_THUMBNAIL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_THUMBNAIL))
#define RYGEL_THUMBNAIL_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_THUMBNAIL, RygelThumbnailClass))

typedef struct _RygelThumbnail RygelThumbnail;
typedef struct _RygelThumbnailClass RygelThumbnailClass;
#define _rygel_icon_info_unref0(var) ((var == NULL) ? NULL : (var = (rygel_icon_info_unref (var), NULL)))
#define _g_free0(var) (var = (g_free (var), NULL))
typedef struct _RygelThumbnailPrivate RygelThumbnailPrivate;
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))
typedef struct _RygelHTTPRequestPrivate RygelHTTPRequestPrivate;

#define RYGEL_TYPE_HTTP_SERVER (rygel_http_server_get_type ())
#define RYGEL_HTTP_SERVER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_HTTP_SERVER, RygelHTTPServer))
#define RYGEL_HTTP_SERVER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_HTTP_SERVER, RygelHTTPServerClass))
#define RYGEL_IS_HTTP_SERVER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_HTTP_SERVER))
#define RYGEL_IS_HTTP_SERVER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_HTTP_SERVER))
#define RYGEL_HTTP_SERVER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_HTTP_SERVER, RygelHTTPServerClass))

typedef struct _RygelHTTPServer RygelHTTPServer;
typedef struct _RygelHTTPServerClass RygelHTTPServerClass;

#define RYGEL_TYPE_HTTP_ITEM_URI (rygel_http_item_uri_get_type ())
#define RYGEL_HTTP_ITEM_URI(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_HTTP_ITEM_URI, RygelHTTPItemURI))
#define RYGEL_HTTP_ITEM_URI_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_HTTP_ITEM_URI, RygelHTTPItemURIClass))
#define RYGEL_IS_HTTP_ITEM_URI(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_HTTP_ITEM_URI))
#define RYGEL_IS_HTTP_ITEM_URI_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_HTTP_ITEM_URI))
#define RYGEL_HTTP_ITEM_URI_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_HTTP_ITEM_URI, RygelHTTPItemURIClass))

typedef struct _RygelHTTPItemURI RygelHTTPItemURI;
typedef struct _RygelHTTPItemURIClass RygelHTTPItemURIClass;

#define RYGEL_TYPE_AUDIO_ITEM (rygel_audio_item_get_type ())
#define RYGEL_AUDIO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_AUDIO_ITEM, RygelAudioItem))
#define RYGEL_AUDIO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_AUDIO_ITEM, RygelAudioItemClass))
#define RYGEL_IS_AUDIO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_AUDIO_ITEM))
#define RYGEL_IS_AUDIO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_AUDIO_ITEM))
#define RYGEL_AUDIO_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_AUDIO_ITEM, RygelAudioItemClass))

typedef struct _RygelAudioItem RygelAudioItem;
typedef struct _RygelAudioItemClass RygelAudioItemClass;

#define RYGEL_TYPE_VIDEO_ITEM (rygel_video_item_get_type ())
#define RYGEL_VIDEO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_VIDEO_ITEM, RygelVideoItem))
#define RYGEL_VIDEO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_VIDEO_ITEM, RygelVideoItemClass))
#define RYGEL_IS_VIDEO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_VIDEO_ITEM))
#define RYGEL_IS_VIDEO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_VIDEO_ITEM))
#define RYGEL_VIDEO_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_VIDEO_ITEM, RygelVideoItemClass))

typedef struct _RygelVideoItem RygelVideoItem;
typedef struct _RygelVideoItemClass RygelVideoItemClass;

#define RYGEL_TYPE_SUBTITLE (rygel_subtitle_get_type ())
#define RYGEL_SUBTITLE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_SUBTITLE, RygelSubtitle))
#define RYGEL_SUBTITLE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_SUBTITLE, RygelSubtitleClass))
#define RYGEL_IS_SUBTITLE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_SUBTITLE))
#define RYGEL_IS_SUBTITLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_SUBTITLE))
#define RYGEL_SUBTITLE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_SUBTITLE, RygelSubtitleClass))

typedef struct _RygelSubtitle RygelSubtitle;
typedef struct _RygelSubtitleClass RygelSubtitleClass;
#define _g_regex_unref0(var) ((var == NULL) ? NULL : (var = (g_regex_unref (var), NULL)))

struct _RygelSearchableContainerIface {
	GTypeInterface parent_iface;
	void (*search) (RygelSearchableContainer* self, RygelSearchExpression* expression, guint offset, guint max_count, const gchar* sort_criteria, GCancellable* cancellable, GAsyncReadyCallback _callback_, gpointer _user_data_);
	RygelMediaObjects* (*search_finish) (RygelSearchableContainer* self, GAsyncResult* _res_, guint* total_matches, GError** error);
	GeeArrayList* (*get_search_classes) (RygelSearchableContainer* self);
	void (*set_search_classes) (RygelSearchableContainer* self, GeeArrayList* value);
};

struct _RygelClientHacks {
	GObject parent_instance;
	RygelClientHacksPrivate * priv;
	GRegex* agent_regex;
};

struct _RygelClientHacksClass {
	GObjectClass parent_class;
	void (*translate_container_id) (RygelClientHacks* self, RygelMediaQueryAction* action, gchar* * container_id);
	void (*apply) (RygelClientHacks* self, RygelMediaObject* object);
	void (*filter_sort_criteria) (RygelClientHacks* self, gchar* * sort_criteria);
	gboolean (*force_seek) (RygelClientHacks* self);
	void (*modify_headers) (RygelClientHacks* self, RygelHTTPRequest* request);
	void (*search) (RygelClientHacks* self, RygelSearchableContainer* container, RygelSearchExpression* expression, guint offset, guint max_count, const gchar* sort_criteria, GCancellable* cancellable, GAsyncReadyCallback _callback_, gpointer _user_data_);
	RygelMediaObjects* (*search_finish) (RygelClientHacks* self, GAsyncResult* _res_, guint* total_matches, GError** error);
};

struct _RygelSamsungTVHacks {
	RygelClientHacks parent_instance;
	RygelSamsungTVHacksPrivate * priv;
};

struct _RygelSamsungTVHacksClass {
	RygelClientHacksClass parent_class;
};

typedef enum  {
	RYGEL_CLIENT_HACKS_ERROR_NA
} RygelClientHacksError;
#define RYGEL_CLIENT_HACKS_ERROR rygel_client_hacks_error_quark ()
struct _RygelVisualItemIface {
	GTypeInterface parent_iface;
	gint (*get_width) (RygelVisualItem* self);
	void (*set_width) (RygelVisualItem* self, gint value);
	gint (*get_height) (RygelVisualItem* self);
	void (*set_height) (RygelVisualItem* self, gint value);
	gint (*get_color_depth) (RygelVisualItem* self);
	void (*set_color_depth) (RygelVisualItem* self, gint value);
	GeeArrayList* (*get_thumbnails) (RygelVisualItem* self);
	void (*set_thumbnails) (RygelVisualItem* self, GeeArrayList* value);
};

struct _RygelThumbnail {
	RygelIconInfo parent_instance;
	RygelThumbnailPrivate * priv;
	gchar* dlna_profile;
};

struct _RygelThumbnailClass {
	RygelIconInfoClass parent_class;
	RygelMediaResource* (*get_resource) (RygelThumbnail* self, const gchar* protocol, gint index);
};

struct _RygelHTTPRequest {
	GObject parent_instance;
	RygelHTTPRequestPrivate * priv;
	RygelHTTPServer* http_server;
	SoupServer* server;
	SoupMessage* msg;
	RygelHTTPItemURI* uri;
	RygelMediaObject* object;
	RygelClientHacks* hack;
};

struct _RygelHTTPRequestClass {
	GObjectClass parent_class;
	void (*handle) (RygelHTTPRequest* self, GAsyncReadyCallback _callback_, gpointer _user_data_);
	void (*handle_finish) (RygelHTTPRequest* self, GAsyncResult* _res_, GError** error);
	void (*find_item) (RygelHTTPRequest* self, GAsyncReadyCallback _callback_, gpointer _user_data_);
	void (*find_item_finish) (RygelHTTPRequest* self, GAsyncResult* _res_, GError** error);
};

static gpointer rygel_samsung_tv_hacks_parent_class = NULL;
static GRegex* rygel_samsung_tv_hacks_mime_regex;
static GRegex* rygel_samsung_tv_hacks_mime_regex = NULL;
static GRegex* rygel_samsung_tv_hacks_dlna_regex;
static GRegex* rygel_samsung_tv_hacks_dlna_regex = NULL;

GType rygel_client_hacks_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelClientHacks, g_object_unref)
GType rygel_media_query_action_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaQueryAction, g_object_unref)
GType rygel_media_object_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaObject, g_object_unref)
GType rygel_http_request_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelHTTPRequest, g_object_unref)
GType rygel_media_container_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaContainer, g_object_unref)
gpointer rygel_search_expression_ref (gpointer instance);
void rygel_search_expression_unref (gpointer instance);
GParamSpec* rygel_param_spec_search_expression (const gchar* name,
                                                const gchar* nick,
                                                const gchar* blurb,
                                                GType object_type,
                                                GParamFlags flags);
void rygel_value_set_search_expression (GValue* value,
                                        gpointer v_object);
void rygel_value_take_search_expression (GValue* value,
                                         gpointer v_object);
gpointer rygel_value_get_search_expression (const GValue* value);
GType rygel_search_expression_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelSearchExpression, rygel_search_expression_unref)
GType rygel_media_objects_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaObjects, g_object_unref)
GType rygel_searchable_container_get_type (void) G_GNUC_CONST;
GType rygel_samsung_tv_hacks_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelSamsungTVHacks, g_object_unref)
#define RYGEL_SAMSUNG_TV_HACKS_AGENT ".*SEC_HHP.*|.*SEC HHP.*"
GQuark rygel_client_hacks_error_quark (void);
RygelSamsungTVHacks* rygel_samsung_tv_hacks_new (SoupMessage* message,
                                                 GError** error);
RygelSamsungTVHacks* rygel_samsung_tv_hacks_construct (GType object_type,
                                                       SoupMessage* message,
                                                       GError** error);
RygelClientHacks* rygel_client_hacks_construct (GType object_type,
                                                const gchar* agent,
                                                SoupMessage* message,
                                                GError** error);
static void rygel_samsung_tv_hacks_real_apply (RygelClientHacks* base,
                                        RygelMediaObject* object);
GType rygel_media_resource_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaResource, g_object_unref)
GeeList* rygel_media_object_get_resource_list (RygelMediaObject* self);
const gchar* rygel_media_resource_get_mime_type (RygelMediaResource* self);
void rygel_media_resource_set_mime_type (RygelMediaResource* self,
                                         const gchar* value);
GType rygel_media_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaItem, g_object_unref)
GType rygel_media_file_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaFileItem, g_object_unref)
GType rygel_thumbnail_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelThumbnail, rygel_icon_info_unref)
GType rygel_visual_item_get_type (void) G_GNUC_CONST;
GeeArrayList* rygel_visual_item_get_thumbnails (RygelVisualItem* self);
static gboolean rygel_samsung_tv_hacks_real_force_seek (RygelClientHacks* base);
static void rygel_samsung_tv_hacks_real_modify_headers (RygelClientHacks* base,
                                                 RygelHTTPRequest* request);
GType rygel_http_server_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelHTTPServer, g_object_unref)
GType rygel_http_item_uri_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelHTTPItemURI, g_object_unref)
GType rygel_audio_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelAudioItem, g_object_unref)
GType rygel_video_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelVideoItem, g_object_unref)
gpointer rygel_subtitle_ref (gpointer instance);
void rygel_subtitle_unref (gpointer instance);
GParamSpec* rygel_param_spec_subtitle (const gchar* name,
                                       const gchar* nick,
                                       const gchar* blurb,
                                       GType object_type,
                                       GParamFlags flags);
void rygel_value_set_subtitle (GValue* value,
                               gpointer v_object);
void rygel_value_take_subtitle (GValue* value,
                                gpointer v_object);
gpointer rygel_value_get_subtitle (const GValue* value);
GType rygel_subtitle_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelSubtitle, rygel_subtitle_unref)
GeeArrayList* rygel_video_item_get_subtitles (RygelVideoItem* self);
gchar* rygel_http_server_create_uri_for_object (RygelHTTPServer* self,
                                                RygelMediaObject* object,
                                                gint thumbnail_index,
                                                gint subtitle_index,
                                                const gchar* resource_name);
static void rygel_samsung_tv_hacks_finalize (GObject * obj);

RygelSamsungTVHacks*
rygel_samsung_tv_hacks_construct (GType object_type,
                                  SoupMessage* message,
                                  GError** error)
{
	RygelSamsungTVHacks * self = NULL;
	GError* _inner_error0_ = NULL;
	self = (RygelSamsungTVHacks*) rygel_client_hacks_construct (object_type, RYGEL_SAMSUNG_TV_HACKS_AGENT, message, &_inner_error0_);
	if (G_UNLIKELY (_inner_error0_ != NULL)) {
		if (_inner_error0_->domain == RYGEL_CLIENT_HACKS_ERROR) {
			g_propagate_error (error, _inner_error0_);
			_g_object_unref0 (self);
			return NULL;
		} else {
			g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
			g_clear_error (&_inner_error0_);
			return NULL;
		}
	}
	return self;
}

RygelSamsungTVHacks*
rygel_samsung_tv_hacks_new (SoupMessage* message,
                            GError** error)
{
	return rygel_samsung_tv_hacks_construct (RYGEL_TYPE_SAMSUNG_TV_HACKS, message, error);
}

static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

static void
rygel_samsung_tv_hacks_real_apply (RygelClientHacks* base,
                                   RygelMediaObject* object)
{
	RygelSamsungTVHacks * self;
	RygelMediaFileItem* item = NULL;
	RygelMediaFileItem* _tmp18_;
	RygelMediaFileItem* _tmp19_;
	GError* _inner_error0_ = NULL;
	self = (RygelSamsungTVHacks*) base;
	g_return_if_fail (object != NULL);
	{
		GeeList* _resource_list = NULL;
		GeeList* _tmp0_;
		gint _resource_size = 0;
		GeeList* _tmp1_;
		gint _tmp2_;
		gint _tmp3_;
		gint _resource_index = 0;
		_tmp0_ = rygel_media_object_get_resource_list (object);
		_resource_list = _tmp0_;
		_tmp1_ = _resource_list;
		_tmp2_ = gee_collection_get_size ((GeeCollection*) _tmp1_);
		_tmp3_ = _tmp2_;
		_resource_size = _tmp3_;
		_resource_index = -1;
		while (TRUE) {
			RygelMediaResource* resource = NULL;
			GeeList* _tmp4_;
			gpointer _tmp5_;
			RygelMediaResource* _tmp6_;
			const gchar* _tmp7_;
			const gchar* _tmp8_;
			_resource_index = _resource_index + 1;
			if (!(_resource_index < _resource_size)) {
				break;
			}
			_tmp4_ = _resource_list;
			_tmp5_ = gee_list_get (_tmp4_, _resource_index);
			resource = (RygelMediaResource*) _tmp5_;
			_tmp6_ = resource;
			_tmp7_ = rygel_media_resource_get_mime_type (_tmp6_);
			_tmp8_ = _tmp7_;
			if (g_strcmp0 (_tmp8_, "video/x-matroska") == 0) {
				RygelMediaResource* _tmp9_;
				_tmp9_ = resource;
				rygel_media_resource_set_mime_type (_tmp9_, "video/x-mkv");
			} else {
				RygelMediaResource* _tmp10_;
				const gchar* _tmp11_;
				const gchar* _tmp12_;
				_tmp10_ = resource;
				_tmp11_ = rygel_media_resource_get_mime_type (_tmp10_);
				_tmp12_ = _tmp11_;
				if (g_strcmp0 (_tmp12_, "video/mp2t") == 0) {
					RygelMediaResource* _tmp13_;
					_tmp13_ = resource;
					rygel_media_resource_set_mime_type (_tmp13_, "video/vnd.dlna.mpeg-tts");
				} else {
					RygelMediaResource* _tmp14_;
					const gchar* _tmp15_;
					const gchar* _tmp16_;
					_tmp14_ = resource;
					_tmp15_ = rygel_media_resource_get_mime_type (_tmp14_);
					_tmp16_ = _tmp15_;
					if (g_strcmp0 (_tmp16_, "video/quicktime") == 0) {
						RygelMediaResource* _tmp17_;
						_tmp17_ = resource;
						rygel_media_resource_set_mime_type (_tmp17_, "video/mp4");
					}
				}
			}
			_g_object_unref0 (resource);
		}
		_g_object_unref0 (_resource_list);
	}
	if (!RYGEL_IS_MEDIA_FILE_ITEM (object)) {
		return;
	}
	_tmp18_ = _g_object_ref0 (RYGEL_IS_MEDIA_FILE_ITEM (object) ? ((RygelMediaFileItem*) object) : NULL);
	item = _tmp18_;
	_tmp19_ = item;
	if (!RYGEL_IS_VISUAL_ITEM (_tmp19_)) {
		_g_object_unref0 (item);
		return;
	}
	{
		GeeArrayList* _thumbnail_list = NULL;
		RygelMediaFileItem* _tmp20_;
		GeeArrayList* _tmp21_;
		GeeArrayList* _tmp22_;
		GeeArrayList* _tmp23_;
		gint _thumbnail_size = 0;
		GeeArrayList* _tmp24_;
		gint _tmp25_;
		gint _tmp26_;
		gint _thumbnail_index = 0;
		_tmp20_ = item;
		_tmp21_ = rygel_visual_item_get_thumbnails (RYGEL_IS_VISUAL_ITEM (_tmp20_) ? ((RygelVisualItem*) _tmp20_) : NULL);
		_tmp22_ = _tmp21_;
		_tmp23_ = _g_object_ref0 (_tmp22_);
		_thumbnail_list = _tmp23_;
		_tmp24_ = _thumbnail_list;
		_tmp25_ = gee_abstract_collection_get_size ((GeeAbstractCollection*) _tmp24_);
		_tmp26_ = _tmp25_;
		_thumbnail_size = _tmp26_;
		_thumbnail_index = -1;
		while (TRUE) {
			RygelThumbnail* thumbnail = NULL;
			GeeArrayList* _tmp27_;
			gpointer _tmp28_;
			_thumbnail_index = _thumbnail_index + 1;
			if (!(_thumbnail_index < _thumbnail_size)) {
				break;
			}
			_tmp27_ = _thumbnail_list;
			_tmp28_ = gee_abstract_list_get ((GeeAbstractList*) _tmp27_, _thumbnail_index);
			thumbnail = (RygelThumbnail*) _tmp28_;
			{
				gchar* _tmp29_ = NULL;
				GRegex* _tmp30_;
				RygelThumbnail* _tmp31_;
				const gchar* _tmp32_;
				gchar* _tmp33_;
				RygelThumbnail* _tmp34_;
				gchar* _tmp35_;
				gchar* _tmp36_ = NULL;
				GRegex* _tmp37_;
				RygelThumbnail* _tmp38_;
				const gchar* _tmp39_;
				gchar* _tmp40_;
				RygelThumbnail* _tmp41_;
				gchar* _tmp42_;
				_tmp30_ = rygel_samsung_tv_hacks_mime_regex;
				_tmp31_ = thumbnail;
				_tmp32_ = ((RygelIconInfo*) _tmp31_)->mime_type;
				_tmp33_ = g_regex_replace_literal (_tmp30_, _tmp32_, (gssize) -1, 0, "jpeg", 0, &_inner_error0_);
				_tmp29_ = _tmp33_;
				if (G_UNLIKELY (_inner_error0_ != NULL)) {
					if (_inner_error0_->domain == G_REGEX_ERROR) {
						goto __catch78_g_regex_error;
					}
					_rygel_icon_info_unref0 (thumbnail);
					_g_object_unref0 (_thumbnail_list);
					_g_object_unref0 (item);
					g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
					g_clear_error (&_inner_error0_);
					return;
				}
				_tmp34_ = thumbnail;
				_tmp35_ = _tmp29_;
				_tmp29_ = NULL;
				_g_free0 (((RygelIconInfo*) _tmp34_)->mime_type);
				((RygelIconInfo*) _tmp34_)->mime_type = _tmp35_;
				_tmp37_ = rygel_samsung_tv_hacks_dlna_regex;
				_tmp38_ = thumbnail;
				_tmp39_ = _tmp38_->dlna_profile;
				_tmp40_ = g_regex_replace_literal (_tmp37_, _tmp39_, (gssize) -1, 0, "JPEG", 0, &_inner_error0_);
				_tmp36_ = _tmp40_;
				if (G_UNLIKELY (_inner_error0_ != NULL)) {
					_g_free0 (_tmp29_);
					if (_inner_error0_->domain == G_REGEX_ERROR) {
						goto __catch78_g_regex_error;
					}
					_rygel_icon_info_unref0 (thumbnail);
					_g_object_unref0 (_thumbnail_list);
					_g_object_unref0 (item);
					g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
					g_clear_error (&_inner_error0_);
					return;
				}
				_tmp41_ = thumbnail;
				_tmp42_ = _tmp36_;
				_tmp36_ = NULL;
				_g_free0 (_tmp41_->dlna_profile);
				_tmp41_->dlna_profile = _tmp42_;
				_g_free0 (_tmp36_);
				_g_free0 (_tmp29_);
			}
			goto __finally78;
			__catch78_g_regex_error:
			{
				GError* _error_ = NULL;
				_error_ = _inner_error0_;
				_inner_error0_ = NULL;
				g_assert_not_reached ();
				_g_error_free0 (_error_);
			}
			__finally78:
			if (G_UNLIKELY (_inner_error0_ != NULL)) {
				_rygel_icon_info_unref0 (thumbnail);
				_g_object_unref0 (_thumbnail_list);
				_g_object_unref0 (item);
				g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
				g_clear_error (&_inner_error0_);
				return;
			}
			_rygel_icon_info_unref0 (thumbnail);
		}
		_g_object_unref0 (_thumbnail_list);
	}
	_g_object_unref0 (item);
}

static gboolean
rygel_samsung_tv_hacks_real_force_seek (RygelClientHacks* base)
{
	RygelSamsungTVHacks * self;
	gboolean result = FALSE;
	self = (RygelSamsungTVHacks*) base;
	result = TRUE;
	return result;
}

static void
rygel_samsung_tv_hacks_real_modify_headers (RygelClientHacks* base,
                                            RygelHTTPRequest* request)
{
	RygelSamsungTVHacks * self;
	gboolean _tmp0_ = FALSE;
	gboolean _tmp1_ = FALSE;
	SoupMessage* _tmp2_;
	SoupMessageHeaders* _tmp3_;
	const gchar* _tmp4_;
	self = (RygelSamsungTVHacks*) base;
	g_return_if_fail (request != NULL);
	_tmp2_ = request->msg;
	_tmp3_ = _tmp2_->request_headers;
	_tmp4_ = soup_message_headers_get_one (_tmp3_, "getCaptionInfo.sec");
	if (_tmp4_ != NULL) {
		RygelMediaObject* _tmp5_;
		_tmp5_ = request->object;
		_tmp1_ = RYGEL_IS_VIDEO_ITEM (_tmp5_);
	} else {
		_tmp1_ = FALSE;
	}
	if (_tmp1_) {
		RygelMediaObject* _tmp6_;
		GeeArrayList* _tmp7_;
		GeeArrayList* _tmp8_;
		gint _tmp9_;
		gint _tmp10_;
		_tmp6_ = request->object;
		_tmp7_ = rygel_video_item_get_subtitles (RYGEL_IS_VIDEO_ITEM (_tmp6_) ? ((RygelVideoItem*) _tmp6_) : NULL);
		_tmp8_ = _tmp7_;
		_tmp9_ = gee_abstract_collection_get_size ((GeeAbstractCollection*) _tmp8_);
		_tmp10_ = _tmp9_;
		_tmp0_ = _tmp10_ > 0;
	} else {
		_tmp0_ = FALSE;
	}
	if (_tmp0_) {
		gchar* caption_uri = NULL;
		RygelHTTPServer* _tmp11_;
		RygelMediaObject* _tmp12_;
		gchar* _tmp13_;
		SoupMessage* _tmp14_;
		SoupMessageHeaders* _tmp15_;
		const gchar* _tmp16_;
		_tmp11_ = request->http_server;
		_tmp12_ = request->object;
		_tmp13_ = rygel_http_server_create_uri_for_object (_tmp11_, (RygelMediaObject*) (RYGEL_IS_MEDIA_ITEM (_tmp12_) ? ((RygelMediaItem*) _tmp12_) : NULL), -1, 0, NULL);
		caption_uri = _tmp13_;
		_tmp14_ = request->msg;
		_tmp15_ = _tmp14_->response_headers;
		_tmp16_ = caption_uri;
		soup_message_headers_append (_tmp15_, "CaptionInfo.sec", _tmp16_);
		_g_free0 (caption_uri);
	}
}

static void
rygel_samsung_tv_hacks_class_init (RygelSamsungTVHacksClass * klass,
                                   gpointer klass_data)
{
	GError* _inner_error0_ = NULL;
	rygel_samsung_tv_hacks_parent_class = g_type_class_peek_parent (klass);
	((RygelClientHacksClass *) klass)->apply = (void (*) (RygelClientHacks*, RygelMediaObject*)) rygel_samsung_tv_hacks_real_apply;
	((RygelClientHacksClass *) klass)->force_seek = (gboolean (*) (RygelClientHacks*)) rygel_samsung_tv_hacks_real_force_seek;
	((RygelClientHacksClass *) klass)->modify_headers = (void (*) (RygelClientHacks*, RygelHTTPRequest*)) rygel_samsung_tv_hacks_real_modify_headers;
	G_OBJECT_CLASS (klass)->finalize = rygel_samsung_tv_hacks_finalize;
	{
		GRegex* _tmp0_ = NULL;
		GRegex* _tmp1_;
		GRegex* _tmp2_;
		GRegex* _tmp3_ = NULL;
		GRegex* _tmp4_;
		GRegex* _tmp5_;
		_tmp1_ = g_regex_new ("png", 0, 0, &_inner_error0_);
		_tmp0_ = _tmp1_;
		if (G_UNLIKELY (_inner_error0_ != NULL)) {
			if (_inner_error0_->domain == G_REGEX_ERROR) {
				goto __catch79_g_regex_error;
			}
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
			g_clear_error (&_inner_error0_);
		}
		_tmp2_ = _tmp0_;
		_tmp0_ = NULL;
		_g_regex_unref0 (rygel_samsung_tv_hacks_mime_regex);
		rygel_samsung_tv_hacks_mime_regex = _tmp2_;
		_tmp4_ = g_regex_new ("PNG", 0, 0, &_inner_error0_);
		_tmp3_ = _tmp4_;
		if (G_UNLIKELY (_inner_error0_ != NULL)) {
			_g_regex_unref0 (_tmp0_);
			if (_inner_error0_->domain == G_REGEX_ERROR) {
				goto __catch79_g_regex_error;
			}
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
			g_clear_error (&_inner_error0_);
		}
		_tmp5_ = _tmp3_;
		_tmp3_ = NULL;
		_g_regex_unref0 (rygel_samsung_tv_hacks_dlna_regex);
		rygel_samsung_tv_hacks_dlna_regex = _tmp5_;
		_g_regex_unref0 (_tmp3_);
		_g_regex_unref0 (_tmp0_);
	}
	goto __finally79;
	__catch79_g_regex_error:
	{
		GError* _error_ = NULL;
		_error_ = _inner_error0_;
		_inner_error0_ = NULL;
		g_assert_not_reached ();
		_g_error_free0 (_error_);
	}
	__finally79:
	if (G_UNLIKELY (_inner_error0_ != NULL)) {
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
		g_clear_error (&_inner_error0_);
	}
}

static void
rygel_samsung_tv_hacks_instance_init (RygelSamsungTVHacks * self,
                                      gpointer klass)
{
}

static void
rygel_samsung_tv_hacks_finalize (GObject * obj)
{
	RygelSamsungTVHacks * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, RYGEL_TYPE_SAMSUNG_TV_HACKS, RygelSamsungTVHacks);
	G_OBJECT_CLASS (rygel_samsung_tv_hacks_parent_class)->finalize (obj);
}

GType
rygel_samsung_tv_hacks_get_type (void)
{
	static volatile gsize rygel_samsung_tv_hacks_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_samsung_tv_hacks_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelSamsungTVHacksClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_samsung_tv_hacks_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelSamsungTVHacks), 0, (GInstanceInitFunc) rygel_samsung_tv_hacks_instance_init, NULL };
		GType rygel_samsung_tv_hacks_type_id;
		rygel_samsung_tv_hacks_type_id = g_type_register_static (RYGEL_TYPE_CLIENT_HACKS, "RygelSamsungTVHacks", &g_define_type_info, 0);
		g_once_init_leave (&rygel_samsung_tv_hacks_type_id__volatile, rygel_samsung_tv_hacks_type_id);
	}
	return rygel_samsung_tv_hacks_type_id__volatile;
}

