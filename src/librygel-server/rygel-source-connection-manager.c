/* rygel-source-connection-manager.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-source-connection-manager.vala, do not modify */

/*
 * Copyright (C) 2012 Intel Corporation.
 * Copyright (C) 2009-2011 Nokia Corporation.
 * Copyright (C) 2008 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>.
 *
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *         Jens Georg <jensg@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <rygel-core.h>
#include <glib-object.h>
#include <gee.h>
#include <libgupnp-av/gupnp-av.h>
#include <glib.h>
#include <libgupnp/gupnp.h>
#include <stdlib.h>
#include <string.h>
#include <gio/gio.h>

#define RYGEL_TYPE_SOURCE_CONNECTION_MANAGER (rygel_source_connection_manager_get_type ())
#define RYGEL_SOURCE_CONNECTION_MANAGER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_SOURCE_CONNECTION_MANAGER, RygelSourceConnectionManager))
#define RYGEL_SOURCE_CONNECTION_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_SOURCE_CONNECTION_MANAGER, RygelSourceConnectionManagerClass))
#define RYGEL_IS_SOURCE_CONNECTION_MANAGER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_SOURCE_CONNECTION_MANAGER))
#define RYGEL_IS_SOURCE_CONNECTION_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_SOURCE_CONNECTION_MANAGER))
#define RYGEL_SOURCE_CONNECTION_MANAGER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_SOURCE_CONNECTION_MANAGER, RygelSourceConnectionManagerClass))

typedef struct _RygelSourceConnectionManager RygelSourceConnectionManager;
typedef struct _RygelSourceConnectionManagerClass RygelSourceConnectionManagerClass;
typedef struct _RygelSourceConnectionManagerPrivate RygelSourceConnectionManagerPrivate;
enum  {
	RYGEL_SOURCE_CONNECTION_MANAGER_0_PROPERTY,
	RYGEL_SOURCE_CONNECTION_MANAGER_NUM_PROPERTIES
};
static GParamSpec* rygel_source_connection_manager_properties[RYGEL_SOURCE_CONNECTION_MANAGER_NUM_PROPERTIES];
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

#define RYGEL_TYPE_HTTP_SERVER (rygel_http_server_get_type ())
#define RYGEL_HTTP_SERVER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_HTTP_SERVER, RygelHTTPServer))
#define RYGEL_HTTP_SERVER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_HTTP_SERVER, RygelHTTPServerClass))
#define RYGEL_IS_HTTP_SERVER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_HTTP_SERVER))
#define RYGEL_IS_HTTP_SERVER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_HTTP_SERVER))
#define RYGEL_HTTP_SERVER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_HTTP_SERVER, RygelHTTPServerClass))

typedef struct _RygelHTTPServer RygelHTTPServer;
typedef struct _RygelHTTPServerClass RygelHTTPServerClass;

#define RYGEL_TYPE_MEDIA_SERVER_PLUGIN (rygel_media_server_plugin_get_type ())
#define RYGEL_MEDIA_SERVER_PLUGIN(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_SERVER_PLUGIN, RygelMediaServerPlugin))
#define RYGEL_MEDIA_SERVER_PLUGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_SERVER_PLUGIN, RygelMediaServerPluginClass))
#define RYGEL_IS_MEDIA_SERVER_PLUGIN(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_SERVER_PLUGIN))
#define RYGEL_IS_MEDIA_SERVER_PLUGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_SERVER_PLUGIN))
#define RYGEL_MEDIA_SERVER_PLUGIN_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_SERVER_PLUGIN, RygelMediaServerPluginClass))

typedef struct _RygelMediaServerPlugin RygelMediaServerPlugin;
typedef struct _RygelMediaServerPluginClass RygelMediaServerPluginClass;
#define _rygel_dlna_profile_unref0(var) ((var == NULL) ? NULL : (var = (rygel_dlna_profile_unref (var), NULL)))

#define RYGEL_TYPE_CONTENT_DIRECTORY (rygel_content_directory_get_type ())
#define RYGEL_CONTENT_DIRECTORY(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_CONTENT_DIRECTORY, RygelContentDirectory))
#define RYGEL_CONTENT_DIRECTORY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_CONTENT_DIRECTORY, RygelContentDirectoryClass))
#define RYGEL_IS_CONTENT_DIRECTORY(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_CONTENT_DIRECTORY))
#define RYGEL_IS_CONTENT_DIRECTORY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_CONTENT_DIRECTORY))
#define RYGEL_CONTENT_DIRECTORY_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_CONTENT_DIRECTORY, RygelContentDirectoryClass))

typedef struct _RygelContentDirectory RygelContentDirectory;
typedef struct _RygelContentDirectoryClass RygelContentDirectoryClass;
typedef struct _RygelContentDirectoryPrivate RygelContentDirectoryPrivate;

#define RYGEL_TYPE_MEDIA_OBJECT (rygel_media_object_get_type ())
#define RYGEL_MEDIA_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObject))
#define RYGEL_MEDIA_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObjectClass))
#define RYGEL_IS_MEDIA_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_OBJECT))
#define RYGEL_IS_MEDIA_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_OBJECT))
#define RYGEL_MEDIA_OBJECT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObjectClass))

typedef struct _RygelMediaObject RygelMediaObject;
typedef struct _RygelMediaObjectClass RygelMediaObjectClass;

#define RYGEL_TYPE_MEDIA_CONTAINER (rygel_media_container_get_type ())
#define RYGEL_MEDIA_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainer))
#define RYGEL_MEDIA_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainerClass))
#define RYGEL_IS_MEDIA_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_CONTAINER))
#define RYGEL_IS_MEDIA_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_CONTAINER))
#define RYGEL_MEDIA_CONTAINER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainerClass))

typedef struct _RygelMediaContainer RygelMediaContainer;
typedef struct _RygelMediaContainerClass RygelMediaContainerClass;

struct _RygelSourceConnectionManager {
	RygelConnectionManager parent_instance;
	RygelSourceConnectionManagerPrivate * priv;
};

struct _RygelSourceConnectionManagerClass {
	RygelConnectionManagerClass parent_class;
};

struct _RygelContentDirectory {
	GUPnPService parent_instance;
	RygelContentDirectoryPrivate * priv;
	gchar* feature_list;
	RygelHTTPServer* http_server;
	RygelMediaContainer* root_container;
	GCancellable* cancellable;
	guint32 system_update_id;
};

struct _RygelContentDirectoryClass {
	GUPnPServiceClass parent_class;
};

static gpointer rygel_source_connection_manager_parent_class = NULL;

GType rygel_source_connection_manager_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelSourceConnectionManager, g_object_unref)
static void rygel_source_connection_manager_real_constructed (GObject* base);
static GeeArrayList* rygel_source_connection_manager_get_protocol_info (RygelSourceConnectionManager* self);
GType rygel_http_server_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelHTTPServer, g_object_unref)
static RygelHTTPServer* rygel_source_connection_manager_get_http_server (RygelSourceConnectionManager* self);
GeeArrayList* rygel_http_server_get_protocol_info (RygelHTTPServer* self);
GType rygel_media_server_plugin_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaServerPlugin, g_object_unref)
GList* rygel_media_server_plugin_get_supported_profiles (RygelMediaServerPlugin* self);
gchar* rygel_http_server_get_protocol (RygelHTTPServer* self);
GType rygel_content_directory_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelContentDirectory, g_object_unref)
GType rygel_media_object_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaObject, g_object_unref)
GType rygel_media_container_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaContainer, g_object_unref)
RygelSourceConnectionManager* rygel_source_connection_manager_new (void);
RygelSourceConnectionManager* rygel_source_connection_manager_construct (GType object_type);

static void
rygel_source_connection_manager_real_constructed (GObject* base)
{
	RygelSourceConnectionManager * self;
	gchar* _tmp0_;
	self = (RygelSourceConnectionManager*) base;
	G_OBJECT_CLASS (rygel_source_connection_manager_parent_class)->constructed ((GObject*) G_TYPE_CHECK_INSTANCE_CAST (self, RYGEL_TYPE_CONNECTION_MANAGER, RygelConnectionManager));
	((RygelConnectionManager*) self)->rcs_id = -1;
	((RygelConnectionManager*) self)->av_transport_id = -1;
	_tmp0_ = g_strdup ("Output");
	_g_free0 (((RygelConnectionManager*) self)->direction);
	((RygelConnectionManager*) self)->direction = _tmp0_;
	{
		GeeArrayList* _protocol_info_list = NULL;
		GeeArrayList* _tmp1_;
		gint _protocol_info_size = 0;
		GeeArrayList* _tmp2_;
		gint _tmp3_;
		gint _tmp4_;
		gint _protocol_info_index = 0;
		_tmp1_ = rygel_source_connection_manager_get_protocol_info (self);
		_protocol_info_list = _tmp1_;
		_tmp2_ = _protocol_info_list;
		_tmp3_ = gee_abstract_collection_get_size ((GeeAbstractCollection*) _tmp2_);
		_tmp4_ = _tmp3_;
		_protocol_info_size = _tmp4_;
		_protocol_info_index = -1;
		while (TRUE) {
			GUPnPProtocolInfo* protocol_info = NULL;
			GeeArrayList* _tmp5_;
			gpointer _tmp6_;
			const gchar* _tmp7_;
			const gchar* _tmp10_;
			GUPnPProtocolInfo* _tmp11_;
			gchar* _tmp12_;
			gchar* _tmp13_;
			gchar* _tmp14_;
			_protocol_info_index = _protocol_info_index + 1;
			if (!(_protocol_info_index < _protocol_info_size)) {
				break;
			}
			_tmp5_ = _protocol_info_list;
			_tmp6_ = gee_abstract_list_get ((GeeAbstractList*) _tmp5_, _protocol_info_index);
			protocol_info = (GUPnPProtocolInfo*) _tmp6_;
			_tmp7_ = ((RygelConnectionManager*) self)->source_protocol_info;
			if (g_strcmp0 (_tmp7_, "") != 0) {
				const gchar* _tmp8_;
				gchar* _tmp9_;
				_tmp8_ = ((RygelConnectionManager*) self)->source_protocol_info;
				_tmp9_ = g_strconcat (_tmp8_, ",", NULL);
				_g_free0 (((RygelConnectionManager*) self)->source_protocol_info);
				((RygelConnectionManager*) self)->source_protocol_info = _tmp9_;
			}
			_tmp10_ = ((RygelConnectionManager*) self)->source_protocol_info;
			_tmp11_ = protocol_info;
			_tmp12_ = gupnp_protocol_info_to_string (_tmp11_);
			_tmp13_ = _tmp12_;
			_tmp14_ = g_strconcat (_tmp10_, _tmp13_, NULL);
			_g_free0 (((RygelConnectionManager*) self)->source_protocol_info);
			((RygelConnectionManager*) self)->source_protocol_info = _tmp14_;
			_g_free0 (_tmp13_);
			_g_object_unref0 (protocol_info);
		}
		_g_object_unref0 (_protocol_info_list);
	}
}

static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

static gpointer
_rygel_dlna_profile_ref0 (gpointer self)
{
	return self ? rygel_dlna_profile_ref (self) : NULL;
}

static GeeArrayList*
rygel_source_connection_manager_get_protocol_info (RygelSourceConnectionManager* self)
{
	RygelHTTPServer* server = NULL;
	RygelHTTPServer* _tmp0_;
	GeeArrayList* protocol_infos = NULL;
	RygelHTTPServer* _tmp1_;
	GeeArrayList* _tmp2_;
	RygelMediaServerPlugin* plugin = NULL;
	GUPnPRootDevice* _tmp3_;
	GUPnPRootDevice* _tmp4_;
	GUPnPRootDevice* _tmp5_;
	GUPnPResourceFactory* _tmp6_;
	GUPnPResourceFactory* _tmp7_;
	RygelMediaServerPlugin* _tmp8_;
	RygelMediaServerPlugin* _tmp9_;
	GList* profiles = NULL;
	RygelMediaServerPlugin* _tmp10_;
	GList* _tmp11_;
	GList* _tmp12_;
	gchar* protocol = NULL;
	RygelHTTPServer* _tmp13_;
	gchar* _tmp14_;
	GList* _tmp15_;
	GeeArrayList* result = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = rygel_source_connection_manager_get_http_server (self);
	server = _tmp0_;
	_tmp1_ = server;
	_tmp2_ = rygel_http_server_get_protocol_info (_tmp1_);
	protocol_infos = _tmp2_;
	g_object_get ((GUPnPService*) self, "root-device", &_tmp3_, NULL);
	_tmp4_ = _tmp3_;
	_tmp5_ = _tmp4_;
	_tmp6_ = gupnp_device_info_get_resource_factory ((GUPnPDeviceInfo*) _tmp5_);
	_tmp7_ = _tmp6_;
	_tmp8_ = _g_object_ref0 (RYGEL_IS_MEDIA_SERVER_PLUGIN (_tmp7_) ? ((RygelMediaServerPlugin*) _tmp7_) : NULL);
	_tmp9_ = _tmp8_;
	_g_object_unref0 (_tmp5_);
	plugin = _tmp9_;
	_tmp10_ = plugin;
	_tmp11_ = rygel_media_server_plugin_get_supported_profiles (_tmp10_);
	_tmp12_ = _tmp11_;
	profiles = _tmp12_;
	_tmp13_ = server;
	_tmp14_ = rygel_http_server_get_protocol (_tmp13_);
	protocol = _tmp14_;
	_tmp15_ = profiles;
	{
		GList* profile_collection = NULL;
		GList* profile_it = NULL;
		profile_collection = _tmp15_;
		for (profile_it = profile_collection; profile_it != NULL; profile_it = profile_it->next) {
			RygelDLNAProfile* _tmp16_;
			RygelDLNAProfile* profile = NULL;
			_tmp16_ = _rygel_dlna_profile_ref0 ((RygelDLNAProfile*) profile_it->data);
			profile = _tmp16_;
			{
				GUPnPProtocolInfo* protocol_info = NULL;
				GUPnPProtocolInfo* _tmp17_;
				GUPnPProtocolInfo* _tmp18_;
				const gchar* _tmp19_;
				GUPnPProtocolInfo* _tmp20_;
				RygelDLNAProfile* _tmp21_;
				const gchar* _tmp22_;
				GUPnPProtocolInfo* _tmp23_;
				RygelDLNAProfile* _tmp24_;
				const gchar* _tmp25_;
				GeeArrayList* _tmp26_;
				GUPnPProtocolInfo* _tmp27_;
				_tmp17_ = gupnp_protocol_info_new ();
				protocol_info = _tmp17_;
				_tmp18_ = protocol_info;
				_tmp19_ = protocol;
				gupnp_protocol_info_set_protocol (_tmp18_, _tmp19_);
				_tmp20_ = protocol_info;
				_tmp21_ = profile;
				_tmp22_ = _tmp21_->mime;
				gupnp_protocol_info_set_mime_type (_tmp20_, _tmp22_);
				_tmp23_ = protocol_info;
				_tmp24_ = profile;
				_tmp25_ = _tmp24_->name;
				gupnp_protocol_info_set_dlna_profile (_tmp23_, _tmp25_);
				_tmp26_ = protocol_infos;
				_tmp27_ = protocol_info;
				if (!gee_abstract_collection_contains ((GeeAbstractCollection*) _tmp26_, _tmp27_)) {
					GeeArrayList* _tmp28_;
					GUPnPProtocolInfo* _tmp29_;
					_tmp28_ = protocol_infos;
					_tmp29_ = protocol_info;
					gee_abstract_list_insert ((GeeAbstractList*) _tmp28_, 0, _tmp29_);
				}
				_g_object_unref0 (protocol_info);
				_rygel_dlna_profile_unref0 (profile);
			}
		}
	}
	result = protocol_infos;
	_g_free0 (protocol);
	_g_object_unref0 (plugin);
	_g_object_unref0 (server);
	return result;
}

static RygelHTTPServer*
rygel_source_connection_manager_get_http_server (RygelSourceConnectionManager* self)
{
	RygelHTTPServer* server = NULL;
	RygelRootDevice* root_device = NULL;
	GUPnPRootDevice* _tmp0_;
	GUPnPRootDevice* _tmp1_;
	RygelHTTPServer* result = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	server = NULL;
	g_object_get ((GUPnPService*) self, "root-device", &_tmp0_, NULL);
	_tmp1_ = _tmp0_;
	root_device = G_TYPE_CHECK_INSTANCE_CAST (_tmp1_, RYGEL_TYPE_ROOT_DEVICE, RygelRootDevice);
	{
		GeeArrayList* _service_list = NULL;
		RygelRootDevice* _tmp2_;
		GeeArrayList* _tmp3_;
		GeeArrayList* _tmp4_;
		GeeArrayList* _tmp5_;
		gint _service_size = 0;
		GeeArrayList* _tmp6_;
		gint _tmp7_;
		gint _tmp8_;
		gint _service_index = 0;
		_tmp2_ = root_device;
		_tmp3_ = rygel_root_device_get_services (_tmp2_);
		_tmp4_ = _tmp3_;
		_tmp5_ = _g_object_ref0 (_tmp4_);
		_service_list = _tmp5_;
		_tmp6_ = _service_list;
		_tmp7_ = gee_abstract_collection_get_size ((GeeAbstractCollection*) _tmp6_);
		_tmp8_ = _tmp7_;
		_service_size = _tmp8_;
		_service_index = -1;
		while (TRUE) {
			GUPnPServiceInfo* service = NULL;
			GeeArrayList* _tmp9_;
			gpointer _tmp10_;
			GUPnPServiceInfo* _tmp11_;
			_service_index = _service_index + 1;
			if (!(_service_index < _service_size)) {
				break;
			}
			_tmp9_ = _service_list;
			_tmp10_ = gee_abstract_list_get ((GeeAbstractList*) _tmp9_, _service_index);
			service = (GUPnPServiceInfo*) _tmp10_;
			_tmp11_ = service;
			if (g_type_is_a (G_TYPE_FROM_INSTANCE ((GObject*) _tmp11_), RYGEL_TYPE_CONTENT_DIRECTORY)) {
				RygelContentDirectory* content_directory = NULL;
				GUPnPServiceInfo* _tmp12_;
				RygelContentDirectory* _tmp13_;
				RygelContentDirectory* _tmp14_;
				RygelHTTPServer* _tmp15_;
				RygelHTTPServer* _tmp16_;
				_tmp12_ = service;
				_tmp13_ = _g_object_ref0 (G_TYPE_CHECK_INSTANCE_CAST (_tmp12_, RYGEL_TYPE_CONTENT_DIRECTORY, RygelContentDirectory));
				content_directory = _tmp13_;
				_tmp14_ = content_directory;
				_tmp15_ = _tmp14_->http_server;
				_tmp16_ = _g_object_ref0 (_tmp15_);
				_g_object_unref0 (server);
				server = _tmp16_;
				_g_object_unref0 (content_directory);
			}
			_g_object_unref0 (service);
		}
		_g_object_unref0 (_service_list);
	}
	result = server;
	_g_object_unref0 (root_device);
	return result;
}

RygelSourceConnectionManager*
rygel_source_connection_manager_construct (GType object_type)
{
	RygelSourceConnectionManager * self = NULL;
	self = (RygelSourceConnectionManager*) rygel_connection_manager_construct (object_type);
	return self;
}

RygelSourceConnectionManager*
rygel_source_connection_manager_new (void)
{
	return rygel_source_connection_manager_construct (RYGEL_TYPE_SOURCE_CONNECTION_MANAGER);
}

static void
rygel_source_connection_manager_class_init (RygelSourceConnectionManagerClass * klass,
                                            gpointer klass_data)
{
	rygel_source_connection_manager_parent_class = g_type_class_peek_parent (klass);
	((GObjectClass *) klass)->constructed = (void (*) (GObject*)) rygel_source_connection_manager_real_constructed;
}

static void
rygel_source_connection_manager_instance_init (RygelSourceConnectionManager * self,
                                               gpointer klass)
{
}

/**
 * UPnP ConnectionManager service for serving end-points (MediaServer).
 */
GType
rygel_source_connection_manager_get_type (void)
{
	static volatile gsize rygel_source_connection_manager_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_source_connection_manager_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelSourceConnectionManagerClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_source_connection_manager_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelSourceConnectionManager), 0, (GInstanceInitFunc) rygel_source_connection_manager_instance_init, NULL };
		GType rygel_source_connection_manager_type_id;
		rygel_source_connection_manager_type_id = g_type_register_static (RYGEL_TYPE_CONNECTION_MANAGER, "RygelSourceConnectionManager", &g_define_type_info, 0);
		g_once_init_leave (&rygel_source_connection_manager_type_id__volatile, rygel_source_connection_manager_type_id);
	}
	return rygel_source_connection_manager_type_id__volatile;
}

