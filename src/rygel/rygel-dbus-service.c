/* rygel-dbus-service.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-dbus-service.vala, do not modify */

/*
 * Copyright (C) 2008,2010 Nokia Corporation.
 * Copyright (C) 2008 Zeeshan Ali (Khattak) <zeeshanak@gnome.org>.
 *
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib-object.h>
#include <rygel-core.h>
#include <gio/gio.h>
#include <glib.h>
#include <stdlib.h>
#include <string.h>
#include <glib/gi18n-lib.h>

#define RYGEL_TYPE_DBUS_SERVICE (rygel_dbus_service_get_type ())
#define RYGEL_DBUS_SERVICE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_DBUS_SERVICE, RygelDBusService))
#define RYGEL_DBUS_SERVICE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_DBUS_SERVICE, RygelDBusServiceClass))
#define RYGEL_IS_DBUS_SERVICE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_DBUS_SERVICE))
#define RYGEL_IS_DBUS_SERVICE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_DBUS_SERVICE))
#define RYGEL_DBUS_SERVICE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_DBUS_SERVICE, RygelDBusServiceClass))

typedef struct _RygelDBusService RygelDBusService;
typedef struct _RygelDBusServiceClass RygelDBusServiceClass;
typedef struct _RygelDBusServicePrivate RygelDBusServicePrivate;

#define RYGEL_TYPE_MAIN (rygel_main_get_type ())
#define RYGEL_MAIN(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MAIN, RygelMain))
#define RYGEL_MAIN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MAIN, RygelMainClass))
#define RYGEL_IS_MAIN(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MAIN))
#define RYGEL_IS_MAIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MAIN))
#define RYGEL_MAIN_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MAIN, RygelMainClass))

typedef struct _RygelMain RygelMain;
typedef struct _RygelMainClass RygelMainClass;
enum  {
	RYGEL_DBUS_SERVICE_0_PROPERTY,
	RYGEL_DBUS_SERVICE_NUM_PROPERTIES
};
static GParamSpec* rygel_dbus_service_properties[RYGEL_DBUS_SERVICE_NUM_PROPERTIES];
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))

struct _RygelDBusService {
	GObject parent_instance;
	RygelDBusServicePrivate * priv;
};

struct _RygelDBusServiceClass {
	GObjectClass parent_class;
};

struct _RygelDBusServicePrivate {
	RygelMain* main;
	guint name_id;
	guint connection_id;
};

static gint RygelDBusService_private_offset;
static gpointer rygel_dbus_service_parent_class = NULL;
static RygelDBusInterfaceIface * rygel_dbus_service_rygel_dbus_interface_parent_iface = NULL;

GType rygel_dbus_service_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelDBusService, g_object_unref)
guint rygel_dbus_service_register_object (void* object,
                                          GDBusConnection* connection,
                                          const gchar* path,
                                          GError** error);
GType rygel_main_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMain, g_object_unref)
RygelDBusService* rygel_dbus_service_new (RygelMain* main);
RygelDBusService* rygel_dbus_service_construct (GType object_type,
                                                RygelMain* main);
static void rygel_dbus_service_real_shutdown (RygelDBusInterface* base,
                                       GError** error);
void rygel_main_exit (RygelMain* self,
                      gint exit_code);
void rygel_dbus_service_publish (RygelDBusService* self);
static void rygel_dbus_service_on_bus_aquired (RygelDBusService* self,
                                        GDBusConnection* connection);
static void _rygel_dbus_service_on_bus_aquired_gbus_acquired_callback (GDBusConnection* connection,
                                                                const gchar* name,
                                                                gpointer self);
static void rygel_dbus_service_on_name_available (RygelDBusService* self,
                                           GDBusConnection* connection);
static void _rygel_dbus_service_on_name_available_gbus_name_acquired_callback (GDBusConnection* connection,
                                                                        const gchar* name,
                                                                        gpointer self);
static void rygel_dbus_service_on_name_lost (RygelDBusService* self,
                                      GDBusConnection* connection);
static void _rygel_dbus_service_on_name_lost_gbus_name_lost_callback (GDBusConnection* connection,
                                                               const gchar* name,
                                                               gpointer self);
void rygel_dbus_service_unpublish (RygelDBusService* self);
void rygel_main_dbus_available (RygelMain* self);
static void rygel_dbus_service_finalize (GObject * obj);
static void _dbus_rygel_dbus_service_shutdown (RygelDBusService* self,
                                        GVariant* _parameters_,
                                        GDBusMethodInvocation* invocation);
static void rygel_dbus_service_dbus_interface_method_call (GDBusConnection* connection,
                                                    const gchar* sender,
                                                    const gchar* object_path,
                                                    const gchar* interface_name,
                                                    const gchar* method_name,
                                                    GVariant* parameters,
                                                    GDBusMethodInvocation* invocation,
                                                    gpointer user_data);
static GVariant* rygel_dbus_service_dbus_interface_get_property (GDBusConnection* connection,
                                                          const gchar* sender,
                                                          const gchar* object_path,
                                                          const gchar* interface_name,
                                                          const gchar* property_name,
                                                          GError** error,
                                                          gpointer user_data);
static gboolean rygel_dbus_service_dbus_interface_set_property (GDBusConnection* connection,
                                                         const gchar* sender,
                                                         const gchar* object_path,
                                                         const gchar* interface_name,
                                                         const gchar* property_name,
                                                         GVariant* value,
                                                         GError** error,
                                                         gpointer user_data);
static void _rygel_dbus_service_unregister_object (gpointer user_data);

static const GDBusArgInfo * const _rygel_dbus_service_dbus_arg_info_shutdown_in[] = {NULL};
static const GDBusArgInfo * const _rygel_dbus_service_dbus_arg_info_shutdown_out[] = {NULL};
static const GDBusMethodInfo _rygel_dbus_service_dbus_method_info_shutdown = {-1, "Shutdown", (GDBusArgInfo **) (&_rygel_dbus_service_dbus_arg_info_shutdown_in), (GDBusArgInfo **) (&_rygel_dbus_service_dbus_arg_info_shutdown_out), NULL};
static const GDBusMethodInfo * const _rygel_dbus_service_dbus_method_info[] = {&_rygel_dbus_service_dbus_method_info_shutdown, NULL};
static const GDBusSignalInfo * const _rygel_dbus_service_dbus_signal_info[] = {NULL};
static const GDBusPropertyInfo * const _rygel_dbus_service_dbus_property_info[] = {NULL};
static const GDBusInterfaceInfo _rygel_dbus_service_dbus_interface_info = {-1, "org.gnome.Rygel1", (GDBusMethodInfo **) (&_rygel_dbus_service_dbus_method_info), (GDBusSignalInfo **) (&_rygel_dbus_service_dbus_signal_info), (GDBusPropertyInfo **) (&_rygel_dbus_service_dbus_property_info), NULL};
static const GDBusInterfaceVTable _rygel_dbus_service_dbus_interface_vtable = {rygel_dbus_service_dbus_interface_method_call, rygel_dbus_service_dbus_interface_get_property, rygel_dbus_service_dbus_interface_set_property};

static inline gpointer
rygel_dbus_service_get_instance_private (RygelDBusService* self)
{
	return G_STRUCT_MEMBER_P (self, RygelDBusService_private_offset);
}

static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

RygelDBusService*
rygel_dbus_service_construct (GType object_type,
                              RygelMain* main)
{
	RygelDBusService * self = NULL;
	RygelMain* _tmp0_;
	g_return_val_if_fail (main != NULL, NULL);
	self = (RygelDBusService*) g_object_new (object_type, NULL);
	_tmp0_ = _g_object_ref0 (main);
	_g_object_unref0 (self->priv->main);
	self->priv->main = _tmp0_;
	return self;
}

RygelDBusService*
rygel_dbus_service_new (RygelMain* main)
{
	return rygel_dbus_service_construct (RYGEL_TYPE_DBUS_SERVICE, main);
}

static void
rygel_dbus_service_real_shutdown (RygelDBusInterface* base,
                                  GError** error)
{
	RygelDBusService * self;
	RygelMain* _tmp0_;
	self = (RygelDBusService*) base;
	_tmp0_ = self->priv->main;
	rygel_main_exit (_tmp0_, 0);
}

static void
_rygel_dbus_service_on_bus_aquired_gbus_acquired_callback (GDBusConnection* connection,
                                                           const gchar* name,
                                                           gpointer self)
{
	rygel_dbus_service_on_bus_aquired ((RygelDBusService*) self, connection);
}

static void
_rygel_dbus_service_on_name_available_gbus_name_acquired_callback (GDBusConnection* connection,
                                                                   const gchar* name,
                                                                   gpointer self)
{
	rygel_dbus_service_on_name_available ((RygelDBusService*) self, connection);
}

static void
_rygel_dbus_service_on_name_lost_gbus_name_lost_callback (GDBusConnection* connection,
                                                          const gchar* name,
                                                          gpointer self)
{
	rygel_dbus_service_on_name_lost ((RygelDBusService*) self, connection);
}

void
rygel_dbus_service_publish (RygelDBusService* self)
{
	g_return_if_fail (self != NULL);
	self->priv->name_id = g_bus_own_name_with_closures (G_BUS_TYPE_SESSION, RYGEL_DBUS_INTERFACE_SERVICE_NAME, G_BUS_NAME_OWNER_FLAGS_NONE, (GClosure*) ((_rygel_dbus_service_on_bus_aquired_gbus_acquired_callback == NULL) ? NULL : g_cclosure_new ((GCallback) _rygel_dbus_service_on_bus_aquired_gbus_acquired_callback, g_object_ref (self), (GClosureNotify) g_object_unref)), (GClosure*) ((_rygel_dbus_service_on_name_available_gbus_name_acquired_callback == NULL) ? NULL : g_cclosure_new ((GCallback) _rygel_dbus_service_on_name_available_gbus_name_acquired_callback, g_object_ref (self), (GClosureNotify) g_object_unref)), (GClosure*) ((_rygel_dbus_service_on_name_lost_gbus_name_lost_callback == NULL) ? NULL : g_cclosure_new ((GCallback) _rygel_dbus_service_on_name_lost_gbus_name_lost_callback, g_object_ref (self), (GClosureNotify) g_object_unref)));
}

void
rygel_dbus_service_unpublish (RygelDBusService* self)
{
	GError* _inner_error0_ = NULL;
	g_return_if_fail (self != NULL);
	if (self->priv->connection_id != ((guint) 0)) {
		{
			GDBusConnection* connection = NULL;
			GDBusConnection* _tmp0_;
			GDBusConnection* _tmp1_;
			_tmp0_ = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &_inner_error0_);
			connection = _tmp0_;
			if (G_UNLIKELY (_inner_error0_ != NULL)) {
				if (_inner_error0_->domain == G_IO_ERROR) {
					goto __catch3_g_io_error;
				}
				g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
				g_clear_error (&_inner_error0_);
				return;
			}
			_tmp1_ = connection;
			g_dbus_connection_unregister_object (_tmp1_, self->priv->connection_id);
			_g_object_unref0 (connection);
		}
		goto __finally3;
		__catch3_g_io_error:
		{
			GError* _error_ = NULL;
			_error_ = _inner_error0_;
			_inner_error0_ = NULL;
			_g_error_free0 (_error_);
		}
		__finally3:
		if (G_UNLIKELY (_inner_error0_ != NULL)) {
			g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
			g_clear_error (&_inner_error0_);
			return;
		}
	}
	if (self->priv->name_id != ((guint) 0)) {
		g_bus_unown_name (self->priv->name_id);
	}
}

static void
rygel_dbus_service_on_bus_aquired (RygelDBusService* self,
                                   GDBusConnection* connection)
{
	GError* _inner_error0_ = NULL;
	g_return_if_fail (self != NULL);
	g_return_if_fail (connection != NULL);
	{
		guint _tmp0_ = 0U;
		guint _tmp1_;
		_tmp1_ = rygel_dbus_service_register_object (self, connection, RYGEL_DBUS_INTERFACE_OBJECT_PATH, &_inner_error0_);
		_tmp0_ = _tmp1_;
		if (G_UNLIKELY (_inner_error0_ != NULL)) {
			goto __catch4_g_error;
		}
		self->priv->connection_id = _tmp0_;
	}
	goto __finally4;
	__catch4_g_error:
	{
		GError* _error_ = NULL;
		_error_ = _inner_error0_;
		_inner_error0_ = NULL;
		_g_error_free0 (_error_);
	}
	__finally4:
	if (G_UNLIKELY (_inner_error0_ != NULL)) {
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
		g_clear_error (&_inner_error0_);
		return;
	}
}

static void
rygel_dbus_service_on_name_available (RygelDBusService* self,
                                      GDBusConnection* connection)
{
	RygelMain* _tmp0_;
	g_return_if_fail (self != NULL);
	g_return_if_fail (connection != NULL);
	_tmp0_ = self->priv->main;
	rygel_main_dbus_available (_tmp0_);
}

static void
rygel_dbus_service_on_name_lost (RygelDBusService* self,
                                 GDBusConnection* connection)
{
	RygelMain* _tmp1_;
	g_return_if_fail (self != NULL);
	if (connection == NULL) {
		RygelMain* _tmp0_;
		_tmp0_ = self->priv->main;
		rygel_main_dbus_available (_tmp0_);
		return;
	}
	g_message ("rygel-dbus-service.vala:81: %s", _ ("Another instance of Rygel is already running. Not starting."));
	_tmp1_ = self->priv->main;
	rygel_main_exit (_tmp1_, -15);
}

static void
rygel_dbus_service_class_init (RygelDBusServiceClass * klass,
                               gpointer klass_data)
{
	rygel_dbus_service_parent_class = g_type_class_peek_parent (klass);
	g_type_class_adjust_private_offset (klass, &RygelDBusService_private_offset);
	G_OBJECT_CLASS (klass)->finalize = rygel_dbus_service_finalize;
}

static void
rygel_dbus_service_rygel_dbus_interface_interface_init (RygelDBusInterfaceIface * iface,
                                                        gpointer iface_data)
{
	rygel_dbus_service_rygel_dbus_interface_parent_iface = g_type_interface_peek_parent (iface);
	iface->shutdown = (void (*) (RygelDBusInterface*, GError**)) rygel_dbus_service_real_shutdown;
}

static void
rygel_dbus_service_instance_init (RygelDBusService * self,
                                  gpointer klass)
{
	self->priv = rygel_dbus_service_get_instance_private (self);
}

static void
rygel_dbus_service_finalize (GObject * obj)
{
	RygelDBusService * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, RYGEL_TYPE_DBUS_SERVICE, RygelDBusService);
	_g_object_unref0 (self->priv->main);
	G_OBJECT_CLASS (rygel_dbus_service_parent_class)->finalize (obj);
}

GType
rygel_dbus_service_get_type (void)
{
	static volatile gsize rygel_dbus_service_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_dbus_service_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelDBusServiceClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_dbus_service_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelDBusService), 0, (GInstanceInitFunc) rygel_dbus_service_instance_init, NULL };
		static const GInterfaceInfo rygel_dbus_interface_info = { (GInterfaceInitFunc) rygel_dbus_service_rygel_dbus_interface_interface_init, (GInterfaceFinalizeFunc) NULL, NULL};
		GType rygel_dbus_service_type_id;
		rygel_dbus_service_type_id = g_type_register_static (G_TYPE_OBJECT, "RygelDBusService", &g_define_type_info, 0);
		g_type_add_interface_static (rygel_dbus_service_type_id, RYGEL_TYPE_DBUS_INTERFACE, &rygel_dbus_interface_info);
		g_type_set_qdata (rygel_dbus_service_type_id, g_quark_from_static_string ("vala-dbus-register-object"), (void*) rygel_dbus_service_register_object);
		RygelDBusService_private_offset = g_type_add_instance_private (rygel_dbus_service_type_id, sizeof (RygelDBusServicePrivate));
		g_once_init_leave (&rygel_dbus_service_type_id__volatile, rygel_dbus_service_type_id);
	}
	return rygel_dbus_service_type_id__volatile;
}

static void
_dbus_rygel_dbus_service_shutdown (RygelDBusService* self,
                                   GVariant* _parameters_,
                                   GDBusMethodInvocation* invocation)
{
	GError* error = NULL;
	GVariantIter _arguments_iter;
	GDBusMessage* _reply_message = NULL;
	GVariant* _reply;
	GVariantBuilder _reply_builder;
	g_variant_iter_init (&_arguments_iter, _parameters_);
	rygel_dbus_interface_shutdown (self, &error);
	if (error) {
		g_dbus_method_invocation_return_gerror (invocation, error);
		g_error_free (error);
		return;
	}
	_reply_message = g_dbus_message_new_method_reply (g_dbus_method_invocation_get_message (invocation));
	g_variant_builder_init (&_reply_builder, G_VARIANT_TYPE_TUPLE);
	_reply = g_variant_builder_end (&_reply_builder);
	g_dbus_message_set_body (_reply_message, _reply);
	g_dbus_connection_send_message (g_dbus_method_invocation_get_connection (invocation), _reply_message, G_DBUS_SEND_MESSAGE_FLAGS_NONE, NULL, NULL);
	g_object_unref (invocation);
	g_object_unref (_reply_message);
}

static void
rygel_dbus_service_dbus_interface_method_call (GDBusConnection* connection,
                                               const gchar* sender,
                                               const gchar* object_path,
                                               const gchar* interface_name,
                                               const gchar* method_name,
                                               GVariant* parameters,
                                               GDBusMethodInvocation* invocation,
                                               gpointer user_data)
{
	gpointer* data;
	gpointer object;
	data = user_data;
	object = data[0];
	if (strcmp (method_name, "Shutdown") == 0) {
		_dbus_rygel_dbus_service_shutdown (object, parameters, invocation);
	} else {
		g_object_unref (invocation);
	}
}

static GVariant*
rygel_dbus_service_dbus_interface_get_property (GDBusConnection* connection,
                                                const gchar* sender,
                                                const gchar* object_path,
                                                const gchar* interface_name,
                                                const gchar* property_name,
                                                GError** error,
                                                gpointer user_data)
{
	gpointer* data;
	gpointer object;
	data = user_data;
	object = data[0];
	return NULL;
}

static gboolean
rygel_dbus_service_dbus_interface_set_property (GDBusConnection* connection,
                                                const gchar* sender,
                                                const gchar* object_path,
                                                const gchar* interface_name,
                                                const gchar* property_name,
                                                GVariant* value,
                                                GError** error,
                                                gpointer user_data)
{
	gpointer* data;
	gpointer object;
	data = user_data;
	object = data[0];
	return FALSE;
}

guint
rygel_dbus_service_register_object (gpointer object,
                                    GDBusConnection* connection,
                                    const gchar* path,
                                    GError** error)
{
	guint result;
	gpointer *data;
	data = g_new (gpointer, 3);
	data[0] = g_object_ref (object);
	data[1] = g_object_ref (connection);
	data[2] = g_strdup (path);
	result = g_dbus_connection_register_object (connection, path, (GDBusInterfaceInfo *) (&_rygel_dbus_service_dbus_interface_info), &_rygel_dbus_service_dbus_interface_vtable, data, _rygel_dbus_service_unregister_object, error);
	if (!result) {
		return 0;
	}
	return result;
}

static void
_rygel_dbus_service_unregister_object (gpointer user_data)
{
	gpointer* data;
	data = user_data;
	g_object_unref (data[0]);
	g_object_unref (data[1]);
	g_free (data[2]);
	g_free (data);
}

