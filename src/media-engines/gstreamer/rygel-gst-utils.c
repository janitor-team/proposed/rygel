/* rygel-gst-utils.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-gst-utils.vala, do not modify */

/*
 * Copyright (C) 2009 Nokia Corporation.
 *
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *                               <zeeshan.ali@nokia.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <gst/gst.h>
#include <glib/gi18n-lib.h>
#include <libsoup/soup.h>
#include <gio/gio.h>
#include <gst/pbutils/pbutils.h>
#include <gobject/gvaluecollector.h>

#define RYGEL_TYPE_GST_UTILS (rygel_gst_utils_get_type ())
#define RYGEL_GST_UTILS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_GST_UTILS, RygelGstUtils))
#define RYGEL_GST_UTILS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_GST_UTILS, RygelGstUtilsClass))
#define RYGEL_IS_GST_UTILS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_GST_UTILS))
#define RYGEL_IS_GST_UTILS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_GST_UTILS))
#define RYGEL_GST_UTILS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_GST_UTILS, RygelGstUtilsClass))

typedef struct _RygelGstUtils RygelGstUtils;
typedef struct _RygelGstUtilsClass RygelGstUtilsClass;
typedef struct _RygelGstUtilsPrivate RygelGstUtilsPrivate;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_regex_unref0(var) ((var == NULL) ? NULL : (var = (g_regex_unref (var), NULL)))
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))
#define _g_hash_table_unref0(var) ((var == NULL) ? NULL : (var = (g_hash_table_unref (var), NULL)))
#define __vala_SoupURI_free0(var) ((var == NULL) ? NULL : (var = (_vala_SoupURI_free (var), NULL)))
#define _gst_caps_unref0(var) ((var == NULL) ? NULL : (var = (gst_caps_unref (var), NULL)))
typedef struct _RygelParamSpecGstUtils RygelParamSpecGstUtils;

typedef enum  {
	RYGEL_GST_ERROR_MISSING_PLUGIN,
	RYGEL_GST_ERROR_LINK
} RygelGstError;
#define RYGEL_GST_ERROR rygel_gst_error_quark ()
struct _RygelGstUtils {
	GTypeInstance parent_instance;
	volatile int ref_count;
	RygelGstUtilsPrivate * priv;
};

struct _RygelGstUtilsClass {
	GTypeClass parent_class;
	void (*finalize) (RygelGstUtils *self);
};

struct _RygelParamSpecGstUtils {
	GParamSpec parent_instance;
};

static gpointer rygel_gst_utils_parent_class = NULL;

GQuark rygel_gst_error_quark (void);
gpointer rygel_gst_utils_ref (gpointer instance);
void rygel_gst_utils_unref (gpointer instance);
GParamSpec* rygel_param_spec_gst_utils (const gchar* name,
                                        const gchar* nick,
                                        const gchar* blurb,
                                        GType object_type,
                                        GParamFlags flags);
void rygel_value_set_gst_utils (GValue* value,
                                gpointer v_object);
void rygel_value_take_gst_utils (GValue* value,
                                 gpointer v_object);
gpointer rygel_value_get_gst_utils (const GValue* value);
GType rygel_gst_utils_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelGstUtils, rygel_gst_utils_unref)
GstElement* rygel_gst_utils_create_element (const gchar* factoryname,
                                            const gchar* name,
                                            GError** error);
GstElement* rygel_gst_utils_create_source_for_uri (const gchar* uri);
static inline void _dynamic_set_title5 (GstElement* obj,
                          gint value);
static inline void _dynamic_set_device6 (GstElement* obj,
                           gchar* value);
static void _vala_SoupURI_free (SoupURI* self);
static inline void _dynamic_set_blocksize7 (GstElement* obj,
                              glong value);
static inline void _dynamic_set_tcp_timeout8 (GstElement* obj,
                                gint64 value);
void rygel_gst_utils_dump_encoding_profile (GstEncodingProfile* profile,
                                            gint indent);
GstElement* rygel_gst_utils_get_rtp_depayloader (GstCaps* caps);
static gboolean rygel_gst_utils_need_rtp_depayloader (GstCaps* caps);
static void _g_object_unref0_ (gpointer var);
static inline void _g_list_free__g_object_unref0_ (GList* self);
RygelGstUtils* rygel_gst_utils_construct (GType object_type);
static void rygel_gst_utils_finalize (RygelGstUtils * obj);

GQuark
rygel_gst_error_quark (void)
{
	return g_quark_from_static_string ("rygel-gst-error-quark");
}

GstElement*
rygel_gst_utils_create_element (const gchar* factoryname,
                                const gchar* name,
                                GError** error)
{
	GstElement* element = NULL;
	GstElement* _tmp0_;
	GstElement* _tmp1_;
	GError* _inner_error0_ = NULL;
	GstElement* result = NULL;
	g_return_val_if_fail (factoryname != NULL, NULL);
	_tmp0_ = gst_element_factory_make (factoryname, name);
	if (_tmp0_ != NULL) {
		g_object_ref_sink (_tmp0_);
	}
	element = _tmp0_;
	_tmp1_ = element;
	if (_tmp1_ == NULL) {
		GError* _tmp2_;
		_tmp2_ = g_error_new (RYGEL_GST_ERROR, RYGEL_GST_ERROR_MISSING_PLUGIN, _ ("Required element %s missing"), factoryname);
		_inner_error0_ = _tmp2_;
		g_propagate_error (error, _inner_error0_);
		_g_object_unref0 (element);
		return NULL;
	}
	result = element;
	return result;
}

static gchar*
string_replace (const gchar* self,
                const gchar* old,
                const gchar* replacement)
{
	gboolean _tmp0_ = FALSE;
	gboolean _tmp1_ = FALSE;
	GError* _inner_error0_ = NULL;
	gchar* result = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	g_return_val_if_fail (old != NULL, NULL);
	g_return_val_if_fail (replacement != NULL, NULL);
	if ((*((gchar*) self)) == '\0') {
		_tmp1_ = TRUE;
	} else {
		_tmp1_ = (*((gchar*) old)) == '\0';
	}
	if (_tmp1_) {
		_tmp0_ = TRUE;
	} else {
		_tmp0_ = g_strcmp0 (old, replacement) == 0;
	}
	if (_tmp0_) {
		gchar* _tmp2_;
		_tmp2_ = g_strdup (self);
		result = _tmp2_;
		return result;
	}
	{
		GRegex* regex = NULL;
		gchar* _tmp3_;
		gchar* _tmp4_;
		GRegex* _tmp5_;
		GRegex* _tmp6_;
		gchar* _tmp7_ = NULL;
		GRegex* _tmp8_;
		gchar* _tmp9_;
		gchar* _tmp10_;
		_tmp3_ = g_regex_escape_string (old, -1);
		_tmp4_ = _tmp3_;
		_tmp5_ = g_regex_new (_tmp4_, 0, 0, &_inner_error0_);
		_tmp6_ = _tmp5_;
		_g_free0 (_tmp4_);
		regex = _tmp6_;
		if (G_UNLIKELY (_inner_error0_ != NULL)) {
			if (_inner_error0_->domain == G_REGEX_ERROR) {
				goto __catch3_g_regex_error;
			}
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
			g_clear_error (&_inner_error0_);
			return NULL;
		}
		_tmp8_ = regex;
		_tmp9_ = g_regex_replace_literal (_tmp8_, self, (gssize) -1, 0, replacement, 0, &_inner_error0_);
		_tmp7_ = _tmp9_;
		if (G_UNLIKELY (_inner_error0_ != NULL)) {
			_g_regex_unref0 (regex);
			if (_inner_error0_->domain == G_REGEX_ERROR) {
				goto __catch3_g_regex_error;
			}
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
			g_clear_error (&_inner_error0_);
			return NULL;
		}
		_tmp10_ = _tmp7_;
		_tmp7_ = NULL;
		result = _tmp10_;
		_g_free0 (_tmp7_);
		_g_regex_unref0 (regex);
		return result;
	}
	goto __finally3;
	__catch3_g_regex_error:
	{
		GError* e = NULL;
		e = _inner_error0_;
		_inner_error0_ = NULL;
		g_assert_not_reached ();
		_g_error_free0 (e);
	}
	__finally3:
	g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
	g_clear_error (&_inner_error0_);
	return NULL;
}

static inline void
_dynamic_set_title5 (GstElement* obj,
                     gint value)
{
	g_object_set (obj, "title", value, NULL);
}

static inline void
_dynamic_set_device6 (GstElement* obj,
                      gchar* value)
{
	g_object_set (obj, "device", value, NULL);
}

static void
_vala_SoupURI_free (SoupURI* self)
{
	g_boxed_free (soup_uri_get_type (), self);
}

static inline void
_dynamic_set_blocksize7 (GstElement* obj,
                         glong value)
{
	g_object_set (obj, "blocksize", value, NULL);
}

static inline void
_dynamic_set_tcp_timeout8 (GstElement* obj,
                           gint64 value)
{
	g_object_set (obj, "tcp-timeout", value, NULL);
}

GstElement*
rygel_gst_utils_create_source_for_uri (const gchar* uri)
{
	GError* _inner_error0_ = NULL;
	GstElement* result = NULL;
	g_return_val_if_fail (uri != NULL, NULL);
	{
		GstElement* src = NULL;
		GstElement* _tmp35_;
		GObjectClass* _tmp36_;
		GParamSpec* _tmp37_;
		GstElement* _tmp39_;
		GObjectClass* _tmp40_;
		GParamSpec* _tmp41_;
		if (g_str_has_prefix (uri, "gst-launch://")) {
			gchar* description = NULL;
			gchar* _tmp0_;
			const gchar* _tmp1_;
			gchar* _tmp2_;
			GstElement* _tmp3_ = NULL;
			const gchar* _tmp4_;
			GstElement* _tmp5_;
			GstElement* _tmp6_;
			_tmp0_ = string_replace (uri, "gst-launch://", "");
			description = _tmp0_;
			_tmp1_ = description;
			_tmp2_ = soup_uri_decode (_tmp1_);
			_g_free0 (description);
			description = _tmp2_;
			_tmp4_ = description;
			_tmp5_ = gst_parse_bin_from_description_full (_tmp4_, TRUE, NULL, GST_PARSE_FLAG_NONE, &_inner_error0_);
			_tmp3_ = _tmp5_;
			if (G_UNLIKELY (_inner_error0_ != NULL)) {
				_g_free0 (description);
				_g_object_unref0 (src);
				goto __catch2_g_error;
			}
			_tmp6_ = _tmp3_;
			_tmp3_ = NULL;
			if (_tmp6_ != NULL) {
				g_object_ref_sink (_tmp6_);
			}
			_g_object_unref0 (src);
			src = _tmp6_;
			_g_object_unref0 (_tmp3_);
			_g_free0 (description);
		} else {
			if (g_str_has_prefix (uri, "dvd://")) {
				GstElement* _tmp7_;
				GstElement* _tmp8_;
				SoupURI* tmp = NULL;
				SoupURI* _tmp9_;
				GHashTable* query = NULL;
				SoupURI* _tmp10_;
				const gchar* _tmp11_;
				GHashTable* _tmp12_;
				GHashTable* _tmp13_;
				GstElement* _tmp17_;
				SoupURI* _tmp18_;
				const gchar* _tmp19_;
				gchar* _tmp20_;
				gchar* _tmp21_;
				_tmp7_ = gst_element_factory_make ("dvdreadsrc", "dvdreadsrc");
				if (_tmp7_ != NULL) {
					g_object_ref_sink (_tmp7_);
				}
				_g_object_unref0 (src);
				src = _tmp7_;
				_tmp8_ = src;
				if (_tmp8_ == NULL) {
					g_warning ("rygel-gst-utils.vala:58: %s", _ ("GStreamer element “dvdreadsrc” not found. DVD support does not work"));
					result = NULL;
					_g_object_unref0 (src);
					return result;
				}
				_tmp9_ = soup_uri_new (uri);
				tmp = _tmp9_;
				_tmp10_ = tmp;
				_tmp11_ = _tmp10_->query;
				_tmp12_ = soup_form_decode (_tmp11_);
				query = _tmp12_;
				_tmp13_ = query;
				if (g_hash_table_contains (_tmp13_, "title")) {
					GstElement* _tmp14_;
					GHashTable* _tmp15_;
					gconstpointer _tmp16_;
					_tmp14_ = src;
					_tmp15_ = query;
					_tmp16_ = g_hash_table_lookup (_tmp15_, "title");
					_dynamic_set_title5 (_tmp14_, atoi ((const gchar*) _tmp16_));
				}
				_tmp17_ = src;
				_tmp18_ = tmp;
				_tmp19_ = _tmp18_->path;
				_tmp20_ = soup_uri_decode (_tmp19_);
				_tmp21_ = _tmp20_;
				_dynamic_set_device6 (_tmp17_, _tmp21_);
				_g_free0 (_tmp21_);
				_g_hash_table_unref0 (query);
				__vala_SoupURI_free0 (tmp);
			} else {
				GFile* file = NULL;
				GFile* _tmp22_;
				gchar* path = NULL;
				GFile* _tmp23_;
				gchar* _tmp24_;
				const gchar* _tmp25_;
				_tmp22_ = g_file_new_for_uri (uri);
				file = _tmp22_;
				_tmp23_ = file;
				_tmp24_ = g_file_get_path (_tmp23_);
				path = _tmp24_;
				_tmp25_ = path;
				if (_tmp25_ != NULL) {
					gchar* _tmp26_ = NULL;
					const gchar* _tmp27_;
					gchar* _tmp28_;
					GstElement* _tmp29_ = NULL;
					GstElement* _tmp30_;
					GstElement* _tmp31_;
					_tmp27_ = path;
					_tmp28_ = g_filename_to_uri (_tmp27_, NULL, &_inner_error0_);
					_tmp26_ = _tmp28_;
					if (G_UNLIKELY (_inner_error0_ != NULL)) {
						_g_free0 (path);
						_g_object_unref0 (file);
						_g_object_unref0 (src);
						goto __catch2_g_error;
					}
					_tmp30_ = gst_element_make_from_uri (GST_URI_SRC, _tmp26_, NULL, &_inner_error0_);
					_tmp29_ = _tmp30_;
					if (G_UNLIKELY (_inner_error0_ != NULL)) {
						_g_free0 (_tmp26_);
						_g_free0 (path);
						_g_object_unref0 (file);
						_g_object_unref0 (src);
						goto __catch2_g_error;
					}
					_tmp31_ = _tmp29_;
					_tmp29_ = NULL;
					if (_tmp31_ != NULL) {
						g_object_ref_sink (_tmp31_);
					}
					_g_object_unref0 (src);
					src = _tmp31_;
					_g_object_unref0 (_tmp29_);
					_g_free0 (_tmp26_);
				} else {
					GstElement* _tmp32_ = NULL;
					GstElement* _tmp33_;
					GstElement* _tmp34_;
					_tmp33_ = gst_element_make_from_uri (GST_URI_SRC, uri, NULL, &_inner_error0_);
					_tmp32_ = _tmp33_;
					if (G_UNLIKELY (_inner_error0_ != NULL)) {
						_g_free0 (path);
						_g_object_unref0 (file);
						_g_object_unref0 (src);
						goto __catch2_g_error;
					}
					_tmp34_ = _tmp32_;
					_tmp32_ = NULL;
					if (_tmp34_ != NULL) {
						g_object_ref_sink (_tmp34_);
					}
					_g_object_unref0 (src);
					src = _tmp34_;
					_g_object_unref0 (_tmp32_);
				}
				_g_free0 (path);
				_g_object_unref0 (file);
			}
		}
		_tmp35_ = src;
		_tmp36_ = G_OBJECT_GET_CLASS ((GObject*) _tmp35_);
		_tmp37_ = g_object_class_find_property (_tmp36_, "blocksize");
		if (_tmp37_ != NULL) {
			GstElement* _tmp38_;
			_tmp38_ = src;
			_dynamic_set_blocksize7 (_tmp38_, (glong) 65536);
		}
		_tmp39_ = src;
		_tmp40_ = G_OBJECT_GET_CLASS ((GObject*) _tmp39_);
		_tmp41_ = g_object_class_find_property (_tmp40_, "tcp-timeout");
		if (_tmp41_ != NULL) {
			GstElement* _tmp42_;
			_tmp42_ = src;
			_dynamic_set_tcp_timeout8 (_tmp42_, (gint64) 60000000);
		}
		result = src;
		return result;
	}
	goto __finally2;
	__catch2_g_error:
	{
		GError* _error_ = NULL;
		_error_ = _inner_error0_;
		_inner_error0_ = NULL;
		result = NULL;
		_g_error_free0 (_error_);
		return result;
	}
	__finally2:
	g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
	g_clear_error (&_inner_error0_);
	return NULL;
}

static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

void
rygel_gst_utils_dump_encoding_profile (GstEncodingProfile* profile,
                                       gint indent)
{
	gchar* indent_s = NULL;
	gchar* _tmp0_;
	const gchar* _tmp1_;
	gchar* _tmp2_;
	gchar* _tmp3_;
	const gchar* _tmp4_;
	const gchar* _tmp5_;
	gchar* _tmp6_;
	gchar* _tmp7_;
	GstCaps* _tmp8_;
	GstCaps* _tmp9_;
	gchar* _tmp10_;
	gchar* _tmp11_;
	GstCaps* _tmp12_;
	GstCaps* _tmp13_;
	gboolean _tmp14_;
	g_return_if_fail (profile != NULL);
	_tmp0_ = g_strnfill ((gsize) indent, ' ');
	indent_s = _tmp0_;
	_tmp1_ = indent_s;
	_tmp2_ = g_strconcat (_tmp1_, "%s:", NULL);
	_tmp3_ = _tmp2_;
	_tmp4_ = gst_encoding_profile_get_name (profile);
	g_debug (_tmp3_, _tmp4_);
	_g_free0 (_tmp3_);
	_tmp5_ = indent_s;
	_tmp6_ = g_strconcat (_tmp5_, "  Format: %s", NULL);
	_tmp7_ = _tmp6_;
	_tmp8_ = gst_encoding_profile_get_format (profile);
	_tmp9_ = _tmp8_;
	_tmp10_ = gst_caps_to_string (_tmp9_);
	_tmp11_ = _tmp10_;
	g_debug (_tmp7_, _tmp11_);
	_g_free0 (_tmp11_);
	_gst_caps_unref0 (_tmp9_);
	_g_free0 (_tmp7_);
	_tmp12_ = gst_encoding_profile_get_restriction (profile);
	_tmp13_ = _tmp12_;
	_tmp14_ = _tmp13_ != NULL;
	_gst_caps_unref0 (_tmp13_);
	if (_tmp14_) {
		const gchar* _tmp15_;
		gchar* _tmp16_;
		gchar* _tmp17_;
		GstCaps* _tmp18_;
		GstCaps* _tmp19_;
		gchar* _tmp20_;
		gchar* _tmp21_;
		_tmp15_ = indent_s;
		_tmp16_ = g_strconcat (_tmp15_, "  Restriction: %s", NULL);
		_tmp17_ = _tmp16_;
		_tmp18_ = gst_encoding_profile_get_restriction (profile);
		_tmp19_ = _tmp18_;
		_tmp20_ = gst_caps_to_string (_tmp19_);
		_tmp21_ = _tmp20_;
		g_debug (_tmp17_, _tmp21_);
		_g_free0 (_tmp21_);
		_gst_caps_unref0 (_tmp19_);
		_g_free0 (_tmp17_);
	}
	if (G_TYPE_CHECK_INSTANCE_TYPE (profile, gst_encoding_container_profile_get_type ())) {
		GstEncodingContainerProfile* container = NULL;
		GstEncodingContainerProfile* _tmp22_;
		GstEncodingContainerProfile* _tmp23_;
		GList* _tmp24_;
		_tmp22_ = _g_object_ref0 (G_TYPE_CHECK_INSTANCE_TYPE (profile, gst_encoding_container_profile_get_type ()) ? ((GstEncodingContainerProfile*) profile) : NULL);
		container = _tmp22_;
		_tmp23_ = container;
		_tmp24_ = gst_encoding_container_profile_get_profiles (_tmp23_);
		{
			GList* subprofile_collection = NULL;
			GList* subprofile_it = NULL;
			subprofile_collection = _tmp24_;
			for (subprofile_it = subprofile_collection; subprofile_it != NULL; subprofile_it = subprofile_it->next) {
				GstEncodingProfile* _tmp25_;
				GstEncodingProfile* subprofile = NULL;
				_tmp25_ = _g_object_ref0 ((GstEncodingProfile*) subprofile_it->data);
				subprofile = _tmp25_;
				{
					GstEncodingProfile* _tmp26_;
					_tmp26_ = subprofile;
					rygel_gst_utils_dump_encoding_profile (_tmp26_, indent + 4);
					_g_object_unref0 (subprofile);
				}
			}
		}
		_g_object_unref0 (container);
	}
	_g_free0 (indent_s);
}

static void
_g_object_unref0_ (gpointer var)
{
	(var == NULL) ? NULL : (var = (g_object_unref (var), NULL));
}

static inline void
_g_list_free__g_object_unref0_ (GList* self)
{
	g_list_free_full (self, (GDestroyNotify) _g_object_unref0_);
}

GstElement*
rygel_gst_utils_get_rtp_depayloader (GstCaps* caps)
{
	GList* features = NULL;
	GList* _tmp0_;
	GList* _tmp1_;
	GList* _tmp2_;
	GList* _tmp3_;
	GList* _tmp4_;
	gconstpointer _tmp5_;
	gchar* _tmp6_;
	gchar* _tmp7_;
	gboolean _tmp8_;
	GstElement* result = NULL;
	g_return_val_if_fail (caps != NULL, NULL);
	if (!rygel_gst_utils_need_rtp_depayloader (caps)) {
		result = NULL;
		return result;
	}
	_tmp0_ = gst_element_factory_list_get_elements (GST_ELEMENT_FACTORY_TYPE_DEPAYLOADER, GST_RANK_NONE);
	features = _tmp0_;
	_tmp1_ = features;
	_tmp2_ = gst_element_factory_list_filter (_tmp1_, caps, GST_PAD_SINK, FALSE);
	(features == NULL) ? NULL : (features = (_g_list_free__g_object_unref0_ (features), NULL));
	features = _tmp2_;
	_tmp3_ = features;
	if (_tmp3_ == NULL) {
		result = NULL;
		(features == NULL) ? NULL : (features = (_g_list_free__g_object_unref0_ (features), NULL));
		return result;
	}
	_tmp4_ = features;
	_tmp5_ = _tmp4_->data;
	_tmp6_ = gst_object_get_name ((GstObject*) ((GstElementFactory*) _tmp5_));
	_tmp7_ = _tmp6_;
	_tmp8_ = g_strcmp0 (_tmp7_, "rtpdepay") == 0;
	_g_free0 (_tmp7_);
	if (_tmp8_) {
		GList* _tmp9_;
		GList* _tmp10_;
		_tmp9_ = features;
		_tmp10_ = _tmp9_->next;
		if (_tmp10_ != NULL) {
			GList* _tmp11_;
			GList* _tmp12_;
			gconstpointer _tmp13_;
			GstElement* _tmp14_;
			_tmp11_ = features;
			_tmp12_ = _tmp11_->next;
			_tmp13_ = _tmp12_->data;
			_tmp14_ = gst_element_factory_create ((GstElementFactory*) _tmp13_, NULL);
			if (_tmp14_ != NULL) {
				g_object_ref_sink (_tmp14_);
			}
			result = _tmp14_;
			(features == NULL) ? NULL : (features = (_g_list_free__g_object_unref0_ (features), NULL));
			return result;
		}
		result = NULL;
		(features == NULL) ? NULL : (features = (_g_list_free__g_object_unref0_ (features), NULL));
		return result;
	} else {
		GList* _tmp15_;
		gconstpointer _tmp16_;
		GstElement* _tmp17_;
		_tmp15_ = features;
		_tmp16_ = _tmp15_->data;
		_tmp17_ = gst_element_factory_create ((GstElementFactory*) _tmp16_, NULL);
		if (_tmp17_ != NULL) {
			g_object_ref_sink (_tmp17_);
		}
		result = _tmp17_;
		(features == NULL) ? NULL : (features = (_g_list_free__g_object_unref0_ (features), NULL));
		return result;
	}
	(features == NULL) ? NULL : (features = (_g_list_free__g_object_unref0_ (features), NULL));
}

static gboolean
rygel_gst_utils_need_rtp_depayloader (GstCaps* caps)
{
	GstStructure* structure = NULL;
	GstStructure* _tmp0_;
	const gchar* _tmp1_;
	gboolean result = FALSE;
	g_return_val_if_fail (caps != NULL, FALSE);
	_tmp0_ = gst_caps_get_structure (caps, (guint) 0);
	structure = _tmp0_;
	_tmp1_ = gst_structure_get_name (structure);
	result = g_strcmp0 (_tmp1_, "application/x-rtp") == 0;
	return result;
}

RygelGstUtils*
rygel_gst_utils_construct (GType object_type)
{
	RygelGstUtils* self = NULL;
	self = (RygelGstUtils*) g_type_create_instance (object_type);
	return self;
}

static void
rygel_value_gst_utils_init (GValue* value)
{
	value->data[0].v_pointer = NULL;
}

static void
rygel_value_gst_utils_free_value (GValue* value)
{
	if (value->data[0].v_pointer) {
		rygel_gst_utils_unref (value->data[0].v_pointer);
	}
}

static void
rygel_value_gst_utils_copy_value (const GValue* src_value,
                                  GValue* dest_value)
{
	if (src_value->data[0].v_pointer) {
		dest_value->data[0].v_pointer = rygel_gst_utils_ref (src_value->data[0].v_pointer);
	} else {
		dest_value->data[0].v_pointer = NULL;
	}
}

static gpointer
rygel_value_gst_utils_peek_pointer (const GValue* value)
{
	return value->data[0].v_pointer;
}

static gchar*
rygel_value_gst_utils_collect_value (GValue* value,
                                     guint n_collect_values,
                                     GTypeCValue* collect_values,
                                     guint collect_flags)
{
	if (collect_values[0].v_pointer) {
		RygelGstUtils * object;
		object = collect_values[0].v_pointer;
		if (object->parent_instance.g_class == NULL) {
			return g_strconcat ("invalid unclassed object pointer for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
		} else if (!g_value_type_compatible (G_TYPE_FROM_INSTANCE (object), G_VALUE_TYPE (value))) {
			return g_strconcat ("invalid object type `", g_type_name (G_TYPE_FROM_INSTANCE (object)), "' for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
		}
		value->data[0].v_pointer = rygel_gst_utils_ref (object);
	} else {
		value->data[0].v_pointer = NULL;
	}
	return NULL;
}

static gchar*
rygel_value_gst_utils_lcopy_value (const GValue* value,
                                   guint n_collect_values,
                                   GTypeCValue* collect_values,
                                   guint collect_flags)
{
	RygelGstUtils ** object_p;
	object_p = collect_values[0].v_pointer;
	if (!object_p) {
		return g_strdup_printf ("value location for `%s' passed as NULL", G_VALUE_TYPE_NAME (value));
	}
	if (!value->data[0].v_pointer) {
		*object_p = NULL;
	} else if (collect_flags & G_VALUE_NOCOPY_CONTENTS) {
		*object_p = value->data[0].v_pointer;
	} else {
		*object_p = rygel_gst_utils_ref (value->data[0].v_pointer);
	}
	return NULL;
}

GParamSpec*
rygel_param_spec_gst_utils (const gchar* name,
                            const gchar* nick,
                            const gchar* blurb,
                            GType object_type,
                            GParamFlags flags)
{
	RygelParamSpecGstUtils* spec;
	g_return_val_if_fail (g_type_is_a (object_type, RYGEL_TYPE_GST_UTILS), NULL);
	spec = g_param_spec_internal (G_TYPE_PARAM_OBJECT, name, nick, blurb, flags);
	G_PARAM_SPEC (spec)->value_type = object_type;
	return G_PARAM_SPEC (spec);
}

gpointer
rygel_value_get_gst_utils (const GValue* value)
{
	g_return_val_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, RYGEL_TYPE_GST_UTILS), NULL);
	return value->data[0].v_pointer;
}

void
rygel_value_set_gst_utils (GValue* value,
                           gpointer v_object)
{
	RygelGstUtils * old;
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, RYGEL_TYPE_GST_UTILS));
	old = value->data[0].v_pointer;
	if (v_object) {
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, RYGEL_TYPE_GST_UTILS));
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
		value->data[0].v_pointer = v_object;
		rygel_gst_utils_ref (value->data[0].v_pointer);
	} else {
		value->data[0].v_pointer = NULL;
	}
	if (old) {
		rygel_gst_utils_unref (old);
	}
}

void
rygel_value_take_gst_utils (GValue* value,
                            gpointer v_object)
{
	RygelGstUtils * old;
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, RYGEL_TYPE_GST_UTILS));
	old = value->data[0].v_pointer;
	if (v_object) {
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, RYGEL_TYPE_GST_UTILS));
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
		value->data[0].v_pointer = v_object;
	} else {
		value->data[0].v_pointer = NULL;
	}
	if (old) {
		rygel_gst_utils_unref (old);
	}
}

static void
rygel_gst_utils_class_init (RygelGstUtilsClass * klass,
                            gpointer klass_data)
{
	rygel_gst_utils_parent_class = g_type_class_peek_parent (klass);
	((RygelGstUtilsClass *) klass)->finalize = rygel_gst_utils_finalize;
}

static void
rygel_gst_utils_instance_init (RygelGstUtils * self,
                               gpointer klass)
{
	self->ref_count = 1;
}

static void
rygel_gst_utils_finalize (RygelGstUtils * obj)
{
	RygelGstUtils * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, RYGEL_TYPE_GST_UTILS, RygelGstUtils);
	g_signal_handlers_destroy (self);
}

GType
rygel_gst_utils_get_type (void)
{
	static volatile gsize rygel_gst_utils_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_gst_utils_type_id__volatile)) {
		static const GTypeValueTable g_define_type_value_table = { rygel_value_gst_utils_init, rygel_value_gst_utils_free_value, rygel_value_gst_utils_copy_value, rygel_value_gst_utils_peek_pointer, "p", rygel_value_gst_utils_collect_value, "p", rygel_value_gst_utils_lcopy_value };
		static const GTypeInfo g_define_type_info = { sizeof (RygelGstUtilsClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_gst_utils_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelGstUtils), 0, (GInstanceInitFunc) rygel_gst_utils_instance_init, &g_define_type_value_table };
		static const GTypeFundamentalInfo g_define_type_fundamental_info = { (G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE | G_TYPE_FLAG_DERIVABLE | G_TYPE_FLAG_DEEP_DERIVABLE) };
		GType rygel_gst_utils_type_id;
		rygel_gst_utils_type_id = g_type_register_fundamental (g_type_fundamental_next (), "RygelGstUtils", &g_define_type_info, &g_define_type_fundamental_info, G_TYPE_FLAG_ABSTRACT);
		g_once_init_leave (&rygel_gst_utils_type_id__volatile, rygel_gst_utils_type_id);
	}
	return rygel_gst_utils_type_id__volatile;
}

gpointer
rygel_gst_utils_ref (gpointer instance)
{
	RygelGstUtils * self;
	self = instance;
	g_atomic_int_inc (&self->ref_count);
	return instance;
}

void
rygel_gst_utils_unref (gpointer instance)
{
	RygelGstUtils * self;
	self = instance;
	if (g_atomic_int_dec_and_test (&self->ref_count)) {
		RYGEL_GST_UTILS_GET_CLASS (self)->finalize (self);
		g_type_free_instance ((GTypeInstance *) self);
	}
}

