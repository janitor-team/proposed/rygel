/* rygel-video-transcoder.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-video-transcoder.vala, do not modify */

/*
 * Copyright (C) 2011 Nokia Corporation.
 * Copyright (C) 2013 Cable Television Laboratories, Inc.
 *
 * Author: Jens Georg <jensg@openismus.com>
 *         Prasanna Modem <prasanna@ecaspia.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib-object.h>
#include <rygel-server.h>
#include <glib.h>
#include <gst/pbutils/pbutils.h>
#include <gst/gst.h>
#include <stdlib.h>
#include <string.h>

#define RYGEL_TYPE_GST_TRANSCODER (rygel_gst_transcoder_get_type ())
#define RYGEL_GST_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_GST_TRANSCODER, RygelGstTranscoder))
#define RYGEL_GST_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_GST_TRANSCODER, RygelGstTranscoderClass))
#define RYGEL_IS_GST_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_GST_TRANSCODER))
#define RYGEL_IS_GST_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_GST_TRANSCODER))
#define RYGEL_GST_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_GST_TRANSCODER, RygelGstTranscoderClass))

typedef struct _RygelGstTranscoder RygelGstTranscoder;
typedef struct _RygelGstTranscoderClass RygelGstTranscoderClass;
typedef struct _RygelGstTranscoderPrivate RygelGstTranscoderPrivate;

#define RYGEL_TYPE_AUDIO_TRANSCODER (rygel_audio_transcoder_get_type ())
#define RYGEL_AUDIO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_AUDIO_TRANSCODER, RygelAudioTranscoder))
#define RYGEL_AUDIO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_AUDIO_TRANSCODER, RygelAudioTranscoderClass))
#define RYGEL_IS_AUDIO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_AUDIO_TRANSCODER))
#define RYGEL_IS_AUDIO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_AUDIO_TRANSCODER))
#define RYGEL_AUDIO_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_AUDIO_TRANSCODER, RygelAudioTranscoderClass))

typedef struct _RygelAudioTranscoder RygelAudioTranscoder;
typedef struct _RygelAudioTranscoderClass RygelAudioTranscoderClass;
typedef struct _RygelAudioTranscoderPrivate RygelAudioTranscoderPrivate;

#define RYGEL_TYPE_VIDEO_TRANSCODER (rygel_video_transcoder_get_type ())
#define RYGEL_VIDEO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoder))
#define RYGEL_VIDEO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoderClass))
#define RYGEL_IS_VIDEO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_VIDEO_TRANSCODER))
#define RYGEL_IS_VIDEO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_VIDEO_TRANSCODER))
#define RYGEL_VIDEO_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoderClass))

typedef struct _RygelVideoTranscoder RygelVideoTranscoder;
typedef struct _RygelVideoTranscoderClass RygelVideoTranscoderClass;
typedef struct _RygelVideoTranscoderPrivate RygelVideoTranscoderPrivate;
enum  {
	RYGEL_VIDEO_TRANSCODER_0_PROPERTY,
	RYGEL_VIDEO_TRANSCODER_NUM_PROPERTIES
};
static GParamSpec* rygel_video_transcoder_properties[RYGEL_VIDEO_TRANSCODER_NUM_PROPERTIES];
#define _gst_caps_unref0(var) ((var == NULL) ? NULL : (var = (gst_caps_unref (var), NULL)))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

struct _RygelGstTranscoder {
	GObject parent_instance;
	RygelGstTranscoderPrivate * priv;
};

struct _RygelGstTranscoderClass {
	GObjectClass parent_class;
	RygelMediaResource* (*get_resource_for_item) (RygelGstTranscoder* self, RygelMediaFileItem* item);
	guint (*get_distance) (RygelGstTranscoder* self, RygelMediaFileItem* item);
	GstEncodingProfile* (*get_encoding_profile) (RygelGstTranscoder* self, RygelMediaFileItem* item);
};

struct _RygelAudioTranscoder {
	RygelGstTranscoder parent_instance;
	RygelAudioTranscoderPrivate * priv;
	gint audio_bitrate;
	GstCaps* container_format;
	GstCaps* audio_codec_format;
};

struct _RygelAudioTranscoderClass {
	RygelGstTranscoderClass parent_class;
};

struct _RygelVideoTranscoder {
	RygelAudioTranscoder parent_instance;
	RygelVideoTranscoderPrivate * priv;
};

struct _RygelVideoTranscoderClass {
	RygelAudioTranscoderClass parent_class;
};

struct _RygelVideoTranscoderPrivate {
	gint video_bitrate;
	GstCaps* video_codec_format;
	GstCaps* video_restrictions;
};

static gint RygelVideoTranscoder_private_offset;
static gpointer rygel_video_transcoder_parent_class = NULL;

GType rygel_gst_transcoder_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelGstTranscoder, g_object_unref)
GType rygel_audio_transcoder_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelAudioTranscoder, g_object_unref)
GType rygel_video_transcoder_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelVideoTranscoder, g_object_unref)
RygelVideoTranscoder* rygel_video_transcoder_construct (GType object_type,
                                                        const gchar* name,
                                                        const gchar* content_type,
                                                        const gchar* dlna_profile,
                                                        gint audio_bitrate,
                                                        gint video_bitrate,
                                                        const gchar* container_caps,
                                                        const gchar* audio_codec_caps,
                                                        const gchar* video_codec_caps,
                                                        const gchar* extension,
                                                        const gchar* restrictions);
RygelAudioTranscoder* rygel_audio_transcoder_construct_with_class (GType object_type,
                                                                   const gchar* name,
                                                                   const gchar* content_type,
                                                                   const gchar* dlna_profile,
                                                                   gint audio_bitrate,
                                                                   const gchar* container_caps,
                                                                   const gchar* audio_codec_caps,
                                                                   const gchar* extension);
static guint rygel_video_transcoder_real_get_distance (RygelGstTranscoder* base,
                                                RygelMediaFileItem* item);
static GstEncodingProfile* rygel_video_transcoder_real_get_encoding_profile (RygelGstTranscoder* base,
                                                                      RygelMediaFileItem* item);
GstEncodingProfile* rygel_gst_transcoder_get_encoding_profile (RygelGstTranscoder* self,
                                                               RygelMediaFileItem* item);
const gchar* rygel_gst_transcoder_get_preset (RygelGstTranscoder* self);
static RygelMediaResource* rygel_video_transcoder_real_get_resource_for_item (RygelGstTranscoder* base,
                                                                       RygelMediaFileItem* item);
RygelMediaResource* rygel_gst_transcoder_get_resource_for_item (RygelGstTranscoder* self,
                                                                RygelMediaFileItem* item);
static void rygel_video_transcoder_finalize (GObject * obj);

static inline gpointer
rygel_video_transcoder_get_instance_private (RygelVideoTranscoder* self)
{
	return G_STRUCT_MEMBER_P (self, RygelVideoTranscoder_private_offset);
}

RygelVideoTranscoder*
rygel_video_transcoder_construct (GType object_type,
                                  const gchar* name,
                                  const gchar* content_type,
                                  const gchar* dlna_profile,
                                  gint audio_bitrate,
                                  gint video_bitrate,
                                  const gchar* container_caps,
                                  const gchar* audio_codec_caps,
                                  const gchar* video_codec_caps,
                                  const gchar* extension,
                                  const gchar* restrictions)
{
	RygelVideoTranscoder * self = NULL;
	GstCaps* _tmp0_;
	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (content_type != NULL, NULL);
	g_return_val_if_fail (dlna_profile != NULL, NULL);
	g_return_val_if_fail (container_caps != NULL, NULL);
	g_return_val_if_fail (audio_codec_caps != NULL, NULL);
	g_return_val_if_fail (video_codec_caps != NULL, NULL);
	g_return_val_if_fail (extension != NULL, NULL);
	self = (RygelVideoTranscoder*) rygel_audio_transcoder_construct_with_class (object_type, name, content_type, dlna_profile, audio_bitrate, container_caps, audio_codec_caps, extension);
	self->priv->video_bitrate = video_bitrate;
	_tmp0_ = gst_caps_from_string (video_codec_caps);
	_gst_caps_unref0 (self->priv->video_codec_format);
	self->priv->video_codec_format = _tmp0_;
	if (restrictions != NULL) {
		GstCaps* _tmp1_;
		_tmp1_ = gst_caps_from_string (restrictions);
		_gst_caps_unref0 (self->priv->video_restrictions);
		self->priv->video_restrictions = _tmp1_;
	}
	return self;
}

static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

static guint
rygel_video_transcoder_real_get_distance (RygelGstTranscoder* base,
                                          RygelMediaFileItem* item)
{
	RygelVideoTranscoder * self;
	RygelVideoItem* video_item = NULL;
	RygelVideoItem* _tmp0_;
	guint distance = 0U;
	RygelVideoItem* _tmp1_;
	gint _tmp2_;
	gint _tmp3_;
	guint result = 0U;
	self = (RygelVideoTranscoder*) base;
	g_return_val_if_fail (item != NULL, 0U);
	if (!G_TYPE_CHECK_INSTANCE_TYPE (item, RYGEL_TYPE_VIDEO_ITEM)) {
		result = G_MAXUINT;
		return result;
	}
	_tmp0_ = _g_object_ref0 (G_TYPE_CHECK_INSTANCE_TYPE (item, RYGEL_TYPE_VIDEO_ITEM) ? ((RygelVideoItem*) item) : NULL);
	video_item = _tmp0_;
	distance = 0;
	_tmp1_ = video_item;
	_tmp2_ = rygel_audio_item_get_bitrate ((RygelAudioItem*) _tmp1_);
	_tmp3_ = _tmp2_;
	if (_tmp3_ > 0) {
		RygelVideoItem* _tmp4_;
		gint _tmp5_;
		gint _tmp6_;
		_tmp4_ = video_item;
		_tmp5_ = rygel_audio_item_get_bitrate ((RygelAudioItem*) _tmp4_);
		_tmp6_ = _tmp5_;
		distance = distance + abs (_tmp6_ - self->priv->video_bitrate);
	}
	result = distance;
	_g_object_unref0 (video_item);
	return result;
}

static GstEncodingProfile*
rygel_video_transcoder_real_get_encoding_profile (RygelGstTranscoder* base,
                                                  RygelMediaFileItem* item)
{
	RygelVideoTranscoder * self;
	GstEncodingContainerProfile* enc_container_profile = NULL;
	GstEncodingProfile* _tmp0_;
	GstEncodingContainerProfile* _tmp1_;
	GstEncodingVideoProfile* enc_video_profile = NULL;
	GstCaps* _tmp2_;
	const gchar* _tmp3_;
	const gchar* _tmp4_;
	GstCaps* _tmp5_;
	GstEncodingVideoProfile* _tmp6_;
	GstEncodingProfile* _tmp7_;
	GstEncodingProfile* result = NULL;
	self = (RygelVideoTranscoder*) base;
	g_return_val_if_fail (item != NULL, NULL);
	_tmp0_ = RYGEL_GST_TRANSCODER_CLASS (rygel_video_transcoder_parent_class)->get_encoding_profile ((RygelGstTranscoder*) G_TYPE_CHECK_INSTANCE_CAST (self, RYGEL_TYPE_AUDIO_TRANSCODER, RygelAudioTranscoder), item);
	_tmp1_ = G_TYPE_CHECK_INSTANCE_TYPE (_tmp0_, gst_encoding_container_profile_get_type ()) ? ((GstEncodingContainerProfile*) _tmp0_) : NULL;
	if (_tmp1_ == NULL) {
		_g_object_unref0 (_tmp0_);
	}
	enc_container_profile = _tmp1_;
	_tmp2_ = self->priv->video_codec_format;
	_tmp3_ = rygel_gst_transcoder_get_preset ((RygelGstTranscoder*) self);
	_tmp4_ = _tmp3_;
	_tmp5_ = self->priv->video_restrictions;
	_tmp6_ = gst_encoding_video_profile_new (_tmp2_, _tmp4_, _tmp5_, (guint) 1);
	enc_video_profile = _tmp6_;
	gst_encoding_profile_set_name ((GstEncodingProfile*) enc_video_profile, "video");
	_tmp7_ = _g_object_ref0 ((GstEncodingProfile*) enc_video_profile);
	gst_encoding_container_profile_add_profile (enc_container_profile, _tmp7_);
	result = (GstEncodingProfile*) enc_container_profile;
	_g_object_unref0 (enc_video_profile);
	return result;
}

static RygelMediaResource*
rygel_video_transcoder_real_get_resource_for_item (RygelGstTranscoder* base,
                                                   RygelMediaFileItem* item)
{
	RygelVideoTranscoder * self;
	RygelMediaResource* resource = NULL;
	RygelMediaResource* _tmp0_;
	RygelMediaResource* _tmp1_;
	RygelVideoItem* video_item = NULL;
	RygelVideoItem* _tmp2_;
	RygelMediaResource* _tmp3_;
	RygelVideoItem* _tmp4_;
	gint _tmp5_;
	gint _tmp6_;
	RygelMediaResource* _tmp7_;
	RygelVideoItem* _tmp8_;
	gint _tmp9_;
	gint _tmp10_;
	RygelMediaResource* _tmp11_;
	RygelMediaResource* result = NULL;
	self = (RygelVideoTranscoder*) base;
	g_return_val_if_fail (item != NULL, NULL);
	_tmp0_ = RYGEL_GST_TRANSCODER_CLASS (rygel_video_transcoder_parent_class)->get_resource_for_item ((RygelGstTranscoder*) G_TYPE_CHECK_INSTANCE_CAST (self, RYGEL_TYPE_AUDIO_TRANSCODER, RygelAudioTranscoder), item);
	resource = _tmp0_;
	_tmp1_ = resource;
	if (_tmp1_ == NULL) {
		result = NULL;
		_g_object_unref0 (resource);
		return result;
	}
	_tmp2_ = _g_object_ref0 (G_TYPE_CHECK_INSTANCE_TYPE (item, RYGEL_TYPE_VIDEO_ITEM) ? ((RygelVideoItem*) item) : NULL);
	video_item = _tmp2_;
	_tmp3_ = resource;
	_tmp4_ = video_item;
	_tmp5_ = rygel_visual_item_get_width ((RygelVisualItem*) _tmp4_);
	_tmp6_ = _tmp5_;
	rygel_media_resource_set_width (_tmp3_, _tmp6_);
	_tmp7_ = resource;
	_tmp8_ = video_item;
	_tmp9_ = rygel_visual_item_get_height ((RygelVisualItem*) _tmp8_);
	_tmp10_ = _tmp9_;
	rygel_media_resource_set_height (_tmp7_, _tmp10_);
	_tmp11_ = resource;
	rygel_media_resource_set_bitrate (_tmp11_, ((self->priv->video_bitrate + ((RygelAudioTranscoder*) self)->audio_bitrate) * 1000) / 8);
	result = resource;
	_g_object_unref0 (video_item);
	return result;
}

static void
rygel_video_transcoder_class_init (RygelVideoTranscoderClass * klass,
                                   gpointer klass_data)
{
	rygel_video_transcoder_parent_class = g_type_class_peek_parent (klass);
	g_type_class_adjust_private_offset (klass, &RygelVideoTranscoder_private_offset);
	((RygelGstTranscoderClass *) klass)->get_distance = (guint (*) (RygelGstTranscoder*, RygelMediaFileItem*)) rygel_video_transcoder_real_get_distance;
	((RygelGstTranscoderClass *) klass)->get_encoding_profile = (GstEncodingProfile* (*) (RygelGstTranscoder*, RygelMediaFileItem*)) rygel_video_transcoder_real_get_encoding_profile;
	((RygelGstTranscoderClass *) klass)->get_resource_for_item = (RygelMediaResource* (*) (RygelGstTranscoder*, RygelMediaFileItem*)) rygel_video_transcoder_real_get_resource_for_item;
	G_OBJECT_CLASS (klass)->finalize = rygel_video_transcoder_finalize;
}

static void
rygel_video_transcoder_instance_init (RygelVideoTranscoder * self,
                                      gpointer klass)
{
	self->priv = rygel_video_transcoder_get_instance_private (self);
	self->priv->video_restrictions = NULL;
}

static void
rygel_video_transcoder_finalize (GObject * obj)
{
	RygelVideoTranscoder * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoder);
	_gst_caps_unref0 (self->priv->video_codec_format);
	_gst_caps_unref0 (self->priv->video_restrictions);
	G_OBJECT_CLASS (rygel_video_transcoder_parent_class)->finalize (obj);
}

/**
 * Base class for all transcoders that handle video.
 */
GType
rygel_video_transcoder_get_type (void)
{
	static volatile gsize rygel_video_transcoder_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_video_transcoder_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelVideoTranscoderClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_video_transcoder_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelVideoTranscoder), 0, (GInstanceInitFunc) rygel_video_transcoder_instance_init, NULL };
		GType rygel_video_transcoder_type_id;
		rygel_video_transcoder_type_id = g_type_register_static (RYGEL_TYPE_AUDIO_TRANSCODER, "RygelVideoTranscoder", &g_define_type_info, G_TYPE_FLAG_ABSTRACT);
		RygelVideoTranscoder_private_offset = g_type_add_instance_private (rygel_video_transcoder_type_id, sizeof (RygelVideoTranscoderPrivate));
		g_once_init_leave (&rygel_video_transcoder_type_id__volatile, rygel_video_transcoder_type_id);
	}
	return rygel_video_transcoder_type_id__volatile;
}

