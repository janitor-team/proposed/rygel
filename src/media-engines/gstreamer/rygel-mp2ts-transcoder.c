/* rygel-mp2ts-transcoder.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-mp2ts-transcoder.vala, do not modify */

/*
 * Copyright (C) 2009 Nokia Corporation.
 * Copyright (C) 2013 Cable Television Laboratories, Inc.
 *
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *                               <zeeshan.ali@nokia.com>
 *         Prasanna Modem <prasanna@ecaspia.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib-object.h>
#include <rygel-server.h>
#include <glib.h>
#include <gst/pbutils/pbutils.h>
#include <gst/gst.h>
#include <stdlib.h>
#include <string.h>

typedef enum  {
	RYGEL_MP2_TS_PROFILE_SD_EU = 0,
	RYGEL_MP2_TS_PROFILE_SD_NA,
	RYGEL_MP2_TS_PROFILE_HD_NA
} RygelMP2TSProfile;

#define RYGEL_TYPE_MP2_TS_PROFILE (rygel_mp2_ts_profile_get_type ())

#define RYGEL_TYPE_GST_TRANSCODER (rygel_gst_transcoder_get_type ())
#define RYGEL_GST_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_GST_TRANSCODER, RygelGstTranscoder))
#define RYGEL_GST_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_GST_TRANSCODER, RygelGstTranscoderClass))
#define RYGEL_IS_GST_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_GST_TRANSCODER))
#define RYGEL_IS_GST_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_GST_TRANSCODER))
#define RYGEL_GST_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_GST_TRANSCODER, RygelGstTranscoderClass))

typedef struct _RygelGstTranscoder RygelGstTranscoder;
typedef struct _RygelGstTranscoderClass RygelGstTranscoderClass;
typedef struct _RygelGstTranscoderPrivate RygelGstTranscoderPrivate;

#define RYGEL_TYPE_AUDIO_TRANSCODER (rygel_audio_transcoder_get_type ())
#define RYGEL_AUDIO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_AUDIO_TRANSCODER, RygelAudioTranscoder))
#define RYGEL_AUDIO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_AUDIO_TRANSCODER, RygelAudioTranscoderClass))
#define RYGEL_IS_AUDIO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_AUDIO_TRANSCODER))
#define RYGEL_IS_AUDIO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_AUDIO_TRANSCODER))
#define RYGEL_AUDIO_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_AUDIO_TRANSCODER, RygelAudioTranscoderClass))

typedef struct _RygelAudioTranscoder RygelAudioTranscoder;
typedef struct _RygelAudioTranscoderClass RygelAudioTranscoderClass;
typedef struct _RygelAudioTranscoderPrivate RygelAudioTranscoderPrivate;

#define RYGEL_TYPE_VIDEO_TRANSCODER (rygel_video_transcoder_get_type ())
#define RYGEL_VIDEO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoder))
#define RYGEL_VIDEO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoderClass))
#define RYGEL_IS_VIDEO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_VIDEO_TRANSCODER))
#define RYGEL_IS_VIDEO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_VIDEO_TRANSCODER))
#define RYGEL_VIDEO_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoderClass))

typedef struct _RygelVideoTranscoder RygelVideoTranscoder;
typedef struct _RygelVideoTranscoderClass RygelVideoTranscoderClass;
typedef struct _RygelVideoTranscoderPrivate RygelVideoTranscoderPrivate;

#define RYGEL_TYPE_MP2_TS_TRANSCODER (rygel_mp2_ts_transcoder_get_type ())
#define RYGEL_MP2_TS_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MP2_TS_TRANSCODER, RygelMP2TSTranscoder))
#define RYGEL_MP2_TS_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MP2_TS_TRANSCODER, RygelMP2TSTranscoderClass))
#define RYGEL_IS_MP2_TS_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MP2_TS_TRANSCODER))
#define RYGEL_IS_MP2_TS_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MP2_TS_TRANSCODER))
#define RYGEL_MP2_TS_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MP2_TS_TRANSCODER, RygelMP2TSTranscoderClass))

typedef struct _RygelMP2TSTranscoder RygelMP2TSTranscoder;
typedef struct _RygelMP2TSTranscoderClass RygelMP2TSTranscoderClass;
typedef struct _RygelMP2TSTranscoderPrivate RygelMP2TSTranscoderPrivate;
enum  {
	RYGEL_MP2_TS_TRANSCODER_0_PROPERTY,
	RYGEL_MP2_TS_TRANSCODER_NUM_PROPERTIES
};
static GParamSpec* rygel_mp2_ts_transcoder_properties[RYGEL_MP2_TS_TRANSCODER_NUM_PROPERTIES];
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

struct _RygelGstTranscoder {
	GObject parent_instance;
	RygelGstTranscoderPrivate * priv;
};

struct _RygelGstTranscoderClass {
	GObjectClass parent_class;
	RygelMediaResource* (*get_resource_for_item) (RygelGstTranscoder* self, RygelMediaFileItem* item);
	guint (*get_distance) (RygelGstTranscoder* self, RygelMediaFileItem* item);
	GstEncodingProfile* (*get_encoding_profile) (RygelGstTranscoder* self, RygelMediaFileItem* item);
};

struct _RygelAudioTranscoder {
	RygelGstTranscoder parent_instance;
	RygelAudioTranscoderPrivate * priv;
	gint audio_bitrate;
	GstCaps* container_format;
	GstCaps* audio_codec_format;
};

struct _RygelAudioTranscoderClass {
	RygelGstTranscoderClass parent_class;
};

struct _RygelVideoTranscoder {
	RygelAudioTranscoder parent_instance;
	RygelVideoTranscoderPrivate * priv;
};

struct _RygelVideoTranscoderClass {
	RygelAudioTranscoderClass parent_class;
};

struct _RygelMP2TSTranscoder {
	RygelVideoTranscoder parent_instance;
	RygelMP2TSTranscoderPrivate * priv;
};

struct _RygelMP2TSTranscoderClass {
	RygelVideoTranscoderClass parent_class;
};

struct _RygelMP2TSTranscoderPrivate {
	RygelMP2TSProfile profile;
};

static gint RygelMP2TSTranscoder_private_offset;
static gpointer rygel_mp2_ts_transcoder_parent_class = NULL;

GType rygel_mp2_ts_profile_get_type (void) G_GNUC_CONST;
GType rygel_gst_transcoder_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelGstTranscoder, g_object_unref)
GType rygel_audio_transcoder_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelAudioTranscoder, g_object_unref)
GType rygel_video_transcoder_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelVideoTranscoder, g_object_unref)
GType rygel_mp2_ts_transcoder_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMP2TSTranscoder, g_object_unref)
#define RYGEL_MP2_TS_TRANSCODER_VIDEO_BITRATE 1500
#define RYGEL_MP2_TS_TRANSCODER_AUDIO_BITRATE 192
#define RYGEL_MP2_TS_TRANSCODER_CONTAINER "video/mpegts,systemstream=true,packetsize=188"
#define RYGEL_MP2_TS_TRANSCODER_AUDIO_FORMAT "audio/mpeg,mpegversion=1,layer=2"
#define RYGEL_MP2_TS_TRANSCODER_BASE_VIDEO_FORMAT "video/mpeg,mpegversion=2,systemstream=false"
#define RYGEL_MP2_TS_TRANSCODER_RESTRICTION_TEMPLATE "video/x-raw,framerate=(fraction)%d/1,width=%d,height=%d"
RygelMP2TSTranscoder* rygel_mp2_ts_transcoder_new (RygelMP2TSProfile profile);
RygelMP2TSTranscoder* rygel_mp2_ts_transcoder_construct (GType object_type,
                                                         RygelMP2TSProfile profile);
RygelVideoTranscoder* rygel_video_transcoder_construct (GType object_type,
                                                        const gchar* name,
                                                        const gchar* content_type,
                                                        const gchar* dlna_profile,
                                                        gint audio_bitrate,
                                                        gint video_bitrate,
                                                        const gchar* container_caps,
                                                        const gchar* audio_codec_caps,
                                                        const gchar* video_codec_caps,
                                                        const gchar* extension,
                                                        const gchar* restrictions);
static guint rygel_mp2_ts_transcoder_real_get_distance (RygelGstTranscoder* base,
                                                 RygelMediaFileItem* item);
guint rygel_gst_transcoder_get_distance (RygelGstTranscoder* self,
                                         RygelMediaFileItem* item);
static RygelMediaResource* rygel_mp2_ts_transcoder_real_get_resource_for_item (RygelGstTranscoder* base,
                                                                        RygelMediaFileItem* item);
RygelMediaResource* rygel_gst_transcoder_get_resource_for_item (RygelGstTranscoder* self,
                                                                RygelMediaFileItem* item);
static void rygel_mp2_ts_transcoder_finalize (GObject * obj);

static const gint RYGEL_MP2_TS_TRANSCODER_WIDTH[3] = {720, 720, 1280};
static const gint RYGEL_MP2_TS_TRANSCODER_HEIGHT[3] = {576, 480, 720};
static const gint RYGEL_MP2_TS_TRANSCODER_FRAME_RATE[3] = {25, 30, 30};
static const gchar* RYGEL_MP2_TS_TRANSCODER_PROFILES[3] = {"MPEG_TS_SD_EU_ISO", "MPEG_TS_SD_NA_ISO", "MPEG_TS_HD_NA_ISO"};

GType
rygel_mp2_ts_profile_get_type (void)
{
	static volatile gsize rygel_mp2_ts_profile_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_mp2_ts_profile_type_id__volatile)) {
		static const GEnumValue values[] = {{RYGEL_MP2_TS_PROFILE_SD_EU, "RYGEL_MP2_TS_PROFILE_SD_EU", "sd-eu"}, {RYGEL_MP2_TS_PROFILE_SD_NA, "RYGEL_MP2_TS_PROFILE_SD_NA", "sd-na"}, {RYGEL_MP2_TS_PROFILE_HD_NA, "RYGEL_MP2_TS_PROFILE_HD_NA", "hd-na"}, {0, NULL, NULL}};
		GType rygel_mp2_ts_profile_type_id;
		rygel_mp2_ts_profile_type_id = g_enum_register_static ("RygelMP2TSProfile", values);
		g_once_init_leave (&rygel_mp2_ts_profile_type_id__volatile, rygel_mp2_ts_profile_type_id);
	}
	return rygel_mp2_ts_profile_type_id__volatile;
}

static inline gpointer
rygel_mp2_ts_transcoder_get_instance_private (RygelMP2TSTranscoder* self)
{
	return G_STRUCT_MEMBER_P (self, RygelMP2TSTranscoder_private_offset);
}

RygelMP2TSTranscoder*
rygel_mp2_ts_transcoder_construct (GType object_type,
                                   RygelMP2TSProfile profile)
{
	RygelMP2TSTranscoder * self = NULL;
	const gchar* _tmp0_;
	const gchar* _tmp1_;
	gint _tmp2_;
	gint _tmp3_;
	gint _tmp4_;
	gchar* _tmp5_;
	gchar* _tmp6_;
	_tmp0_ = RYGEL_MP2_TS_TRANSCODER_PROFILES[profile];
	_tmp1_ = RYGEL_MP2_TS_TRANSCODER_PROFILES[profile];
	_tmp2_ = RYGEL_MP2_TS_TRANSCODER_FRAME_RATE[profile];
	_tmp3_ = RYGEL_MP2_TS_TRANSCODER_WIDTH[profile];
	_tmp4_ = RYGEL_MP2_TS_TRANSCODER_HEIGHT[profile];
	_tmp5_ = g_strdup_printf (RYGEL_MP2_TS_TRANSCODER_RESTRICTION_TEMPLATE, _tmp2_, _tmp3_, _tmp4_);
	_tmp6_ = _tmp5_;
	self = (RygelMP2TSTranscoder*) rygel_video_transcoder_construct (object_type, _tmp0_, "video/mpeg", _tmp1_, RYGEL_MP2_TS_TRANSCODER_AUDIO_BITRATE, RYGEL_MP2_TS_TRANSCODER_VIDEO_BITRATE, RYGEL_MP2_TS_TRANSCODER_CONTAINER, RYGEL_MP2_TS_TRANSCODER_AUDIO_FORMAT, RYGEL_MP2_TS_TRANSCODER_BASE_VIDEO_FORMAT, "mpg", _tmp6_);
	_g_free0 (_tmp6_);
	self->priv->profile = profile;
	return self;
}

RygelMP2TSTranscoder*
rygel_mp2_ts_transcoder_new (RygelMP2TSProfile profile)
{
	return rygel_mp2_ts_transcoder_construct (RYGEL_TYPE_MP2_TS_TRANSCODER, profile);
}

static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

static guint
rygel_mp2_ts_transcoder_real_get_distance (RygelGstTranscoder* base,
                                           RygelMediaFileItem* item)
{
	RygelMP2TSTranscoder * self;
	RygelVideoItem* video_item = NULL;
	RygelVideoItem* _tmp0_;
	guint distance = 0U;
	RygelVideoItem* _tmp1_;
	gint _tmp2_;
	gint _tmp3_;
	RygelVideoItem* _tmp7_;
	gint _tmp8_;
	gint _tmp9_;
	RygelVideoItem* _tmp15_;
	gint _tmp16_;
	gint _tmp17_;
	guint result = 0U;
	self = (RygelMP2TSTranscoder*) base;
	g_return_val_if_fail (item != NULL, 0U);
	if (!G_TYPE_CHECK_INSTANCE_TYPE (item, RYGEL_TYPE_VIDEO_ITEM)) {
		result = G_MAXUINT;
		return result;
	}
	_tmp0_ = _g_object_ref0 (G_TYPE_CHECK_INSTANCE_TYPE (item, RYGEL_TYPE_VIDEO_ITEM) ? ((RygelVideoItem*) item) : NULL);
	video_item = _tmp0_;
	distance = RYGEL_GST_TRANSCODER_CLASS (rygel_mp2_ts_transcoder_parent_class)->get_distance ((RygelGstTranscoder*) G_TYPE_CHECK_INSTANCE_CAST (self, RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoder), item);
	_tmp1_ = video_item;
	_tmp2_ = rygel_audio_item_get_bitrate ((RygelAudioItem*) _tmp1_);
	_tmp3_ = _tmp2_;
	if (_tmp3_ > 0) {
		RygelVideoItem* _tmp4_;
		gint _tmp5_;
		gint _tmp6_;
		_tmp4_ = video_item;
		_tmp5_ = rygel_audio_item_get_bitrate ((RygelAudioItem*) _tmp4_);
		_tmp6_ = _tmp5_;
		distance = distance + abs (_tmp6_ - RYGEL_MP2_TS_TRANSCODER_VIDEO_BITRATE);
	}
	_tmp7_ = video_item;
	_tmp8_ = rygel_visual_item_get_width ((RygelVisualItem*) _tmp7_);
	_tmp9_ = _tmp8_;
	if (_tmp9_ > 0) {
		RygelVideoItem* _tmp10_;
		gint _tmp11_;
		gint _tmp12_;
		RygelMP2TSProfile _tmp13_;
		gint _tmp14_;
		_tmp10_ = video_item;
		_tmp11_ = rygel_visual_item_get_width ((RygelVisualItem*) _tmp10_);
		_tmp12_ = _tmp11_;
		_tmp13_ = self->priv->profile;
		_tmp14_ = RYGEL_MP2_TS_TRANSCODER_WIDTH[_tmp13_];
		distance = distance + abs (_tmp12_ - _tmp14_);
	}
	_tmp15_ = video_item;
	_tmp16_ = rygel_visual_item_get_height ((RygelVisualItem*) _tmp15_);
	_tmp17_ = _tmp16_;
	if (_tmp17_ > 0) {
		RygelVideoItem* _tmp18_;
		gint _tmp19_;
		gint _tmp20_;
		RygelMP2TSProfile _tmp21_;
		gint _tmp22_;
		_tmp18_ = video_item;
		_tmp19_ = rygel_visual_item_get_height ((RygelVisualItem*) _tmp18_);
		_tmp20_ = _tmp19_;
		_tmp21_ = self->priv->profile;
		_tmp22_ = RYGEL_MP2_TS_TRANSCODER_HEIGHT[_tmp21_];
		distance = distance + abs (_tmp20_ - _tmp22_);
	}
	result = distance;
	_g_object_unref0 (video_item);
	return result;
}

static RygelMediaResource*
rygel_mp2_ts_transcoder_real_get_resource_for_item (RygelGstTranscoder* base,
                                                    RygelMediaFileItem* item)
{
	RygelMP2TSTranscoder * self;
	RygelMediaResource* resource = NULL;
	RygelMediaResource* _tmp0_;
	RygelMediaResource* _tmp1_;
	RygelMediaResource* _tmp2_;
	RygelMP2TSProfile _tmp3_;
	gint _tmp4_;
	RygelMediaResource* _tmp5_;
	RygelMP2TSProfile _tmp6_;
	gint _tmp7_;
	RygelMediaResource* _tmp8_;
	RygelMediaResource* result = NULL;
	self = (RygelMP2TSTranscoder*) base;
	g_return_val_if_fail (item != NULL, NULL);
	_tmp0_ = RYGEL_GST_TRANSCODER_CLASS (rygel_mp2_ts_transcoder_parent_class)->get_resource_for_item ((RygelGstTranscoder*) G_TYPE_CHECK_INSTANCE_CAST (self, RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoder), item);
	resource = _tmp0_;
	_tmp1_ = resource;
	if (_tmp1_ == NULL) {
		result = NULL;
		_g_object_unref0 (resource);
		return result;
	}
	_tmp2_ = resource;
	_tmp3_ = self->priv->profile;
	_tmp4_ = RYGEL_MP2_TS_TRANSCODER_WIDTH[_tmp3_];
	rygel_media_resource_set_width (_tmp2_, _tmp4_);
	_tmp5_ = resource;
	_tmp6_ = self->priv->profile;
	_tmp7_ = RYGEL_MP2_TS_TRANSCODER_HEIGHT[_tmp6_];
	rygel_media_resource_set_height (_tmp5_, _tmp7_);
	_tmp8_ = resource;
	rygel_media_resource_set_bitrate (_tmp8_, ((RYGEL_MP2_TS_TRANSCODER_VIDEO_BITRATE + RYGEL_MP2_TS_TRANSCODER_AUDIO_BITRATE) * 1000) / 8);
	result = resource;
	return result;
}

static void
rygel_mp2_ts_transcoder_class_init (RygelMP2TSTranscoderClass * klass,
                                    gpointer klass_data)
{
	rygel_mp2_ts_transcoder_parent_class = g_type_class_peek_parent (klass);
	g_type_class_adjust_private_offset (klass, &RygelMP2TSTranscoder_private_offset);
	((RygelGstTranscoderClass *) klass)->get_distance = (guint (*) (RygelGstTranscoder*, RygelMediaFileItem*)) rygel_mp2_ts_transcoder_real_get_distance;
	((RygelGstTranscoderClass *) klass)->get_resource_for_item = (RygelMediaResource* (*) (RygelGstTranscoder*, RygelMediaFileItem*)) rygel_mp2_ts_transcoder_real_get_resource_for_item;
	G_OBJECT_CLASS (klass)->finalize = rygel_mp2_ts_transcoder_finalize;
}

static void
rygel_mp2_ts_transcoder_instance_init (RygelMP2TSTranscoder * self,
                                       gpointer klass)
{
	self->priv = rygel_mp2_ts_transcoder_get_instance_private (self);
}

static void
rygel_mp2_ts_transcoder_finalize (GObject * obj)
{
	RygelMP2TSTranscoder * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, RYGEL_TYPE_MP2_TS_TRANSCODER, RygelMP2TSTranscoder);
	G_OBJECT_CLASS (rygel_mp2_ts_transcoder_parent_class)->finalize (obj);
}

/**
 * Transcoder for mpeg transport stream containing mpeg 2 video and mp2 audio.
 */
GType
rygel_mp2_ts_transcoder_get_type (void)
{
	static volatile gsize rygel_mp2_ts_transcoder_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_mp2_ts_transcoder_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelMP2TSTranscoderClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_mp2_ts_transcoder_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelMP2TSTranscoder), 0, (GInstanceInitFunc) rygel_mp2_ts_transcoder_instance_init, NULL };
		GType rygel_mp2_ts_transcoder_type_id;
		rygel_mp2_ts_transcoder_type_id = g_type_register_static (RYGEL_TYPE_VIDEO_TRANSCODER, "RygelMP2TSTranscoder", &g_define_type_info, 0);
		RygelMP2TSTranscoder_private_offset = g_type_add_instance_private (rygel_mp2_ts_transcoder_type_id, sizeof (RygelMP2TSTranscoderPrivate));
		g_once_init_leave (&rygel_mp2_ts_transcoder_type_id__volatile, rygel_mp2_ts_transcoder_type_id);
	}
	return rygel_mp2_ts_transcoder_type_id__volatile;
}

