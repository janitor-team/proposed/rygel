/* rygel-lms-image-year.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-lms-image-year.vala, do not modify */

/*
 * Copyright (C) 2013 Intel Corporation.
 *
 * Author: Jussi Kukkonen <jussi.kukkonen@intel.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <rygel-server.h>
#include <glib-object.h>
#include <sqlite3.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <rygel-db.h>
#include <gio/gio.h>

#define RYGEL_LMS_TYPE_CATEGORY_CONTAINER (rygel_lms_category_container_get_type ())
#define RYGEL_LMS_CATEGORY_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_LMS_TYPE_CATEGORY_CONTAINER, RygelLMSCategoryContainer))
#define RYGEL_LMS_CATEGORY_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_LMS_TYPE_CATEGORY_CONTAINER, RygelLMSCategoryContainerClass))
#define RYGEL_LMS_IS_CATEGORY_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_LMS_TYPE_CATEGORY_CONTAINER))
#define RYGEL_LMS_IS_CATEGORY_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_LMS_TYPE_CATEGORY_CONTAINER))
#define RYGEL_LMS_CATEGORY_CONTAINER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_LMS_TYPE_CATEGORY_CONTAINER, RygelLMSCategoryContainerClass))

typedef struct _RygelLMSCategoryContainer RygelLMSCategoryContainer;
typedef struct _RygelLMSCategoryContainerClass RygelLMSCategoryContainerClass;
typedef struct _RygelLMSCategoryContainerPrivate RygelLMSCategoryContainerPrivate;

#define RYGEL_LMS_TYPE_IMAGE_YEAR (rygel_lms_image_year_get_type ())
#define RYGEL_LMS_IMAGE_YEAR(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_LMS_TYPE_IMAGE_YEAR, RygelLMSImageYear))
#define RYGEL_LMS_IMAGE_YEAR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_LMS_TYPE_IMAGE_YEAR, RygelLMSImageYearClass))
#define RYGEL_LMS_IS_IMAGE_YEAR(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_LMS_TYPE_IMAGE_YEAR))
#define RYGEL_LMS_IS_IMAGE_YEAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_LMS_TYPE_IMAGE_YEAR))
#define RYGEL_LMS_IMAGE_YEAR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_LMS_TYPE_IMAGE_YEAR, RygelLMSImageYearClass))

typedef struct _RygelLMSImageYear RygelLMSImageYear;
typedef struct _RygelLMSImageYearClass RygelLMSImageYearClass;
typedef struct _RygelLMSImageYearPrivate RygelLMSImageYearPrivate;
enum  {
	RYGEL_LMS_IMAGE_YEAR_0_PROPERTY,
	RYGEL_LMS_IMAGE_YEAR_NUM_PROPERTIES
};
static GParamSpec* rygel_lms_image_year_properties[RYGEL_LMS_IMAGE_YEAR_NUM_PROPERTIES];
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

#define RYGEL_LMS_TYPE_DATABASE (rygel_lms_database_get_type ())
#define RYGEL_LMS_DATABASE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_LMS_TYPE_DATABASE, RygelLMSDatabase))
#define RYGEL_LMS_DATABASE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_LMS_TYPE_DATABASE, RygelLMSDatabaseClass))
#define RYGEL_LMS_IS_DATABASE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_LMS_TYPE_DATABASE))
#define RYGEL_LMS_IS_DATABASE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_LMS_TYPE_DATABASE))
#define RYGEL_LMS_DATABASE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_LMS_TYPE_DATABASE, RygelLMSDatabaseClass))

typedef struct _RygelLMSDatabase RygelLMSDatabase;
typedef struct _RygelLMSDatabaseClass RygelLMSDatabaseClass;

struct _RygelLMSCategoryContainer {
	RygelMediaContainer parent_instance;
	RygelLMSCategoryContainerPrivate * priv;
	RygelDatabaseCursor* cursor_all;
	RygelDatabaseCursor* cursor_find_object;
	RygelDatabaseCursor* cursor_added;
	RygelDatabaseCursor* cursor_removed;
	gchar* child_prefix;
	gchar* ref_prefix;
};

struct _RygelLMSCategoryContainerClass {
	RygelMediaContainerClass parent_class;
	RygelMediaObject* (*object_from_statement) (RygelLMSCategoryContainer* self, sqlite3_stmt* statement);
	gchar* (*get_sql_all_with_filter) (RygelLMSCategoryContainer* self, const gchar* filter);
	gchar* (*get_sql_count_with_filter) (RygelLMSCategoryContainer* self, const gchar* filter);
	guint (*get_child_count_with_filter) (RygelLMSCategoryContainer* self, const gchar* where_filter, GValueArray* args);
	RygelMediaObjects* (*get_children_with_filter) (RygelLMSCategoryContainer* self, const gchar* where_filter, GValueArray* args, const gchar* sort_criteria, guint offset, guint max_count);
};

struct _RygelLMSImageYear {
	RygelLMSCategoryContainer parent_instance;
	RygelLMSImageYearPrivate * priv;
};

struct _RygelLMSImageYearClass {
	RygelLMSCategoryContainerClass parent_class;
};

static gpointer rygel_lms_image_year_parent_class = NULL;

GType rygel_lms_category_container_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelLMSCategoryContainer, g_object_unref)
GType rygel_lms_image_year_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelLMSImageYear, g_object_unref)
#define RYGEL_LMS_IMAGE_YEAR_SQL_ALL_TEMPLATE "SELECT images.id, title, artist, date, width, height, path, size, " "dlna_profile, dlna_mime, strftime('%Y', date, 'unixepoch') as year " "FROM images, files " "WHERE dtime = 0 AND images.id = files.id AND year = '%s' " "LIMIT ? OFFSET ?;"
#define RYGEL_LMS_IMAGE_YEAR_SQL_COUNT_TEMPLATE "SELECT count(images.id), strftime('%Y', date, 'unixepoch') as year " "FROM images, files " "WHERE dtime = 0 AND images.id = files.id AND year = '%s';"
#define RYGEL_LMS_IMAGE_YEAR_SQL_FIND_OBJECT_TEMPLATE "SELECT images.id, title, artist, date, width, height, path, size, " "dlna_profile, dlna_mime, strftime('%Y', date, 'unixepoch') as year " "FROM images, files " "WHERE dtime = 0 AND files.id = ? AND images.id = files.id AND year = '" \
"%s';"
#define RYGEL_LMS_IMAGE_YEAR_SQL_ADDED_TEMPLATE "SELECT images.id, title, artist, date, width, height, path, size, " "dlna_profile, dlna_mime, strftime('%Y', date, 'unixepoch') as year " "FROM images, files " "WHERE dtime = 0 AND images.id = files.id AND year = '%s' " "AND update_id > ? AND update_id <= ?;"
#define RYGEL_LMS_IMAGE_YEAR_SQL_REMOVED_TEMPLATE "SELECT images.id, title, artist, date, width, height, path, size, " "dlna_profile, dlna_mime, strftime('%Y', date, 'unixepoch') as year " "FROM images, files " "WHERE dtime <> 0 AND images.id = files.id AND year = '%s' " "AND update_id > ? AND update_id <= ?;"
static RygelMediaObject* rygel_lms_image_year_real_object_from_statement (RygelLMSCategoryContainer* base,
                                                                   sqlite3_stmt* statement);
gchar* rygel_lms_category_container_build_child_id (RygelLMSCategoryContainer* self,
                                                    gint db_id);
gchar* rygel_lms_category_container_build_reference_id (RygelLMSCategoryContainer* self,
                                                        gint db_id);
static gchar* rygel_lms_image_year_get_sql_all (const gchar* year);
static gchar* rygel_lms_image_year_get_sql_find_object (const gchar* year);
static gchar* rygel_lms_image_year_get_sql_count (const gchar* year);
static gchar* rygel_lms_image_year_get_sql_added (const gchar* year);
static gchar* rygel_lms_image_year_get_sql_removed (const gchar* year);
GType rygel_lms_database_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelLMSDatabase, g_object_unref)
RygelLMSImageYear* rygel_lms_image_year_new (RygelMediaContainer* parent,
                                             const gchar* year,
                                             RygelLMSDatabase* lms_db);
RygelLMSImageYear* rygel_lms_image_year_construct (GType object_type,
                                                   RygelMediaContainer* parent,
                                                   const gchar* year,
                                                   RygelLMSDatabase* lms_db);
RygelLMSCategoryContainer* rygel_lms_category_container_construct (GType object_type,
                                                                   const gchar* db_id,
                                                                   RygelMediaContainer* parent,
                                                                   const gchar* title,
                                                                   RygelLMSDatabase* lms_db,
                                                                   const gchar* sql_all,
                                                                   const gchar* sql_find_object,
                                                                   const gchar* sql_count,
                                                                   const gchar* sql_added,
                                                                   const gchar* sql_removed);

static RygelMediaObject*
rygel_lms_image_year_real_object_from_statement (RygelLMSCategoryContainer* base,
                                                 sqlite3_stmt* statement)
{
	RygelLMSImageYear * self;
	gint id = 0;
	gchar* path = NULL;
	const gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* mime_type = NULL;
	const gchar* _tmp2_;
	gchar* _tmp3_;
	gboolean _tmp4_ = FALSE;
	const gchar* _tmp5_;
	gchar* title = NULL;
	const gchar* _tmp10_;
	gchar* _tmp11_;
	RygelImageItem* image = NULL;
	gchar* _tmp12_;
	gchar* _tmp13_;
	RygelImageItem* _tmp14_;
	RygelImageItem* _tmp15_;
	gchar* _tmp16_;
	gchar* _tmp17_;
	const gchar* _tmp18_;
	GTimeVal tv = {0};
	GTimeVal _tmp19_ = {0};
	gchar* _tmp20_;
	gchar* _tmp21_;
	const gchar* _tmp22_;
	const gchar* _tmp23_;
	GFile* file = NULL;
	const gchar* _tmp24_;
	GFile* _tmp25_;
	gchar* _tmp26_;
	gchar* _tmp27_;
	RygelMediaObject* result = NULL;
	self = (RygelLMSImageYear*) base;
	g_return_val_if_fail (statement != NULL, NULL);
	id = sqlite3_column_int (statement, 0);
	_tmp0_ = (const gchar*) sqlite3_column_text (statement, 6);
	_tmp1_ = g_strdup (_tmp0_);
	path = _tmp1_;
	_tmp2_ = (const gchar*) sqlite3_column_text (statement, 9);
	_tmp3_ = g_strdup (_tmp2_);
	mime_type = _tmp3_;
	_tmp5_ = mime_type;
	if (_tmp5_ == NULL) {
		_tmp4_ = TRUE;
	} else {
		const gchar* _tmp6_;
		gint _tmp7_;
		gint _tmp8_;
		_tmp6_ = mime_type;
		_tmp7_ = strlen (_tmp6_);
		_tmp8_ = _tmp7_;
		_tmp4_ = _tmp8_ == 0;
	}
	if (_tmp4_) {
		const gchar* _tmp9_;
		_tmp9_ = path;
		g_debug ("rygel-lms-image-year.vala:67: Image item %d (%s) has no MIME type", id, _tmp9_);
	}
	_tmp10_ = (const gchar*) sqlite3_column_text (statement, 1);
	_tmp11_ = g_strdup (_tmp10_);
	title = _tmp11_;
	_tmp12_ = rygel_lms_category_container_build_child_id ((RygelLMSCategoryContainer*) self, id);
	_tmp13_ = _tmp12_;
	_tmp14_ = rygel_image_item_new (_tmp13_, (RygelMediaContainer*) self, title, RYGEL_IMAGE_ITEM_UPNP_CLASS);
	_tmp15_ = _tmp14_;
	_g_free0 (_tmp13_);
	image = _tmp15_;
	_tmp16_ = rygel_lms_category_container_build_reference_id ((RygelLMSCategoryContainer*) self, id);
	_tmp17_ = _tmp16_;
	rygel_media_object_set_ref_id ((RygelMediaObject*) image, _tmp17_);
	_g_free0 (_tmp17_);
	_tmp18_ = (const gchar*) sqlite3_column_text (statement, 2);
	rygel_media_object_set_creator ((RygelMediaObject*) image, _tmp18_);
	_tmp19_.tv_sec = (glong) sqlite3_column_int (statement, 3);
	_tmp19_.tv_usec = (glong) 0;
	tv = _tmp19_;
	_tmp20_ = g_time_val_to_iso8601 (&tv);
	_tmp21_ = _tmp20_;
	rygel_media_object_set_date ((RygelMediaObject*) image, _tmp21_);
	_g_free0 (_tmp21_);
	rygel_visual_item_set_width ((RygelVisualItem*) image, sqlite3_column_int (statement, 4));
	rygel_visual_item_set_height ((RygelVisualItem*) image, sqlite3_column_int (statement, 5));
	rygel_media_file_item_set_size ((RygelMediaFileItem*) image, (gint64) sqlite3_column_int (statement, 7));
	_tmp22_ = mime_type;
	rygel_media_file_item_set_mime_type ((RygelMediaFileItem*) image, _tmp22_);
	_tmp23_ = (const gchar*) sqlite3_column_text (statement, 8);
	rygel_media_file_item_set_dlna_profile ((RygelMediaFileItem*) image, _tmp23_);
	_tmp24_ = path;
	_tmp25_ = g_file_new_for_path (_tmp24_);
	file = _tmp25_;
	_tmp26_ = g_file_get_uri (file);
	_tmp27_ = _tmp26_;
	rygel_media_object_add_uri ((RygelMediaObject*) image, _tmp27_);
	_g_free0 (_tmp27_);
	result = (RygelMediaObject*) image;
	_g_object_unref0 (file);
	_g_free0 (title);
	_g_free0 (mime_type);
	_g_free0 (path);
	return result;
}

static gchar*
rygel_lms_image_year_get_sql_all (const gchar* year)
{
	gchar* _tmp0_;
	gchar* result = NULL;
	g_return_val_if_fail (year != NULL, NULL);
	_tmp0_ = g_strdup_printf (RYGEL_LMS_IMAGE_YEAR_SQL_ALL_TEMPLATE, year);
	result = _tmp0_;
	return result;
}

static gchar*
rygel_lms_image_year_get_sql_find_object (const gchar* year)
{
	gchar* _tmp0_;
	gchar* result = NULL;
	g_return_val_if_fail (year != NULL, NULL);
	_tmp0_ = g_strdup_printf (RYGEL_LMS_IMAGE_YEAR_SQL_FIND_OBJECT_TEMPLATE, year);
	result = _tmp0_;
	return result;
}

static gchar*
rygel_lms_image_year_get_sql_count (const gchar* year)
{
	gchar* _tmp0_;
	gchar* result = NULL;
	g_return_val_if_fail (year != NULL, NULL);
	_tmp0_ = g_strdup_printf (RYGEL_LMS_IMAGE_YEAR_SQL_COUNT_TEMPLATE, year);
	result = _tmp0_;
	return result;
}

static gchar*
rygel_lms_image_year_get_sql_added (const gchar* year)
{
	gchar* _tmp0_;
	gchar* result = NULL;
	g_return_val_if_fail (year != NULL, NULL);
	_tmp0_ = g_strdup_printf (RYGEL_LMS_IMAGE_YEAR_SQL_ADDED_TEMPLATE, year);
	result = _tmp0_;
	return result;
}

static gchar*
rygel_lms_image_year_get_sql_removed (const gchar* year)
{
	gchar* _tmp0_;
	gchar* result = NULL;
	g_return_val_if_fail (year != NULL, NULL);
	_tmp0_ = g_strdup_printf (RYGEL_LMS_IMAGE_YEAR_SQL_REMOVED_TEMPLATE, year);
	result = _tmp0_;
	return result;
}

RygelLMSImageYear*
rygel_lms_image_year_construct (GType object_type,
                                RygelMediaContainer* parent,
                                const gchar* year,
                                RygelLMSDatabase* lms_db)
{
	RygelLMSImageYear * self = NULL;
	gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* _tmp2_;
	gchar* _tmp3_;
	gchar* _tmp4_;
	gchar* _tmp5_;
	gchar* _tmp6_;
	gchar* _tmp7_;
	gchar* _tmp8_;
	gchar* _tmp9_;
	gchar* _tmp10_;
	gchar* _tmp11_;
	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (year != NULL, NULL);
	g_return_val_if_fail (lms_db != NULL, NULL);
	_tmp0_ = g_strdup_printf ("%s", year);
	_tmp1_ = _tmp0_;
	_tmp2_ = rygel_lms_image_year_get_sql_all (year);
	_tmp3_ = _tmp2_;
	_tmp4_ = rygel_lms_image_year_get_sql_find_object (year);
	_tmp5_ = _tmp4_;
	_tmp6_ = rygel_lms_image_year_get_sql_count (year);
	_tmp7_ = _tmp6_;
	_tmp8_ = rygel_lms_image_year_get_sql_added (year);
	_tmp9_ = _tmp8_;
	_tmp10_ = rygel_lms_image_year_get_sql_removed (year);
	_tmp11_ = _tmp10_;
	self = (RygelLMSImageYear*) rygel_lms_category_container_construct (object_type, _tmp1_, parent, year, lms_db, _tmp3_, _tmp5_, _tmp7_, _tmp9_, _tmp11_);
	_g_free0 (_tmp11_);
	_g_free0 (_tmp9_);
	_g_free0 (_tmp7_);
	_g_free0 (_tmp5_);
	_g_free0 (_tmp3_);
	_g_free0 (_tmp1_);
	return self;
}

RygelLMSImageYear*
rygel_lms_image_year_new (RygelMediaContainer* parent,
                          const gchar* year,
                          RygelLMSDatabase* lms_db)
{
	return rygel_lms_image_year_construct (RYGEL_LMS_TYPE_IMAGE_YEAR, parent, year, lms_db);
}

static void
rygel_lms_image_year_class_init (RygelLMSImageYearClass * klass,
                                 gpointer klass_data)
{
	rygel_lms_image_year_parent_class = g_type_class_peek_parent (klass);
	((RygelLMSCategoryContainerClass *) klass)->object_from_statement = (RygelMediaObject* (*) (RygelLMSCategoryContainer*, sqlite3_stmt*)) rygel_lms_image_year_real_object_from_statement;
}

static void
rygel_lms_image_year_instance_init (RygelLMSImageYear * self,
                                    gpointer klass)
{
}

GType
rygel_lms_image_year_get_type (void)
{
	static volatile gsize rygel_lms_image_year_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_lms_image_year_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelLMSImageYearClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_lms_image_year_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelLMSImageYear), 0, (GInstanceInitFunc) rygel_lms_image_year_instance_init, NULL };
		GType rygel_lms_image_year_type_id;
		rygel_lms_image_year_type_id = g_type_register_static (RYGEL_LMS_TYPE_CATEGORY_CONTAINER, "RygelLMSImageYear", &g_define_type_info, 0);
		g_once_init_leave (&rygel_lms_image_year_type_id__volatile, rygel_lms_image_year_type_id);
	}
	return rygel_lms_image_year_type_id__volatile;
}

