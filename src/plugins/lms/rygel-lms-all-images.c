/* rygel-lms-all-images.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-lms-all-images.vala, do not modify */

/*
 * Copyright (C) 2013 Intel Corporation.
 *
 * Author: Jussi Kukkonen <jussi.kukkonen@intel.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <rygel-server.h>
#include <glib-object.h>
#include <sqlite3.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <rygel-db.h>
#include <gio/gio.h>
#include <glib/gi18n-lib.h>

#define RYGEL_LMS_TYPE_CATEGORY_CONTAINER (rygel_lms_category_container_get_type ())
#define RYGEL_LMS_CATEGORY_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_LMS_TYPE_CATEGORY_CONTAINER, RygelLMSCategoryContainer))
#define RYGEL_LMS_CATEGORY_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_LMS_TYPE_CATEGORY_CONTAINER, RygelLMSCategoryContainerClass))
#define RYGEL_LMS_IS_CATEGORY_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_LMS_TYPE_CATEGORY_CONTAINER))
#define RYGEL_LMS_IS_CATEGORY_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_LMS_TYPE_CATEGORY_CONTAINER))
#define RYGEL_LMS_CATEGORY_CONTAINER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_LMS_TYPE_CATEGORY_CONTAINER, RygelLMSCategoryContainerClass))

typedef struct _RygelLMSCategoryContainer RygelLMSCategoryContainer;
typedef struct _RygelLMSCategoryContainerClass RygelLMSCategoryContainerClass;
typedef struct _RygelLMSCategoryContainerPrivate RygelLMSCategoryContainerPrivate;

#define RYGEL_LMS_TYPE_ALL_IMAGES (rygel_lms_all_images_get_type ())
#define RYGEL_LMS_ALL_IMAGES(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_LMS_TYPE_ALL_IMAGES, RygelLMSAllImages))
#define RYGEL_LMS_ALL_IMAGES_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_LMS_TYPE_ALL_IMAGES, RygelLMSAllImagesClass))
#define RYGEL_LMS_IS_ALL_IMAGES(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_LMS_TYPE_ALL_IMAGES))
#define RYGEL_LMS_IS_ALL_IMAGES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_LMS_TYPE_ALL_IMAGES))
#define RYGEL_LMS_ALL_IMAGES_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_LMS_TYPE_ALL_IMAGES, RygelLMSAllImagesClass))

typedef struct _RygelLMSAllImages RygelLMSAllImages;
typedef struct _RygelLMSAllImagesClass RygelLMSAllImagesClass;
typedef struct _RygelLMSAllImagesPrivate RygelLMSAllImagesPrivate;
enum  {
	RYGEL_LMS_ALL_IMAGES_0_PROPERTY,
	RYGEL_LMS_ALL_IMAGES_NUM_PROPERTIES
};
static GParamSpec* rygel_lms_all_images_properties[RYGEL_LMS_ALL_IMAGES_NUM_PROPERTIES];
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

#define RYGEL_LMS_TYPE_DATABASE (rygel_lms_database_get_type ())
#define RYGEL_LMS_DATABASE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_LMS_TYPE_DATABASE, RygelLMSDatabase))
#define RYGEL_LMS_DATABASE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_LMS_TYPE_DATABASE, RygelLMSDatabaseClass))
#define RYGEL_LMS_IS_DATABASE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_LMS_TYPE_DATABASE))
#define RYGEL_LMS_IS_DATABASE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_LMS_TYPE_DATABASE))
#define RYGEL_LMS_DATABASE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_LMS_TYPE_DATABASE, RygelLMSDatabaseClass))

typedef struct _RygelLMSDatabase RygelLMSDatabase;
typedef struct _RygelLMSDatabaseClass RygelLMSDatabaseClass;

struct _RygelLMSCategoryContainer {
	RygelMediaContainer parent_instance;
	RygelLMSCategoryContainerPrivate * priv;
	RygelDatabaseCursor* cursor_all;
	RygelDatabaseCursor* cursor_find_object;
	RygelDatabaseCursor* cursor_added;
	RygelDatabaseCursor* cursor_removed;
	gchar* child_prefix;
	gchar* ref_prefix;
};

struct _RygelLMSCategoryContainerClass {
	RygelMediaContainerClass parent_class;
	RygelMediaObject* (*object_from_statement) (RygelLMSCategoryContainer* self, sqlite3_stmt* statement);
	gchar* (*get_sql_all_with_filter) (RygelLMSCategoryContainer* self, const gchar* filter);
	gchar* (*get_sql_count_with_filter) (RygelLMSCategoryContainer* self, const gchar* filter);
	guint (*get_child_count_with_filter) (RygelLMSCategoryContainer* self, const gchar* where_filter, GValueArray* args);
	RygelMediaObjects* (*get_children_with_filter) (RygelLMSCategoryContainer* self, const gchar* where_filter, GValueArray* args, const gchar* sort_criteria, guint offset, guint max_count);
};

struct _RygelLMSAllImages {
	RygelLMSCategoryContainer parent_instance;
	RygelLMSAllImagesPrivate * priv;
};

struct _RygelLMSAllImagesClass {
	RygelLMSCategoryContainerClass parent_class;
};

static gpointer rygel_lms_all_images_parent_class = NULL;

GType rygel_lms_category_container_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelLMSCategoryContainer, g_object_unref)
GType rygel_lms_all_images_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelLMSAllImages, g_object_unref)
#define RYGEL_LMS_ALL_IMAGES_SQL_ALL "SELECT images.id, title, artist, date, width, height, path, size, " "dlna_profile, dlna_mime " "FROM images, files " "WHERE dtime = 0 AND images.id = files.id " "LIMIT ? OFFSET ?;"
#define RYGEL_LMS_ALL_IMAGES_SQL_COUNT "SELECT count(images.id) " "FROM images, files " "WHERE dtime = 0 AND images.id = files.id;"
#define RYGEL_LMS_ALL_IMAGES_SQL_FIND_OBJECT "SELECT images.id, title, artist, date, width, height, path, size, " "dlna_profile, dlna_mime " "FROM images, files " "WHERE dtime = 0 AND files.id = ? AND images.id = files.id;"
#define RYGEL_LMS_ALL_IMAGES_SQL_ADDED "SELECT images.id, title, artist, date, width, height, path, size, " "dlna_profile, dlna_mime " "FROM images, files " "WHERE dtime = 0 AND images.id = files.id " "AND update_id > ? AND update_id <= ?;"
#define RYGEL_LMS_ALL_IMAGES_SQL_REMOVED "SELECT images.id, title, artist, date, width, height, path, size, " "dlna_profile, dlna_mime " "FROM images, files " "WHERE dtime <> 0 AND images.id = files.id " "AND update_id > ? AND update_id <= ?;"
static RygelMediaObject* rygel_lms_all_images_real_object_from_statement (RygelLMSCategoryContainer* base,
                                                                   sqlite3_stmt* statement);
gchar* rygel_lms_category_container_build_child_id (RygelLMSCategoryContainer* self,
                                                    gint db_id);
GType rygel_lms_database_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelLMSDatabase, g_object_unref)
RygelLMSAllImages* rygel_lms_all_images_new (RygelMediaContainer* parent,
                                             RygelLMSDatabase* lms_db);
RygelLMSAllImages* rygel_lms_all_images_construct (GType object_type,
                                                   RygelMediaContainer* parent,
                                                   RygelLMSDatabase* lms_db);
RygelLMSCategoryContainer* rygel_lms_category_container_construct (GType object_type,
                                                                   const gchar* db_id,
                                                                   RygelMediaContainer* parent,
                                                                   const gchar* title,
                                                                   RygelLMSDatabase* lms_db,
                                                                   const gchar* sql_all,
                                                                   const gchar* sql_find_object,
                                                                   const gchar* sql_count,
                                                                   const gchar* sql_added,
                                                                   const gchar* sql_removed);

static RygelMediaObject*
rygel_lms_all_images_real_object_from_statement (RygelLMSCategoryContainer* base,
                                                 sqlite3_stmt* statement)
{
	RygelLMSAllImages * self;
	gint id = 0;
	gchar* path = NULL;
	const gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* mime_type = NULL;
	const gchar* _tmp2_;
	gchar* _tmp3_;
	gboolean _tmp4_ = FALSE;
	const gchar* _tmp5_;
	gchar* title = NULL;
	const gchar* _tmp10_;
	gchar* _tmp11_;
	RygelImageItem* image = NULL;
	gchar* _tmp12_;
	gchar* _tmp13_;
	RygelImageItem* _tmp14_;
	RygelImageItem* _tmp15_;
	const gchar* _tmp16_;
	GTimeVal tv = {0};
	GTimeVal _tmp17_ = {0};
	gchar* _tmp18_;
	gchar* _tmp19_;
	const gchar* _tmp20_;
	const gchar* _tmp21_;
	GFile* file = NULL;
	const gchar* _tmp22_;
	GFile* _tmp23_;
	gchar* _tmp24_;
	gchar* _tmp25_;
	RygelMediaObject* result = NULL;
	self = (RygelLMSAllImages*) base;
	g_return_val_if_fail (statement != NULL, NULL);
	id = sqlite3_column_int (statement, 0);
	_tmp0_ = (const gchar*) sqlite3_column_text (statement, 6);
	_tmp1_ = g_strdup (_tmp0_);
	path = _tmp1_;
	_tmp2_ = (const gchar*) sqlite3_column_text (statement, 9);
	_tmp3_ = g_strdup (_tmp2_);
	mime_type = _tmp3_;
	_tmp5_ = mime_type;
	if (_tmp5_ == NULL) {
		_tmp4_ = TRUE;
	} else {
		const gchar* _tmp6_;
		gint _tmp7_;
		gint _tmp8_;
		_tmp6_ = mime_type;
		_tmp7_ = strlen (_tmp6_);
		_tmp8_ = _tmp7_;
		_tmp4_ = _tmp8_ == 0;
	}
	if (_tmp4_) {
		const gchar* _tmp9_;
		_tmp9_ = path;
		g_debug ("rygel-lms-all-images.vala:67: Image item %d (%s) has no MIME type", id, _tmp9_);
	}
	_tmp10_ = (const gchar*) sqlite3_column_text (statement, 1);
	_tmp11_ = g_strdup (_tmp10_);
	title = _tmp11_;
	_tmp12_ = rygel_lms_category_container_build_child_id ((RygelLMSCategoryContainer*) self, id);
	_tmp13_ = _tmp12_;
	_tmp14_ = rygel_image_item_new (_tmp13_, (RygelMediaContainer*) self, title, RYGEL_IMAGE_ITEM_UPNP_CLASS);
	_tmp15_ = _tmp14_;
	_g_free0 (_tmp13_);
	image = _tmp15_;
	_tmp16_ = (const gchar*) sqlite3_column_text (statement, 2);
	rygel_media_object_set_creator ((RygelMediaObject*) image, _tmp16_);
	_tmp17_.tv_sec = (glong) sqlite3_column_int (statement, 3);
	_tmp17_.tv_usec = (glong) 0;
	tv = _tmp17_;
	_tmp18_ = g_time_val_to_iso8601 (&tv);
	_tmp19_ = _tmp18_;
	rygel_media_object_set_date ((RygelMediaObject*) image, _tmp19_);
	_g_free0 (_tmp19_);
	rygel_visual_item_set_width ((RygelVisualItem*) image, sqlite3_column_int (statement, 4));
	rygel_visual_item_set_height ((RygelVisualItem*) image, sqlite3_column_int (statement, 5));
	rygel_media_file_item_set_size ((RygelMediaFileItem*) image, (gint64) sqlite3_column_int (statement, 7));
	_tmp20_ = mime_type;
	rygel_media_file_item_set_mime_type ((RygelMediaFileItem*) image, _tmp20_);
	_tmp21_ = (const gchar*) sqlite3_column_text (statement, 8);
	rygel_media_file_item_set_dlna_profile ((RygelMediaFileItem*) image, _tmp21_);
	_tmp22_ = path;
	_tmp23_ = g_file_new_for_path (_tmp22_);
	file = _tmp23_;
	_tmp24_ = g_file_get_uri (file);
	_tmp25_ = _tmp24_;
	rygel_media_object_add_uri ((RygelMediaObject*) image, _tmp25_);
	_g_free0 (_tmp25_);
	result = (RygelMediaObject*) image;
	_g_object_unref0 (file);
	_g_free0 (title);
	_g_free0 (mime_type);
	_g_free0 (path);
	return result;
}

RygelLMSAllImages*
rygel_lms_all_images_construct (GType object_type,
                                RygelMediaContainer* parent,
                                RygelLMSDatabase* lms_db)
{
	RygelLMSAllImages * self = NULL;
	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (lms_db != NULL, NULL);
	self = (RygelLMSAllImages*) rygel_lms_category_container_construct (object_type, "all", parent, _ ("All"), lms_db, RYGEL_LMS_ALL_IMAGES_SQL_ALL, RYGEL_LMS_ALL_IMAGES_SQL_FIND_OBJECT, RYGEL_LMS_ALL_IMAGES_SQL_COUNT, RYGEL_LMS_ALL_IMAGES_SQL_ADDED, RYGEL_LMS_ALL_IMAGES_SQL_REMOVED);
	return self;
}

RygelLMSAllImages*
rygel_lms_all_images_new (RygelMediaContainer* parent,
                          RygelLMSDatabase* lms_db)
{
	return rygel_lms_all_images_construct (RYGEL_LMS_TYPE_ALL_IMAGES, parent, lms_db);
}

static void
rygel_lms_all_images_class_init (RygelLMSAllImagesClass * klass,
                                 gpointer klass_data)
{
	rygel_lms_all_images_parent_class = g_type_class_peek_parent (klass);
	((RygelLMSCategoryContainerClass *) klass)->object_from_statement = (RygelMediaObject* (*) (RygelLMSCategoryContainer*, sqlite3_stmt*)) rygel_lms_all_images_real_object_from_statement;
}

static void
rygel_lms_all_images_instance_init (RygelLMSAllImages * self,
                                    gpointer klass)
{
}

GType
rygel_lms_all_images_get_type (void)
{
	static volatile gsize rygel_lms_all_images_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_lms_all_images_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelLMSAllImagesClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_lms_all_images_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelLMSAllImages), 0, (GInstanceInitFunc) rygel_lms_all_images_instance_init, NULL };
		GType rygel_lms_all_images_type_id;
		rygel_lms_all_images_type_id = g_type_register_static (RYGEL_LMS_TYPE_CATEGORY_CONTAINER, "RygelLMSAllImages", &g_define_type_info, 0);
		g_once_init_leave (&rygel_lms_all_images_type_id__volatile, rygel_lms_all_images_type_id);
	}
	return rygel_lms_all_images_type_id__volatile;
}

