/* rygel-tracker-query-triplet.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-tracker-query-triplet.vala, do not modify */

/*
 * Copyright (C) 2008 Nokia Corporation.
 *
 * Author: Zeeshan Ali <zeenix@gmail.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <gobject/gvaluecollector.h>

#define RYGEL_TRACKER_TYPE_QUERY_TRIPLET (rygel_tracker_query_triplet_get_type ())
#define RYGEL_TRACKER_QUERY_TRIPLET(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TRACKER_TYPE_QUERY_TRIPLET, RygelTrackerQueryTriplet))
#define RYGEL_TRACKER_QUERY_TRIPLET_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TRACKER_TYPE_QUERY_TRIPLET, RygelTrackerQueryTripletClass))
#define RYGEL_TRACKER_IS_QUERY_TRIPLET(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TRACKER_TYPE_QUERY_TRIPLET))
#define RYGEL_TRACKER_IS_QUERY_TRIPLET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TRACKER_TYPE_QUERY_TRIPLET))
#define RYGEL_TRACKER_QUERY_TRIPLET_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TRACKER_TYPE_QUERY_TRIPLET, RygelTrackerQueryTripletClass))

typedef struct _RygelTrackerQueryTriplet RygelTrackerQueryTriplet;
typedef struct _RygelTrackerQueryTripletClass RygelTrackerQueryTripletClass;
typedef struct _RygelTrackerQueryTripletPrivate RygelTrackerQueryTripletPrivate;
#define _g_free0(var) (var = (g_free (var), NULL))
#define _rygel_tracker_query_triplet_unref0(var) ((var == NULL) ? NULL : (var = (rygel_tracker_query_triplet_unref (var), NULL)))
typedef struct _RygelTrackerParamSpecQueryTriplet RygelTrackerParamSpecQueryTriplet;

struct _RygelTrackerQueryTriplet {
	GTypeInstance parent_instance;
	volatile int ref_count;
	RygelTrackerQueryTripletPrivate * priv;
	gchar* graph;
	gchar* subject;
	gchar* predicate;
	gchar* obj;
	RygelTrackerQueryTriplet* next;
};

struct _RygelTrackerQueryTripletClass {
	GTypeClass parent_class;
	void (*finalize) (RygelTrackerQueryTriplet *self);
};

struct _RygelTrackerParamSpecQueryTriplet {
	GParamSpec parent_instance;
};

static gpointer rygel_tracker_query_triplet_parent_class = NULL;

gpointer rygel_tracker_query_triplet_ref (gpointer instance);
void rygel_tracker_query_triplet_unref (gpointer instance);
GParamSpec* rygel_tracker_param_spec_query_triplet (const gchar* name,
                                                    const gchar* nick,
                                                    const gchar* blurb,
                                                    GType object_type,
                                                    GParamFlags flags);
void rygel_tracker_value_set_query_triplet (GValue* value,
                                            gpointer v_object);
void rygel_tracker_value_take_query_triplet (GValue* value,
                                             gpointer v_object);
gpointer rygel_tracker_value_get_query_triplet (const GValue* value);
GType rygel_tracker_query_triplet_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelTrackerQueryTriplet, rygel_tracker_query_triplet_unref)
RygelTrackerQueryTriplet* rygel_tracker_query_triplet_new (const gchar* subject,
                                                           const gchar* predicate,
                                                           const gchar* obj);
RygelTrackerQueryTriplet* rygel_tracker_query_triplet_construct (GType object_type,
                                                                 const gchar* subject,
                                                                 const gchar* predicate,
                                                                 const gchar* obj);
RygelTrackerQueryTriplet* rygel_tracker_query_triplet_new_with_graph (const gchar* graph,
                                                                      const gchar* subject,
                                                                      const gchar* predicate,
                                                                      const gchar* object);
RygelTrackerQueryTriplet* rygel_tracker_query_triplet_construct_with_graph (GType object_type,
                                                                            const gchar* graph,
                                                                            const gchar* subject,
                                                                            const gchar* predicate,
                                                                            const gchar* object);
RygelTrackerQueryTriplet* rygel_tracker_query_triplet_new_chain (const gchar* subject,
                                                                 const gchar* predicate,
                                                                 RygelTrackerQueryTriplet* next);
RygelTrackerQueryTriplet* rygel_tracker_query_triplet_construct_chain (GType object_type,
                                                                       const gchar* subject,
                                                                       const gchar* predicate,
                                                                       RygelTrackerQueryTriplet* next);
RygelTrackerQueryTriplet* rygel_tracker_query_triplet_new_clone (RygelTrackerQueryTriplet* triplet);
RygelTrackerQueryTriplet* rygel_tracker_query_triplet_construct_clone (GType object_type,
                                                                       RygelTrackerQueryTriplet* triplet);
gboolean rygel_tracker_query_triplet_equal_func (RygelTrackerQueryTriplet* a,
                                                 RygelTrackerQueryTriplet* b);
gchar* rygel_tracker_query_triplet_to_string (RygelTrackerQueryTriplet* self,
                                              gboolean include_subject);
static void rygel_tracker_query_triplet_finalize (RygelTrackerQueryTriplet * obj);

RygelTrackerQueryTriplet*
rygel_tracker_query_triplet_construct (GType object_type,
                                       const gchar* subject,
                                       const gchar* predicate,
                                       const gchar* obj)
{
	RygelTrackerQueryTriplet* self = NULL;
	gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* _tmp2_;
	g_return_val_if_fail (subject != NULL, NULL);
	g_return_val_if_fail (predicate != NULL, NULL);
	g_return_val_if_fail (obj != NULL, NULL);
	self = (RygelTrackerQueryTriplet*) g_type_create_instance (object_type);
	_g_free0 (self->graph);
	self->graph = NULL;
	_tmp0_ = g_strdup (subject);
	_g_free0 (self->subject);
	self->subject = _tmp0_;
	_tmp1_ = g_strdup (predicate);
	_g_free0 (self->predicate);
	self->predicate = _tmp1_;
	_tmp2_ = g_strdup (obj);
	_g_free0 (self->obj);
	self->obj = _tmp2_;
	return self;
}

RygelTrackerQueryTriplet*
rygel_tracker_query_triplet_new (const gchar* subject,
                                 const gchar* predicate,
                                 const gchar* obj)
{
	return rygel_tracker_query_triplet_construct (RYGEL_TRACKER_TYPE_QUERY_TRIPLET, subject, predicate, obj);
}

RygelTrackerQueryTriplet*
rygel_tracker_query_triplet_construct_with_graph (GType object_type,
                                                  const gchar* graph,
                                                  const gchar* subject,
                                                  const gchar* predicate,
                                                  const gchar* object)
{
	RygelTrackerQueryTriplet* self = NULL;
	gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* _tmp2_;
	gchar* _tmp3_;
	g_return_val_if_fail (graph != NULL, NULL);
	g_return_val_if_fail (subject != NULL, NULL);
	g_return_val_if_fail (predicate != NULL, NULL);
	g_return_val_if_fail (object != NULL, NULL);
	self = (RygelTrackerQueryTriplet*) g_type_create_instance (object_type);
	_tmp0_ = g_strdup (graph);
	_g_free0 (self->graph);
	self->graph = _tmp0_;
	_tmp1_ = g_strdup (subject);
	_g_free0 (self->subject);
	self->subject = _tmp1_;
	_tmp2_ = g_strdup (predicate);
	_g_free0 (self->predicate);
	self->predicate = _tmp2_;
	_tmp3_ = g_strdup (object);
	_g_free0 (self->obj);
	self->obj = _tmp3_;
	return self;
}

RygelTrackerQueryTriplet*
rygel_tracker_query_triplet_new_with_graph (const gchar* graph,
                                            const gchar* subject,
                                            const gchar* predicate,
                                            const gchar* object)
{
	return rygel_tracker_query_triplet_construct_with_graph (RYGEL_TRACKER_TYPE_QUERY_TRIPLET, graph, subject, predicate, object);
}

static gpointer
_rygel_tracker_query_triplet_ref0 (gpointer self)
{
	return self ? rygel_tracker_query_triplet_ref (self) : NULL;
}

RygelTrackerQueryTriplet*
rygel_tracker_query_triplet_construct_chain (GType object_type,
                                             const gchar* subject,
                                             const gchar* predicate,
                                             RygelTrackerQueryTriplet* next)
{
	RygelTrackerQueryTriplet* self = NULL;
	gchar* _tmp0_;
	gchar* _tmp1_;
	RygelTrackerQueryTriplet* _tmp2_;
	g_return_val_if_fail (subject != NULL, NULL);
	g_return_val_if_fail (predicate != NULL, NULL);
	g_return_val_if_fail (next != NULL, NULL);
	self = (RygelTrackerQueryTriplet*) g_type_create_instance (object_type);
	_tmp0_ = g_strdup (subject);
	_g_free0 (self->subject);
	self->subject = _tmp0_;
	_tmp1_ = g_strdup (predicate);
	_g_free0 (self->predicate);
	self->predicate = _tmp1_;
	_tmp2_ = _rygel_tracker_query_triplet_ref0 (next);
	_rygel_tracker_query_triplet_unref0 (self->next);
	self->next = _tmp2_;
	return self;
}

RygelTrackerQueryTriplet*
rygel_tracker_query_triplet_new_chain (const gchar* subject,
                                       const gchar* predicate,
                                       RygelTrackerQueryTriplet* next)
{
	return rygel_tracker_query_triplet_construct_chain (RYGEL_TRACKER_TYPE_QUERY_TRIPLET, subject, predicate, next);
}

RygelTrackerQueryTriplet*
rygel_tracker_query_triplet_construct_clone (GType object_type,
                                             RygelTrackerQueryTriplet* triplet)
{
	RygelTrackerQueryTriplet* self = NULL;
	const gchar* _tmp0_;
	gchar* _tmp1_;
	const gchar* _tmp2_;
	gchar* _tmp3_;
	const gchar* _tmp4_;
	gchar* _tmp5_;
	RygelTrackerQueryTriplet* _tmp6_;
	g_return_val_if_fail (triplet != NULL, NULL);
	self = (RygelTrackerQueryTriplet*) g_type_create_instance (object_type);
	_tmp0_ = triplet->graph;
	_tmp1_ = g_strdup (_tmp0_);
	_g_free0 (self->graph);
	self->graph = _tmp1_;
	_tmp2_ = triplet->subject;
	_tmp3_ = g_strdup (_tmp2_);
	_g_free0 (self->subject);
	self->subject = _tmp3_;
	_tmp4_ = triplet->predicate;
	_tmp5_ = g_strdup (_tmp4_);
	_g_free0 (self->predicate);
	self->predicate = _tmp5_;
	_tmp6_ = triplet->next;
	if (_tmp6_ != NULL) {
		RygelTrackerQueryTriplet* _tmp7_;
		RygelTrackerQueryTriplet* _tmp8_;
		_tmp7_ = triplet->next;
		_tmp8_ = _rygel_tracker_query_triplet_ref0 (_tmp7_);
		_rygel_tracker_query_triplet_unref0 (self->next);
		self->next = _tmp8_;
	} else {
		const gchar* _tmp9_;
		gchar* _tmp10_;
		_tmp9_ = triplet->obj;
		_tmp10_ = g_strdup (_tmp9_);
		_g_free0 (self->obj);
		self->obj = _tmp10_;
	}
	return self;
}

RygelTrackerQueryTriplet*
rygel_tracker_query_triplet_new_clone (RygelTrackerQueryTriplet* triplet)
{
	return rygel_tracker_query_triplet_construct_clone (RYGEL_TRACKER_TYPE_QUERY_TRIPLET, triplet);
}

gboolean
rygel_tracker_query_triplet_equal_func (RygelTrackerQueryTriplet* a,
                                        RygelTrackerQueryTriplet* b)
{
	gboolean chain_equal = FALSE;
	gboolean _tmp0_ = FALSE;
	RygelTrackerQueryTriplet* _tmp1_;
	gboolean _tmp7_ = FALSE;
	gboolean _tmp8_ = FALSE;
	gboolean _tmp9_ = FALSE;
	gboolean _tmp10_ = FALSE;
	const gchar* _tmp11_;
	const gchar* _tmp12_;
	gboolean result = FALSE;
	g_return_val_if_fail (a != NULL, FALSE);
	g_return_val_if_fail (b != NULL, FALSE);
	_tmp1_ = a->next;
	if (_tmp1_ != NULL) {
		RygelTrackerQueryTriplet* _tmp2_;
		_tmp2_ = b->next;
		_tmp0_ = _tmp2_ != NULL;
	} else {
		_tmp0_ = FALSE;
	}
	if (_tmp0_) {
		RygelTrackerQueryTriplet* _tmp3_;
		RygelTrackerQueryTriplet* _tmp4_;
		_tmp3_ = a->next;
		_tmp4_ = b->next;
		chain_equal = rygel_tracker_query_triplet_equal_func (_tmp3_, _tmp4_);
	} else {
		RygelTrackerQueryTriplet* _tmp5_;
		RygelTrackerQueryTriplet* _tmp6_;
		_tmp5_ = a->next;
		_tmp6_ = b->next;
		chain_equal = _tmp5_ == _tmp6_;
	}
	_tmp11_ = a->graph;
	_tmp12_ = b->graph;
	if (g_strcmp0 (_tmp11_, _tmp12_) == 0) {
		const gchar* _tmp13_;
		const gchar* _tmp14_;
		_tmp13_ = a->subject;
		_tmp14_ = b->subject;
		_tmp10_ = g_strcmp0 (_tmp13_, _tmp14_) == 0;
	} else {
		_tmp10_ = FALSE;
	}
	if (_tmp10_) {
		const gchar* _tmp15_;
		const gchar* _tmp16_;
		_tmp15_ = a->obj;
		_tmp16_ = b->obj;
		_tmp9_ = g_strcmp0 (_tmp15_, _tmp16_) == 0;
	} else {
		_tmp9_ = FALSE;
	}
	if (_tmp9_) {
		const gchar* _tmp17_;
		const gchar* _tmp18_;
		_tmp17_ = a->predicate;
		_tmp18_ = b->predicate;
		_tmp8_ = g_strcmp0 (_tmp17_, _tmp18_) == 0;
	} else {
		_tmp8_ = FALSE;
	}
	if (_tmp8_) {
		_tmp7_ = chain_equal;
	} else {
		_tmp7_ = FALSE;
	}
	result = _tmp7_;
	return result;
}

gchar*
rygel_tracker_query_triplet_to_string (RygelTrackerQueryTriplet* self,
                                       gboolean include_subject)
{
	gchar* str = NULL;
	gchar* _tmp0_;
	const gchar* _tmp6_;
	const gchar* _tmp7_;
	gchar* _tmp8_;
	gchar* _tmp9_;
	gchar* _tmp10_;
	RygelTrackerQueryTriplet* _tmp11_;
	gchar* result = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = g_strdup ("");
	str = _tmp0_;
	if (include_subject) {
		const gchar* _tmp1_;
		const gchar* _tmp2_;
		gchar* _tmp3_;
		gchar* _tmp4_;
		gchar* _tmp5_;
		_tmp1_ = str;
		_tmp2_ = self->subject;
		_tmp3_ = g_strconcat (" ", _tmp2_, NULL);
		_tmp4_ = _tmp3_;
		_tmp5_ = g_strconcat (_tmp1_, _tmp4_, NULL);
		_g_free0 (str);
		str = _tmp5_;
		_g_free0 (_tmp4_);
	}
	_tmp6_ = str;
	_tmp7_ = self->predicate;
	_tmp8_ = g_strconcat (" ", _tmp7_, NULL);
	_tmp9_ = _tmp8_;
	_tmp10_ = g_strconcat (_tmp6_, _tmp9_, NULL);
	_g_free0 (str);
	str = _tmp10_;
	_g_free0 (_tmp9_);
	_tmp11_ = self->next;
	if (_tmp11_ != NULL) {
		const gchar* _tmp12_;
		RygelTrackerQueryTriplet* _tmp13_;
		gchar* _tmp14_;
		gchar* _tmp15_;
		gchar* _tmp16_;
		gchar* _tmp17_;
		gchar* _tmp18_;
		gchar* _tmp19_;
		gchar* _tmp20_;
		_tmp12_ = str;
		_tmp13_ = self->next;
		_tmp14_ = rygel_tracker_query_triplet_to_string (_tmp13_, TRUE);
		_tmp15_ = _tmp14_;
		_tmp16_ = g_strconcat (" [ ", _tmp15_, NULL);
		_tmp17_ = _tmp16_;
		_tmp18_ = g_strconcat (_tmp17_, " ] ", NULL);
		_tmp19_ = _tmp18_;
		_tmp20_ = g_strconcat (_tmp12_, _tmp19_, NULL);
		_g_free0 (str);
		str = _tmp20_;
		_g_free0 (_tmp19_);
		_g_free0 (_tmp17_);
		_g_free0 (_tmp15_);
	} else {
		const gchar* _tmp21_;
		const gchar* _tmp22_;
		gchar* _tmp23_;
		gchar* _tmp24_;
		gchar* _tmp25_;
		_tmp21_ = str;
		_tmp22_ = self->obj;
		_tmp23_ = g_strconcat (" ", _tmp22_, NULL);
		_tmp24_ = _tmp23_;
		_tmp25_ = g_strconcat (_tmp21_, _tmp24_, NULL);
		_g_free0 (str);
		str = _tmp25_;
		_g_free0 (_tmp24_);
	}
	result = str;
	return result;
}

static void
rygel_tracker_value_query_triplet_init (GValue* value)
{
	value->data[0].v_pointer = NULL;
}

static void
rygel_tracker_value_query_triplet_free_value (GValue* value)
{
	if (value->data[0].v_pointer) {
		rygel_tracker_query_triplet_unref (value->data[0].v_pointer);
	}
}

static void
rygel_tracker_value_query_triplet_copy_value (const GValue* src_value,
                                              GValue* dest_value)
{
	if (src_value->data[0].v_pointer) {
		dest_value->data[0].v_pointer = rygel_tracker_query_triplet_ref (src_value->data[0].v_pointer);
	} else {
		dest_value->data[0].v_pointer = NULL;
	}
}

static gpointer
rygel_tracker_value_query_triplet_peek_pointer (const GValue* value)
{
	return value->data[0].v_pointer;
}

static gchar*
rygel_tracker_value_query_triplet_collect_value (GValue* value,
                                                 guint n_collect_values,
                                                 GTypeCValue* collect_values,
                                                 guint collect_flags)
{
	if (collect_values[0].v_pointer) {
		RygelTrackerQueryTriplet * object;
		object = collect_values[0].v_pointer;
		if (object->parent_instance.g_class == NULL) {
			return g_strconcat ("invalid unclassed object pointer for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
		} else if (!g_value_type_compatible (G_TYPE_FROM_INSTANCE (object), G_VALUE_TYPE (value))) {
			return g_strconcat ("invalid object type `", g_type_name (G_TYPE_FROM_INSTANCE (object)), "' for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
		}
		value->data[0].v_pointer = rygel_tracker_query_triplet_ref (object);
	} else {
		value->data[0].v_pointer = NULL;
	}
	return NULL;
}

static gchar*
rygel_tracker_value_query_triplet_lcopy_value (const GValue* value,
                                               guint n_collect_values,
                                               GTypeCValue* collect_values,
                                               guint collect_flags)
{
	RygelTrackerQueryTriplet ** object_p;
	object_p = collect_values[0].v_pointer;
	if (!object_p) {
		return g_strdup_printf ("value location for `%s' passed as NULL", G_VALUE_TYPE_NAME (value));
	}
	if (!value->data[0].v_pointer) {
		*object_p = NULL;
	} else if (collect_flags & G_VALUE_NOCOPY_CONTENTS) {
		*object_p = value->data[0].v_pointer;
	} else {
		*object_p = rygel_tracker_query_triplet_ref (value->data[0].v_pointer);
	}
	return NULL;
}

GParamSpec*
rygel_tracker_param_spec_query_triplet (const gchar* name,
                                        const gchar* nick,
                                        const gchar* blurb,
                                        GType object_type,
                                        GParamFlags flags)
{
	RygelTrackerParamSpecQueryTriplet* spec;
	g_return_val_if_fail (g_type_is_a (object_type, RYGEL_TRACKER_TYPE_QUERY_TRIPLET), NULL);
	spec = g_param_spec_internal (G_TYPE_PARAM_OBJECT, name, nick, blurb, flags);
	G_PARAM_SPEC (spec)->value_type = object_type;
	return G_PARAM_SPEC (spec);
}

gpointer
rygel_tracker_value_get_query_triplet (const GValue* value)
{
	g_return_val_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, RYGEL_TRACKER_TYPE_QUERY_TRIPLET), NULL);
	return value->data[0].v_pointer;
}

void
rygel_tracker_value_set_query_triplet (GValue* value,
                                       gpointer v_object)
{
	RygelTrackerQueryTriplet * old;
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, RYGEL_TRACKER_TYPE_QUERY_TRIPLET));
	old = value->data[0].v_pointer;
	if (v_object) {
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, RYGEL_TRACKER_TYPE_QUERY_TRIPLET));
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
		value->data[0].v_pointer = v_object;
		rygel_tracker_query_triplet_ref (value->data[0].v_pointer);
	} else {
		value->data[0].v_pointer = NULL;
	}
	if (old) {
		rygel_tracker_query_triplet_unref (old);
	}
}

void
rygel_tracker_value_take_query_triplet (GValue* value,
                                        gpointer v_object)
{
	RygelTrackerQueryTriplet * old;
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, RYGEL_TRACKER_TYPE_QUERY_TRIPLET));
	old = value->data[0].v_pointer;
	if (v_object) {
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, RYGEL_TRACKER_TYPE_QUERY_TRIPLET));
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
		value->data[0].v_pointer = v_object;
	} else {
		value->data[0].v_pointer = NULL;
	}
	if (old) {
		rygel_tracker_query_triplet_unref (old);
	}
}

static void
rygel_tracker_query_triplet_class_init (RygelTrackerQueryTripletClass * klass,
                                        gpointer klass_data)
{
	rygel_tracker_query_triplet_parent_class = g_type_class_peek_parent (klass);
	((RygelTrackerQueryTripletClass *) klass)->finalize = rygel_tracker_query_triplet_finalize;
}

static void
rygel_tracker_query_triplet_instance_init (RygelTrackerQueryTriplet * self,
                                           gpointer klass)
{
	self->ref_count = 1;
}

static void
rygel_tracker_query_triplet_finalize (RygelTrackerQueryTriplet * obj)
{
	RygelTrackerQueryTriplet * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, RYGEL_TRACKER_TYPE_QUERY_TRIPLET, RygelTrackerQueryTriplet);
	g_signal_handlers_destroy (self);
	_g_free0 (self->graph);
	_g_free0 (self->subject);
	_g_free0 (self->predicate);
	_g_free0 (self->obj);
	_rygel_tracker_query_triplet_unref0 (self->next);
}

/**
 * Represents SPARQL Triplet
 */
GType
rygel_tracker_query_triplet_get_type (void)
{
	static volatile gsize rygel_tracker_query_triplet_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_tracker_query_triplet_type_id__volatile)) {
		static const GTypeValueTable g_define_type_value_table = { rygel_tracker_value_query_triplet_init, rygel_tracker_value_query_triplet_free_value, rygel_tracker_value_query_triplet_copy_value, rygel_tracker_value_query_triplet_peek_pointer, "p", rygel_tracker_value_query_triplet_collect_value, "p", rygel_tracker_value_query_triplet_lcopy_value };
		static const GTypeInfo g_define_type_info = { sizeof (RygelTrackerQueryTripletClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_tracker_query_triplet_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelTrackerQueryTriplet), 0, (GInstanceInitFunc) rygel_tracker_query_triplet_instance_init, &g_define_type_value_table };
		static const GTypeFundamentalInfo g_define_type_fundamental_info = { (G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE | G_TYPE_FLAG_DERIVABLE | G_TYPE_FLAG_DEEP_DERIVABLE) };
		GType rygel_tracker_query_triplet_type_id;
		rygel_tracker_query_triplet_type_id = g_type_register_fundamental (g_type_fundamental_next (), "RygelTrackerQueryTriplet", &g_define_type_info, &g_define_type_fundamental_info, 0);
		g_once_init_leave (&rygel_tracker_query_triplet_type_id__volatile, rygel_tracker_query_triplet_type_id);
	}
	return rygel_tracker_query_triplet_type_id__volatile;
}

gpointer
rygel_tracker_query_triplet_ref (gpointer instance)
{
	RygelTrackerQueryTriplet * self;
	self = instance;
	g_atomic_int_inc (&self->ref_count);
	return instance;
}

void
rygel_tracker_query_triplet_unref (gpointer instance)
{
	RygelTrackerQueryTriplet * self;
	self = instance;
	if (g_atomic_int_dec_and_test (&self->ref_count)) {
		RYGEL_TRACKER_QUERY_TRIPLET_GET_CLASS (self)->finalize (self);
		g_type_free_instance ((GTypeInstance *) self);
	}
}

