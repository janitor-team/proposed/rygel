/* rygel-gst-launch-audio-item.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-gst-launch-audio-item.vala, do not modify */

/*
 * Copyright (C) 2009 Thijs Vermeir <thijsvermeir@gmail.com>
 * Copyright (C) 2010 Nokia Corporation.
 * Copyright (C) 2012 Intel Corporation.
 *
 * Author: Thijs Vermeir <thijsvermeir@gmail.com>
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *                               <zeeshan.ali@nokia.com>
 *         Jens Georg <jensg@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <rygel-server.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <libsoup/soup.h>

#define RYGEL_GST_LAUNCH_TYPE_AUDIO_ITEM (rygel_gst_launch_audio_item_get_type ())
#define RYGEL_GST_LAUNCH_AUDIO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_GST_LAUNCH_TYPE_AUDIO_ITEM, RygelGstLaunchAudioItem))
#define RYGEL_GST_LAUNCH_AUDIO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_GST_LAUNCH_TYPE_AUDIO_ITEM, RygelGstLaunchAudioItemClass))
#define RYGEL_GST_LAUNCH_IS_AUDIO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_GST_LAUNCH_TYPE_AUDIO_ITEM))
#define RYGEL_GST_LAUNCH_IS_AUDIO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_GST_LAUNCH_TYPE_AUDIO_ITEM))
#define RYGEL_GST_LAUNCH_AUDIO_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_GST_LAUNCH_TYPE_AUDIO_ITEM, RygelGstLaunchAudioItemClass))

typedef struct _RygelGstLaunchAudioItem RygelGstLaunchAudioItem;
typedef struct _RygelGstLaunchAudioItemClass RygelGstLaunchAudioItemClass;
typedef struct _RygelGstLaunchAudioItemPrivate RygelGstLaunchAudioItemPrivate;
enum  {
	RYGEL_GST_LAUNCH_AUDIO_ITEM_0_PROPERTY,
	RYGEL_GST_LAUNCH_AUDIO_ITEM_NUM_PROPERTIES
};
static GParamSpec* rygel_gst_launch_audio_item_properties[RYGEL_GST_LAUNCH_AUDIO_ITEM_NUM_PROPERTIES];
#define _g_free0(var) (var = (g_free (var), NULL))

struct _RygelGstLaunchAudioItem {
	RygelAudioItem parent_instance;
	RygelGstLaunchAudioItemPrivate * priv;
};

struct _RygelGstLaunchAudioItemClass {
	RygelAudioItemClass parent_class;
};

static gpointer rygel_gst_launch_audio_item_parent_class = NULL;

GType rygel_gst_launch_audio_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelGstLaunchAudioItem, g_object_unref)
RygelGstLaunchAudioItem* rygel_gst_launch_audio_item_new (const gchar* id,
                                                          RygelMediaContainer* parent,
                                                          const gchar* title,
                                                          const gchar* mime_type,
                                                          const gchar* launch_line);
RygelGstLaunchAudioItem* rygel_gst_launch_audio_item_construct (GType object_type,
                                                                const gchar* id,
                                                                RygelMediaContainer* parent,
                                                                const gchar* title,
                                                                const gchar* mime_type,
                                                                const gchar* launch_line);

RygelGstLaunchAudioItem*
rygel_gst_launch_audio_item_construct (GType object_type,
                                       const gchar* id,
                                       RygelMediaContainer* parent,
                                       const gchar* title,
                                       const gchar* mime_type,
                                       const gchar* launch_line)
{
	RygelGstLaunchAudioItem * self = NULL;
	gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* _tmp2_;
	gchar* _tmp3_;
	g_return_val_if_fail (id != NULL, NULL);
	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (title != NULL, NULL);
	g_return_val_if_fail (mime_type != NULL, NULL);
	g_return_val_if_fail (launch_line != NULL, NULL);
	self = (RygelGstLaunchAudioItem*) rygel_audio_item_construct (object_type, id, parent, title, RYGEL_AUDIO_ITEM_UPNP_CLASS);
	rygel_media_file_item_set_mime_type ((RygelMediaFileItem*) self, mime_type);
	_tmp0_ = soup_uri_encode (launch_line, ".!");
	_tmp1_ = _tmp0_;
	_tmp2_ = g_strconcat ("gst-launch://", _tmp1_, NULL);
	_tmp3_ = _tmp2_;
	rygel_media_object_add_uri ((RygelMediaObject*) self, _tmp3_);
	_g_free0 (_tmp3_);
	_g_free0 (_tmp1_);
	return self;
}

RygelGstLaunchAudioItem*
rygel_gst_launch_audio_item_new (const gchar* id,
                                 RygelMediaContainer* parent,
                                 const gchar* title,
                                 const gchar* mime_type,
                                 const gchar* launch_line)
{
	return rygel_gst_launch_audio_item_construct (RYGEL_GST_LAUNCH_TYPE_AUDIO_ITEM, id, parent, title, mime_type, launch_line);
}

static void
rygel_gst_launch_audio_item_class_init (RygelGstLaunchAudioItemClass * klass,
                                        gpointer klass_data)
{
	rygel_gst_launch_audio_item_parent_class = g_type_class_peek_parent (klass);
}

static void
rygel_gst_launch_audio_item_instance_init (RygelGstLaunchAudioItem * self,
                                           gpointer klass)
{
}

/**
 * Audio item that serves data from a gst-launch commandline.
 */
GType
rygel_gst_launch_audio_item_get_type (void)
{
	static volatile gsize rygel_gst_launch_audio_item_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_gst_launch_audio_item_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelGstLaunchAudioItemClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_gst_launch_audio_item_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelGstLaunchAudioItem), 0, (GInstanceInitFunc) rygel_gst_launch_audio_item_instance_init, NULL };
		GType rygel_gst_launch_audio_item_type_id;
		rygel_gst_launch_audio_item_type_id = g_type_register_static (RYGEL_TYPE_AUDIO_ITEM, "RygelGstLaunchAudioItem", &g_define_type_info, 0);
		g_once_init_leave (&rygel_gst_launch_audio_item_type_id__volatile, rygel_gst_launch_audio_item_type_id);
	}
	return rygel_gst_launch_audio_item_type_id__volatile;
}

