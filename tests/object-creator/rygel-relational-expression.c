/* rygel-relational-expression.c generated by valac 0.44.9, the Vala compiler
 * generated from rygel-relational-expression.vala, do not modify */

/*
 * Copyright (C) 2009 Nokia Corporation.
 * Copyright (C) 2012 Intel Corporation.
 *
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib-object.h>
#include <glib.h>
#include <stdlib.h>
#include <string.h>
#include <libgupnp-av/gupnp-av.h>
#include <gio/gio.h>
#include <gee.h>

#define RYGEL_TYPE_SEARCH_EXPRESSION (rygel_search_expression_get_type ())
#define RYGEL_SEARCH_EXPRESSION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_SEARCH_EXPRESSION, RygelSearchExpression))
#define RYGEL_SEARCH_EXPRESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_SEARCH_EXPRESSION, RygelSearchExpressionClass))
#define RYGEL_IS_SEARCH_EXPRESSION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_SEARCH_EXPRESSION))
#define RYGEL_IS_SEARCH_EXPRESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_SEARCH_EXPRESSION))
#define RYGEL_SEARCH_EXPRESSION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_SEARCH_EXPRESSION, RygelSearchExpressionClass))

typedef struct _RygelSearchExpression RygelSearchExpression;
typedef struct _RygelSearchExpressionClass RygelSearchExpressionClass;
typedef struct _RygelSearchExpressionPrivate RygelSearchExpressionPrivate;

#define RYGEL_TYPE_MEDIA_OBJECT (rygel_media_object_get_type ())
#define RYGEL_MEDIA_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObject))
#define RYGEL_MEDIA_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObjectClass))
#define RYGEL_IS_MEDIA_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_OBJECT))
#define RYGEL_IS_MEDIA_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_OBJECT))
#define RYGEL_MEDIA_OBJECT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_OBJECT, RygelMediaObjectClass))

typedef struct _RygelMediaObject RygelMediaObject;
typedef struct _RygelMediaObjectClass RygelMediaObjectClass;

#define RYGEL_TYPE_RELATIONAL_EXPRESSION (rygel_relational_expression_get_type ())
#define RYGEL_RELATIONAL_EXPRESSION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_RELATIONAL_EXPRESSION, RygelRelationalExpression))
#define RYGEL_RELATIONAL_EXPRESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_RELATIONAL_EXPRESSION, RygelRelationalExpressionClass))
#define RYGEL_IS_RELATIONAL_EXPRESSION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_RELATIONAL_EXPRESSION))
#define RYGEL_IS_RELATIONAL_EXPRESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_RELATIONAL_EXPRESSION))
#define RYGEL_RELATIONAL_EXPRESSION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_RELATIONAL_EXPRESSION, RygelRelationalExpressionClass))

typedef struct _RygelRelationalExpression RygelRelationalExpression;
typedef struct _RygelRelationalExpressionClass RygelRelationalExpressionClass;
typedef struct _RygelRelationalExpressionPrivate RygelRelationalExpressionPrivate;
typedef struct _RygelMediaObjectPrivate RygelMediaObjectPrivate;

#define RYGEL_TYPE_MEDIA_OBJECTS (rygel_media_objects_get_type ())
#define RYGEL_MEDIA_OBJECTS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_OBJECTS, RygelMediaObjects))
#define RYGEL_MEDIA_OBJECTS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_OBJECTS, RygelMediaObjectsClass))
#define RYGEL_IS_MEDIA_OBJECTS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_OBJECTS))
#define RYGEL_IS_MEDIA_OBJECTS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_OBJECTS))
#define RYGEL_MEDIA_OBJECTS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_OBJECTS, RygelMediaObjectsClass))

typedef struct _RygelMediaObjects RygelMediaObjects;
typedef struct _RygelMediaObjectsClass RygelMediaObjectsClass;

#define RYGEL_TYPE_MEDIA_CONTAINER (rygel_media_container_get_type ())
#define RYGEL_MEDIA_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainer))
#define RYGEL_MEDIA_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainerClass))
#define RYGEL_IS_MEDIA_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_CONTAINER))
#define RYGEL_IS_MEDIA_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_CONTAINER))
#define RYGEL_MEDIA_CONTAINER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_CONTAINER, RygelMediaContainerClass))

typedef struct _RygelMediaContainer RygelMediaContainer;
typedef struct _RygelMediaContainerClass RygelMediaContainerClass;

#define RYGEL_TYPE_TRACKABLE_CONTAINER (rygel_trackable_container_get_type ())
#define RYGEL_TRACKABLE_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_TRACKABLE_CONTAINER, RygelTrackableContainer))
#define RYGEL_IS_TRACKABLE_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_TRACKABLE_CONTAINER))
#define RYGEL_TRACKABLE_CONTAINER_GET_INTERFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), RYGEL_TYPE_TRACKABLE_CONTAINER, RygelTrackableContainerIface))

typedef struct _RygelTrackableContainer RygelTrackableContainer;
typedef struct _RygelTrackableContainerIface RygelTrackableContainerIface;

#define RYGEL_TYPE_TRACKABLE_ITEM (rygel_trackable_item_get_type ())
#define RYGEL_TRACKABLE_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_TRACKABLE_ITEM, RygelTrackableItem))
#define RYGEL_IS_TRACKABLE_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_TRACKABLE_ITEM))
#define RYGEL_TRACKABLE_ITEM_GET_INTERFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), RYGEL_TYPE_TRACKABLE_ITEM, RygelTrackableItemIface))

typedef struct _RygelTrackableItem RygelTrackableItem;
typedef struct _RygelTrackableItemIface RygelTrackableItemIface;

#define RYGEL_TYPE_MEDIA_ITEM (rygel_media_item_get_type ())
#define RYGEL_MEDIA_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_ITEM, RygelMediaItem))
#define RYGEL_MEDIA_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_ITEM, RygelMediaItemClass))
#define RYGEL_IS_MEDIA_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_ITEM))
#define RYGEL_IS_MEDIA_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_ITEM))
#define RYGEL_MEDIA_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_ITEM, RygelMediaItemClass))

typedef struct _RygelMediaItem RygelMediaItem;
typedef struct _RygelMediaItemClass RygelMediaItemClass;
typedef struct _RygelMediaContainerPrivate RygelMediaContainerPrivate;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

#define RYGEL_TYPE_WRITABLE_CONTAINER (rygel_writable_container_get_type ())
#define RYGEL_WRITABLE_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_WRITABLE_CONTAINER, RygelWritableContainer))
#define RYGEL_WRITABLE_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_WRITABLE_CONTAINER, RygelWritableContainerClass))
#define RYGEL_IS_WRITABLE_CONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_WRITABLE_CONTAINER))
#define RYGEL_IS_WRITABLE_CONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_WRITABLE_CONTAINER))
#define RYGEL_WRITABLE_CONTAINER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_WRITABLE_CONTAINER, RygelWritableContainerClass))

typedef struct _RygelWritableContainer RygelWritableContainer;
typedef struct _RygelWritableContainerClass RygelWritableContainerClass;

#define RYGEL_TYPE_MEDIA_FILE_ITEM (rygel_media_file_item_get_type ())
#define RYGEL_MEDIA_FILE_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MEDIA_FILE_ITEM, RygelMediaFileItem))
#define RYGEL_MEDIA_FILE_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MEDIA_FILE_ITEM, RygelMediaFileItemClass))
#define RYGEL_IS_MEDIA_FILE_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MEDIA_FILE_ITEM))
#define RYGEL_IS_MEDIA_FILE_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MEDIA_FILE_ITEM))
#define RYGEL_MEDIA_FILE_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MEDIA_FILE_ITEM, RygelMediaFileItemClass))

typedef struct _RygelMediaFileItem RygelMediaFileItem;
typedef struct _RygelMediaFileItemClass RygelMediaFileItemClass;

#define RYGEL_TYPE_PHOTO_ITEM (rygel_photo_item_get_type ())
#define RYGEL_PHOTO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_PHOTO_ITEM, RygelPhotoItem))
#define RYGEL_PHOTO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_PHOTO_ITEM, RygelPhotoItemClass))
#define RYGEL_IS_PHOTO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_PHOTO_ITEM))
#define RYGEL_IS_PHOTO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_PHOTO_ITEM))
#define RYGEL_PHOTO_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_PHOTO_ITEM, RygelPhotoItemClass))

typedef struct _RygelPhotoItem RygelPhotoItem;
typedef struct _RygelPhotoItemClass RygelPhotoItemClass;
typedef struct _RygelMediaItemPrivate RygelMediaItemPrivate;
typedef struct _RygelMediaFileItemPrivate RygelMediaFileItemPrivate;
typedef struct _RygelPhotoItemPrivate RygelPhotoItemPrivate;

#define RYGEL_TYPE_AUDIO_ITEM (rygel_audio_item_get_type ())
#define RYGEL_AUDIO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_AUDIO_ITEM, RygelAudioItem))
#define RYGEL_AUDIO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_AUDIO_ITEM, RygelAudioItemClass))
#define RYGEL_IS_AUDIO_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_AUDIO_ITEM))
#define RYGEL_IS_AUDIO_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_AUDIO_ITEM))
#define RYGEL_AUDIO_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_AUDIO_ITEM, RygelAudioItemClass))

typedef struct _RygelAudioItem RygelAudioItem;
typedef struct _RygelAudioItemClass RygelAudioItemClass;

#define RYGEL_TYPE_MUSIC_ITEM (rygel_music_item_get_type ())
#define RYGEL_MUSIC_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MUSIC_ITEM, RygelMusicItem))
#define RYGEL_MUSIC_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MUSIC_ITEM, RygelMusicItemClass))
#define RYGEL_IS_MUSIC_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MUSIC_ITEM))
#define RYGEL_IS_MUSIC_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MUSIC_ITEM))
#define RYGEL_MUSIC_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MUSIC_ITEM, RygelMusicItemClass))

typedef struct _RygelMusicItem RygelMusicItem;
typedef struct _RygelMusicItemClass RygelMusicItemClass;
typedef struct _RygelAudioItemPrivate RygelAudioItemPrivate;
#define _g_free0(var) (var = (g_free (var), NULL))

struct _RygelSearchExpression {
	GTypeInstance parent_instance;
	volatile int ref_count;
	RygelSearchExpressionPrivate * priv;
	gpointer op;
	gpointer operand1;
	gpointer operand2;
};

struct _RygelSearchExpressionClass {
	GTypeClass parent_class;
	void (*finalize) (RygelSearchExpression *self);
	gboolean (*satisfied_by) (RygelSearchExpression* self, RygelMediaObject* media_object);
	gchar* (*to_string) (RygelSearchExpression* self);
};

struct _RygelRelationalExpression {
	RygelSearchExpression parent_instance;
	RygelRelationalExpressionPrivate * priv;
};

struct _RygelRelationalExpressionClass {
	RygelSearchExpressionClass parent_class;
};

struct _RygelMediaObject {
	GObject parent_instance;
	RygelMediaObjectPrivate * priv;
	gchar* ref_id;
	gchar* upnp_class;
	GUPnPOCMFlags ocm_flags;
	GeeArrayList* uris;
	guint object_update_id;
};

struct _RygelMediaObjectClass {
	GObjectClass parent_class;
	void (*get_children) (RygelMediaObject* self, guint offset, guint max_count, const gchar* sort_criteria, GCancellable* cancellable, GAsyncReadyCallback _callback_, gpointer _user_data_);
	RygelMediaObjects* (*get_children_finish) (RygelMediaObject* self, GAsyncResult* _res_, GError** error);
	void (*find_object) (RygelMediaObject* self, const gchar* id, GCancellable* cancellable, GAsyncReadyCallback _callback_, gpointer _user_data_);
	RygelMediaObject* (*find_object_finish) (RygelMediaObject* self, GAsyncResult* _res_, GError** error);
};

struct _RygelTrackableContainerIface {
	GTypeInterface parent_iface;
};

struct _RygelTrackableItemIface {
	GTypeInterface parent_iface;
};

struct _RygelMediaContainer {
	RygelMediaObject parent_instance;
	RygelMediaContainerPrivate * priv;
	GeeArrayList* create_classes;
	gchar* sort_criteria;
	guint update_id;
	RygelMediaObject* found_object;
};

struct _RygelMediaContainerClass {
	RygelMediaObjectClass parent_class;
};

struct _RygelMediaItem {
	RygelMediaObject parent_instance;
	RygelMediaItemPrivate * priv;
};

struct _RygelMediaItemClass {
	RygelMediaObjectClass parent_class;
};

struct _RygelMediaFileItem {
	RygelMediaItem parent_instance;
	RygelMediaFileItemPrivate * priv;
	gchar* dlna_profile;
	gchar* mime_type;
	glong size;
	gboolean place_holder;
	gchar* date;
};

struct _RygelMediaFileItemClass {
	RygelMediaItemClass parent_class;
};

struct _RygelPhotoItem {
	RygelMediaFileItem parent_instance;
	RygelPhotoItemPrivate * priv;
	gchar* creator;
};

struct _RygelPhotoItemClass {
	RygelMediaFileItemClass parent_class;
};

struct _RygelAudioItem {
	RygelMediaFileItem parent_instance;
	RygelAudioItemPrivate * priv;
	gchar* artist;
	gchar* album;
};

struct _RygelAudioItemClass {
	RygelMediaFileItemClass parent_class;
};

static gpointer rygel_relational_expression_parent_class = NULL;

gpointer rygel_search_expression_ref (gpointer instance);
void rygel_search_expression_unref (gpointer instance);
GParamSpec* rygel_param_spec_search_expression (const gchar* name,
                                                const gchar* nick,
                                                const gchar* blurb,
                                                GType object_type,
                                                GParamFlags flags);
void rygel_value_set_search_expression (GValue* value,
                                        gpointer v_object);
void rygel_value_take_search_expression (GValue* value,
                                         gpointer v_object);
gpointer rygel_value_get_search_expression (const GValue* value);
GType rygel_search_expression_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelSearchExpression, rygel_search_expression_unref)
GType rygel_media_object_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaObject, g_object_unref)
GType rygel_relational_expression_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelRelationalExpression, rygel_search_expression_unref)
#define RYGEL_RELATIONAL_EXPRESSION_CAPS "@id,@parentID,@refID,upnp:class," "dc:title,upnp:artist,upnp:album," "dc:creator,upnp:createClass,@childCount"
static gboolean rygel_relational_expression_real_satisfied_by (RygelSearchExpression* base,
                                                        RygelMediaObject* media_object);
gboolean rygel_relational_expression_compare_string (RygelRelationalExpression* self,
                                                     const gchar* str);
const gchar* rygel_media_object_get_id (RygelMediaObject* self);
GType rygel_media_objects_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaObjects, g_object_unref)
GType rygel_media_container_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaContainer, g_object_unref)
RygelMediaContainer* rygel_media_object_get_parent (RygelMediaObject* self);
const gchar* rygel_media_object_get_title (RygelMediaObject* self);
GType rygel_trackable_container_get_type (void) G_GNUC_CONST;
GType rygel_media_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaItem, g_object_unref)
GType rygel_trackable_item_get_type (void) G_GNUC_CONST;
gboolean rygel_relational_expression_compare_uint (RygelRelationalExpression* self,
                                                   guint integer);
GType rygel_writable_container_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelWritableContainer, g_object_unref)
static gboolean rygel_relational_expression_compare_create_class (RygelRelationalExpression* self,
                                                           RygelWritableContainer* container);
GType rygel_media_file_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMediaFileItem, g_object_unref)
GType rygel_photo_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelPhotoItem, g_object_unref)
GType rygel_audio_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelAudioItem, g_object_unref)
GType rygel_music_item_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RygelMusicItem, g_object_unref)
gboolean rygel_relational_expression_compare_int (RygelRelationalExpression* self,
                                                  gint integer);
gint rygel_media_container_get_child_count (RygelMediaContainer* self);
static gchar* rygel_relational_expression_real_to_string (RygelSearchExpression* base);
RygelRelationalExpression* rygel_relational_expression_new (void);
RygelRelationalExpression* rygel_relational_expression_construct (GType object_type);
RygelSearchExpression* rygel_search_expression_construct (GType object_type,
                                                          GType g_type,
                                                          GBoxedCopyFunc g_dup_func,
                                                          GDestroyNotify g_destroy_func,
                                                          GType h_type,
                                                          GBoxedCopyFunc h_dup_func,
                                                          GDestroyNotify h_destroy_func,
                                                          GType i_type,
                                                          GBoxedCopyFunc i_dup_func,
                                                          GDestroyNotify i_destroy_func);

static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

static gboolean
rygel_relational_expression_real_satisfied_by (RygelSearchExpression* base,
                                               RygelMediaObject* media_object)
{
	RygelRelationalExpression * self;
	gconstpointer _tmp0_;
	const gchar* _tmp1_;
	GQuark _tmp3_ = 0U;
	static GQuark _tmp2_label0 = 0;
	static GQuark _tmp2_label1 = 0;
	static GQuark _tmp2_label2 = 0;
	static GQuark _tmp2_label3 = 0;
	static GQuark _tmp2_label4 = 0;
	static GQuark _tmp2_label5 = 0;
	static GQuark _tmp2_label6 = 0;
	static GQuark _tmp2_label7 = 0;
	static GQuark _tmp2_label8 = 0;
	static GQuark _tmp2_label9 = 0;
	static GQuark _tmp2_label10 = 0;
	static GQuark _tmp2_label11 = 0;
	gboolean result = FALSE;
	self = (RygelRelationalExpression*) base;
	g_return_val_if_fail (media_object != NULL, FALSE);
	_tmp0_ = ((RygelSearchExpression*) self)->operand1;
	_tmp1_ = (const gchar*) _tmp0_;
	_tmp3_ = (NULL == _tmp1_) ? 0 : g_quark_from_string (_tmp1_);
	if (_tmp3_ == ((0 != _tmp2_label0) ? _tmp2_label0 : (_tmp2_label0 = g_quark_from_static_string ("@id")))) {
		switch (0) {
			default:
			{
				const gchar* _tmp4_;
				const gchar* _tmp5_;
				_tmp4_ = rygel_media_object_get_id (media_object);
				_tmp5_ = _tmp4_;
				result = rygel_relational_expression_compare_string (self, _tmp5_);
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label1) ? _tmp2_label1 : (_tmp2_label1 = g_quark_from_static_string ("@refID")))) {
		switch (0) {
			default:
			{
				const gchar* _tmp6_;
				_tmp6_ = media_object->ref_id;
				result = rygel_relational_expression_compare_string (self, _tmp6_);
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label2) ? _tmp2_label2 : (_tmp2_label2 = g_quark_from_static_string ("@parentID")))) {
		switch (0) {
			default:
			{
				RygelMediaContainer* _tmp7_;
				RygelMediaContainer* _tmp8_;
				const gchar* _tmp9_;
				const gchar* _tmp10_;
				_tmp7_ = rygel_media_object_get_parent (media_object);
				_tmp8_ = _tmp7_;
				_tmp9_ = rygel_media_object_get_id ((RygelMediaObject*) _tmp8_);
				_tmp10_ = _tmp9_;
				result = rygel_relational_expression_compare_string (self, _tmp10_);
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label3) ? _tmp2_label3 : (_tmp2_label3 = g_quark_from_static_string ("upnp:class")))) {
		switch (0) {
			default:
			{
				const gchar* _tmp11_;
				_tmp11_ = media_object->upnp_class;
				result = rygel_relational_expression_compare_string (self, _tmp11_);
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label4) ? _tmp2_label4 : (_tmp2_label4 = g_quark_from_static_string ("dc:title")))) {
		switch (0) {
			default:
			{
				const gchar* _tmp12_;
				const gchar* _tmp13_;
				_tmp12_ = rygel_media_object_get_title (media_object);
				_tmp13_ = _tmp12_;
				result = rygel_relational_expression_compare_string (self, _tmp13_);
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label5) ? _tmp2_label5 : (_tmp2_label5 = g_quark_from_static_string ("upnp:objectUpdateID")))) {
		switch (0) {
			default:
			{
				gconstpointer _tmp14_;
				_tmp14_ = ((RygelSearchExpression*) self)->op;
				if (((GUPnPSearchCriteriaOp) ((gintptr) _tmp14_)) == GUPNP_SEARCH_CRITERIA_OP_EXISTS) {
					gconstpointer _tmp15_;
					_tmp15_ = ((RygelSearchExpression*) self)->operand2;
					if (g_strcmp0 ((const gchar*) _tmp15_, "true") == 0) {
						gboolean _tmp16_ = FALSE;
						if (RYGEL_IS_TRACKABLE_CONTAINER (media_object)) {
							_tmp16_ = TRUE;
						} else {
							_tmp16_ = RYGEL_IS_TRACKABLE_ITEM (media_object);
						}
						result = _tmp16_;
						return result;
					} else {
						gboolean _tmp17_ = FALSE;
						if (RYGEL_IS_TRACKABLE_CONTAINER (media_object)) {
							_tmp17_ = TRUE;
						} else {
							_tmp17_ = RYGEL_IS_TRACKABLE_ITEM (media_object);
						}
						result = !_tmp17_;
						return result;
					}
				} else {
					result = rygel_relational_expression_compare_uint (self, media_object->object_update_id);
					return result;
				}
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label6) ? _tmp2_label6 : (_tmp2_label6 = g_quark_from_static_string ("upnp:containerUpdateID")))) {
		switch (0) {
			default:
			{
				gconstpointer _tmp18_;
				if (!RYGEL_IS_MEDIA_CONTAINER (media_object)) {
					result = FALSE;
					return result;
				}
				_tmp18_ = ((RygelSearchExpression*) self)->op;
				if (((GUPnPSearchCriteriaOp) ((gintptr) _tmp18_)) == GUPNP_SEARCH_CRITERIA_OP_EXISTS) {
					gconstpointer _tmp19_;
					_tmp19_ = ((RygelSearchExpression*) self)->operand2;
					if (g_strcmp0 ((const gchar*) _tmp19_, "true") == 0) {
						result = RYGEL_IS_TRACKABLE_CONTAINER (media_object);
						return result;
					} else {
						result = !RYGEL_IS_TRACKABLE_CONTAINER (media_object);
						return result;
					}
				} else {
					RygelMediaContainer* container = NULL;
					RygelMediaContainer* _tmp20_;
					RygelMediaContainer* _tmp21_;
					_tmp20_ = _g_object_ref0 (RYGEL_IS_MEDIA_CONTAINER (media_object) ? ((RygelMediaContainer*) media_object) : NULL);
					container = _tmp20_;
					_tmp21_ = container;
					result = rygel_relational_expression_compare_uint (self, _tmp21_->update_id);
					_g_object_unref0 (container);
					return result;
				}
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label7) ? _tmp2_label7 : (_tmp2_label7 = g_quark_from_static_string ("upnp:createClass")))) {
		switch (0) {
			default:
			{
				if (!RYGEL_IS_WRITABLE_CONTAINER (media_object)) {
					result = FALSE;
					return result;
				}
				result = rygel_relational_expression_compare_create_class (self, RYGEL_IS_WRITABLE_CONTAINER (media_object) ? ((RygelWritableContainer*) media_object) : NULL);
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label8) ? _tmp2_label8 : (_tmp2_label8 = g_quark_from_static_string ("dc:creator")))) {
		switch (0) {
			default:
			{
				const gchar* _tmp22_;
				if (!RYGEL_IS_PHOTO_ITEM (media_object)) {
					result = FALSE;
					return result;
				}
				_tmp22_ = (RYGEL_IS_PHOTO_ITEM (media_object) ? ((RygelPhotoItem*) media_object) : NULL)->creator;
				result = rygel_relational_expression_compare_string (self, _tmp22_);
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label9) ? _tmp2_label9 : (_tmp2_label9 = g_quark_from_static_string ("upnp:artist")))) {
		switch (0) {
			default:
			{
				const gchar* _tmp23_;
				if (!RYGEL_IS_MUSIC_ITEM (media_object)) {
					result = FALSE;
					return result;
				}
				_tmp23_ = ((RygelAudioItem*) (RYGEL_IS_MUSIC_ITEM (media_object) ? ((RygelMusicItem*) media_object) : NULL))->artist;
				result = rygel_relational_expression_compare_string (self, _tmp23_);
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label10) ? _tmp2_label10 : (_tmp2_label10 = g_quark_from_static_string ("upnp:album")))) {
		switch (0) {
			default:
			{
				const gchar* _tmp24_;
				if (!RYGEL_IS_MUSIC_ITEM (media_object)) {
					result = FALSE;
					return result;
				}
				_tmp24_ = ((RygelAudioItem*) (RYGEL_IS_MUSIC_ITEM (media_object) ? ((RygelMusicItem*) media_object) : NULL))->album;
				result = rygel_relational_expression_compare_string (self, _tmp24_);
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label11) ? _tmp2_label11 : (_tmp2_label11 = g_quark_from_static_string ("@childCount")))) {
		switch (0) {
			default:
			{
				RygelMediaContainer* container = NULL;
				RygelMediaContainer* _tmp25_;
				RygelMediaContainer* _tmp26_;
				gint _tmp27_;
				gint _tmp28_;
				if (!RYGEL_IS_MEDIA_CONTAINER (media_object)) {
					result = FALSE;
					return result;
				}
				_tmp25_ = _g_object_ref0 (RYGEL_IS_MEDIA_CONTAINER (media_object) ? ((RygelMediaContainer*) media_object) : NULL);
				container = _tmp25_;
				_tmp26_ = container;
				_tmp27_ = rygel_media_container_get_child_count (_tmp26_);
				_tmp28_ = _tmp27_;
				result = rygel_relational_expression_compare_int (self, _tmp28_);
				_g_object_unref0 (container);
				return result;
			}
		}
	} else {
		switch (0) {
			default:
			{
				result = FALSE;
				return result;
			}
		}
	}
}

static gchar*
rygel_relational_expression_real_to_string (RygelSearchExpression* base)
{
	RygelRelationalExpression * self;
	gconstpointer _tmp0_;
	gconstpointer _tmp1_;
	gconstpointer _tmp2_;
	gchar* _tmp3_;
	gchar* result = NULL;
	self = (RygelRelationalExpression*) base;
	_tmp0_ = ((RygelSearchExpression*) self)->operand1;
	_tmp1_ = ((RygelSearchExpression*) self)->op;
	_tmp2_ = ((RygelSearchExpression*) self)->operand2;
	_tmp3_ = g_strdup_printf ("%s %d %s", (const gchar*) _tmp0_, (gint) ((GUPnPSearchCriteriaOp) ((gintptr) _tmp1_)), (const gchar*) _tmp2_);
	result = _tmp3_;
	return result;
}

static gboolean
rygel_relational_expression_compare_create_class (RygelRelationalExpression* self,
                                                  RygelWritableContainer* container)
{
	gboolean ret = FALSE;
	gboolean result = FALSE;
	g_return_val_if_fail (self != NULL, FALSE);
	g_return_val_if_fail (container != NULL, FALSE);
	ret = FALSE;
	{
		GeeArrayList* _create_class_list = NULL;
		GeeArrayList* _tmp0_;
		GeeArrayList* _tmp1_;
		gint _create_class_size = 0;
		GeeArrayList* _tmp2_;
		gint _tmp3_;
		gint _tmp4_;
		gint _create_class_index = 0;
		_tmp0_ = ((RygelMediaContainer*) container)->create_classes;
		_tmp1_ = _g_object_ref0 (_tmp0_);
		_create_class_list = _tmp1_;
		_tmp2_ = _create_class_list;
		_tmp3_ = gee_abstract_collection_get_size ((GeeAbstractCollection*) _tmp2_);
		_tmp4_ = _tmp3_;
		_create_class_size = _tmp4_;
		_create_class_index = -1;
		while (TRUE) {
			gchar* create_class = NULL;
			GeeArrayList* _tmp5_;
			gpointer _tmp6_;
			const gchar* _tmp7_;
			_create_class_index = _create_class_index + 1;
			if (!(_create_class_index < _create_class_size)) {
				break;
			}
			_tmp5_ = _create_class_list;
			_tmp6_ = gee_abstract_list_get ((GeeAbstractList*) _tmp5_, _create_class_index);
			create_class = (gchar*) _tmp6_;
			_tmp7_ = create_class;
			if (rygel_relational_expression_compare_string (self, _tmp7_)) {
				ret = TRUE;
				_g_free0 (create_class);
				break;
			}
			_g_free0 (create_class);
		}
		_g_object_unref0 (_create_class_list);
	}
	result = ret;
	return result;
}

static gboolean
string_contains (const gchar* self,
                 const gchar* needle)
{
	gchar* _tmp0_;
	gboolean result = FALSE;
	g_return_val_if_fail (self != NULL, FALSE);
	g_return_val_if_fail (needle != NULL, FALSE);
	_tmp0_ = strstr ((gchar*) self, (gchar*) needle);
	result = _tmp0_ != NULL;
	return result;
}

gboolean
rygel_relational_expression_compare_string (RygelRelationalExpression* self,
                                            const gchar* str)
{
	gchar* up_operand2 = NULL;
	gconstpointer _tmp0_;
	gchar* _tmp1_;
	gchar* up_str = NULL;
	gconstpointer _tmp3_;
	gboolean result = FALSE;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp0_ = ((RygelSearchExpression*) self)->operand2;
	_tmp1_ = g_utf8_strup ((const gchar*) _tmp0_, (gssize) -1);
	up_operand2 = _tmp1_;
	if (str != NULL) {
		gchar* _tmp2_;
		_tmp2_ = g_utf8_strup (str, (gssize) -1);
		_g_free0 (up_str);
		up_str = _tmp2_;
	} else {
		_g_free0 (up_str);
		up_str = NULL;
	}
	_tmp3_ = ((RygelSearchExpression*) self)->op;
	switch ((GUPnPSearchCriteriaOp) ((gintptr) _tmp3_)) {
		case GUPNP_SEARCH_CRITERIA_OP_EXISTS:
		{
			gconstpointer _tmp4_;
			_tmp4_ = ((RygelSearchExpression*) self)->operand2;
			if (g_strcmp0 ((const gchar*) _tmp4_, "true") == 0) {
				const gchar* _tmp5_;
				_tmp5_ = up_str;
				result = _tmp5_ != NULL;
				_g_free0 (up_str);
				_g_free0 (up_operand2);
				return result;
			} else {
				const gchar* _tmp6_;
				_tmp6_ = up_str;
				result = _tmp6_ == NULL;
				_g_free0 (up_str);
				_g_free0 (up_operand2);
				return result;
			}
		}
		case GUPNP_SEARCH_CRITERIA_OP_EQ:
		{
			const gchar* _tmp7_;
			const gchar* _tmp8_;
			_tmp7_ = up_operand2;
			_tmp8_ = up_str;
			result = g_strcmp0 (_tmp7_, _tmp8_) == 0;
			_g_free0 (up_str);
			_g_free0 (up_operand2);
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_NEQ:
		{
			const gchar* _tmp9_;
			const gchar* _tmp10_;
			_tmp9_ = up_operand2;
			_tmp10_ = up_str;
			result = g_strcmp0 (_tmp9_, _tmp10_) != 0;
			_g_free0 (up_str);
			_g_free0 (up_operand2);
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_CONTAINS:
		{
			const gchar* _tmp11_;
			const gchar* _tmp12_;
			_tmp11_ = up_str;
			_tmp12_ = up_operand2;
			result = string_contains (_tmp11_, _tmp12_);
			_g_free0 (up_str);
			_g_free0 (up_operand2);
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_DERIVED_FROM:
		{
			const gchar* _tmp13_;
			const gchar* _tmp14_;
			_tmp13_ = up_str;
			_tmp14_ = up_operand2;
			result = g_str_has_prefix (_tmp13_, _tmp14_);
			_g_free0 (up_str);
			_g_free0 (up_operand2);
			return result;
		}
		default:
		{
			result = FALSE;
			_g_free0 (up_str);
			_g_free0 (up_operand2);
			return result;
		}
	}
	_g_free0 (up_str);
	_g_free0 (up_operand2);
}

gboolean
rygel_relational_expression_compare_int (RygelRelationalExpression* self,
                                         gint integer)
{
	gint operand2 = 0;
	gconstpointer _tmp0_;
	gconstpointer _tmp1_;
	gboolean result = FALSE;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp0_ = ((RygelSearchExpression*) self)->operand2;
	operand2 = atoi ((const gchar*) _tmp0_);
	_tmp1_ = ((RygelSearchExpression*) self)->op;
	switch ((GUPnPSearchCriteriaOp) ((gintptr) _tmp1_)) {
		case GUPNP_SEARCH_CRITERIA_OP_EQ:
		{
			result = integer == operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_NEQ:
		{
			result = integer != operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_LESS:
		{
			result = integer < operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_LEQ:
		{
			result = integer <= operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_GREATER:
		{
			result = integer > operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_GEQ:
		{
			result = integer >= operand2;
			return result;
		}
		default:
		{
			result = FALSE;
			return result;
		}
	}
}

static guint64
uint64_parse (const gchar* str)
{
	guint64 result = 0ULL;
	g_return_val_if_fail (str != NULL, 0ULL);
	result = g_ascii_strtoull (str, NULL, (guint) 0);
	return result;
}

gboolean
rygel_relational_expression_compare_uint (RygelRelationalExpression* self,
                                          guint integer)
{
	guint64 operand2 = 0ULL;
	gconstpointer _tmp0_;
	gconstpointer _tmp1_;
	gboolean result = FALSE;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp0_ = ((RygelSearchExpression*) self)->operand2;
	operand2 = uint64_parse ((const gchar*) _tmp0_);
	_tmp1_ = ((RygelSearchExpression*) self)->op;
	switch ((GUPnPSearchCriteriaOp) ((gintptr) _tmp1_)) {
		case GUPNP_SEARCH_CRITERIA_OP_EQ:
		{
			result = ((guint64) integer) == operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_NEQ:
		{
			result = ((guint64) integer) != operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_LESS:
		{
			result = ((guint64) integer) < operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_LEQ:
		{
			result = ((guint64) integer) <= operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_GREATER:
		{
			result = ((guint64) integer) > operand2;
			return result;
		}
		case GUPNP_SEARCH_CRITERIA_OP_GEQ:
		{
			result = ((guint64) integer) >= operand2;
			return result;
		}
		default:
		{
			result = FALSE;
			return result;
		}
	}
}

RygelRelationalExpression*
rygel_relational_expression_construct (GType object_type)
{
	RygelRelationalExpression* self = NULL;
	self = (RygelRelationalExpression*) rygel_search_expression_construct (object_type, gupnp_search_criteria_op_get_type (), NULL, NULL, G_TYPE_STRING, (GBoxedCopyFunc) g_strdup, (GDestroyNotify) g_free, G_TYPE_STRING, (GBoxedCopyFunc) g_strdup, (GDestroyNotify) g_free);
	return self;
}

RygelRelationalExpression*
rygel_relational_expression_new (void)
{
	return rygel_relational_expression_construct (RYGEL_TYPE_RELATIONAL_EXPRESSION);
}

static void
rygel_relational_expression_class_init (RygelRelationalExpressionClass * klass,
                                        gpointer klass_data)
{
	rygel_relational_expression_parent_class = g_type_class_peek_parent (klass);
	((RygelSearchExpressionClass *) klass)->satisfied_by = (gboolean (*) (RygelSearchExpression*, RygelMediaObject*)) rygel_relational_expression_real_satisfied_by;
	((RygelSearchExpressionClass *) klass)->to_string = (gchar* (*) (RygelSearchExpression*)) rygel_relational_expression_real_to_string;
}

static void
rygel_relational_expression_instance_init (RygelRelationalExpression * self,
                                           gpointer klass)
{
}

/**
 * This is a parsed UPnP search expression consisting of two strings joined by a
 * relational operator such as such <, <=, ==, !=, >, >=, derivedFrom or exists.
 */
GType
rygel_relational_expression_get_type (void)
{
	static volatile gsize rygel_relational_expression_type_id__volatile = 0;
	if (g_once_init_enter (&rygel_relational_expression_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (RygelRelationalExpressionClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) rygel_relational_expression_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (RygelRelationalExpression), 0, (GInstanceInitFunc) rygel_relational_expression_instance_init, NULL };
		GType rygel_relational_expression_type_id;
		rygel_relational_expression_type_id = g_type_register_static (RYGEL_TYPE_SEARCH_EXPRESSION, "RygelRelationalExpression", &g_define_type_info, 0);
		g_once_init_leave (&rygel_relational_expression_type_id__volatile, rygel_relational_expression_type_id);
	}
	return rygel_relational_expression_type_id__volatile;
}

