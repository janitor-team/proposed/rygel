<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>RygelMediaRenderer: librygel-renderer Reference Manual</title>
<meta name="generator" content="DocBook XSL Stylesheets V1.79.1">
<link rel="home" href="index.html" title="librygel-renderer Reference Manual">
<link rel="up" href="api-main-list.html" title="librygel-renderer API Reference">
<link rel="prev" href="RygelMediaRendererPlugin.html" title="RygelMediaRendererPlugin">
<link rel="next" href="RygelMediaPlayer.html" title="RygelMediaPlayer">
<meta name="generator" content="GTK-Doc V1.28 (XML mode)">
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<table class="navigation" id="top" width="100%" summary="Navigation header" cellpadding="2" cellspacing="5"><tr valign="middle">
<td width="100%" align="left" class="shortcuts">
<a href="#" class="shortcut">Top</a><span id="nav_description">  <span class="dim">|</span> 
                  <a href="#RygelMediaRenderer.description" class="shortcut">Description</a></span><span id="nav_hierarchy">  <span class="dim">|</span> 
                  <a href="#RygelMediaRenderer.object-hierarchy" class="shortcut">Object Hierarchy</a></span><span id="nav_properties">  <span class="dim">|</span> 
                  <a href="#RygelMediaRenderer.properties" class="shortcut">Properties</a></span>
</td>
<td><a accesskey="h" href="index.html"><img src="home.png" width="16" height="16" border="0" alt="Home"></a></td>
<td><a accesskey="u" href="api-main-list.html"><img src="up.png" width="16" height="16" border="0" alt="Up"></a></td>
<td><a accesskey="p" href="RygelMediaRendererPlugin.html"><img src="left.png" width="16" height="16" border="0" alt="Prev"></a></td>
<td><a accesskey="n" href="RygelMediaPlayer.html"><img src="right.png" width="16" height="16" border="0" alt="Next"></a></td>
</tr></table>
<div class="refentry">
<a name="RygelMediaRenderer"></a><div class="titlepage"></div>
<div class="refnamediv"><table width="100%"><tr>
<td valign="top">
<h2><span class="refentrytitle"><a name="RygelMediaRenderer.top_of_page"></a>RygelMediaRenderer</span></h2>
<p>RygelMediaRenderer — This class may be used to implement in-process UPnP-AV media renderers.</p>
</td>
<td class="gallery_image" valign="top" align="right"></td>
</tr></table></div>
<div class="refsect1">
<a name="RygelMediaRenderer.functions"></a><h2>Functions</h2>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="functions_return">
<col class="functions_name">
</colgroup>
<tbody><tr>
<td class="function_type">
<a class="link" href="RygelMediaRenderer.html" title="RygelMediaRenderer"><span class="returnvalue">RygelMediaRenderer</span></a> *
</td>
<td class="function_name">
<a class="link" href="RygelMediaRenderer.html#rygel-media-renderer-new" title="rygel_media_renderer_new ()">rygel_media_renderer_new</a> <span class="c_punctuation">()</span>
</td>
</tr></tbody>
</table></div>
</div>
<div class="refsect1">
<a name="RygelMediaRenderer.properties"></a><h2>Properties</h2>
<div class="informaltable"><table class="informaltable" border="0">
<colgroup>
<col width="150px" class="properties_type">
<col width="300px" class="properties_name">
<col width="200px" class="properties_flags">
</colgroup>
<tbody><tr>
<td class="property_type">
<a class="link" href="RygelMediaPlayer.html" title="RygelMediaPlayer"><span class="type">RygelMediaPlayer</span></a> *</td>
<td class="property_name"><a class="link" href="RygelMediaRenderer.html#RygelMediaRenderer--player" title="The “player” property">player</a></td>
<td class="property_flags">Write / Construct Only</td>
</tr></tbody>
</table></div>
</div>
<div class="refsect1">
<a name="RygelMediaRenderer.other"></a><h2>Types and Values</h2>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="name">
<col class="description">
</colgroup>
<tbody>
<tr>
<td class="define_keyword">#define</td>
<td class="function_name"><a class="link" href="RygelMediaRenderer.html#RYGEL-TYPE-MEDIA-RENDERER:CAPS" title="RYGEL_TYPE_MEDIA_RENDERER">RYGEL_TYPE_MEDIA_RENDERER</a></td>
</tr>
<tr>
<td class="datatype_keyword">struct</td>
<td class="function_name"><a class="link" href="RygelMediaRenderer.html#RygelMediaRenderer-struct" title="struct RygelMediaRenderer">RygelMediaRenderer</a></td>
</tr>
<tr>
<td class="datatype_keyword">struct</td>
<td class="function_name"><a class="link" href="RygelMediaRenderer.html#RygelMediaRendererClass" title="struct RygelMediaRendererClass">RygelMediaRendererClass</a></td>
</tr>
</tbody>
</table></div>
</div>
<div class="refsect1">
<a name="RygelMediaRenderer.object-hierarchy"></a><h2>Object Hierarchy</h2>
<pre class="screen">    <a href="../gobject/gobject-The-Base-Object-Type.html#GObject-struct">GObject</a>
    <span class="lineart">╰──</span> RygelMediaDevice
        <span class="lineart">╰──</span> RygelMediaRenderer
</pre>
</div>
<div class="refsect1">
<a name="RygelMediaRenderer.description"></a><h2>Description</h2>
<p>Call <code class="function">rygel_media_device_add_interface()</code> on the RygelMediaRenderer to allow it to render media from that network interface.</p>
<p>See the &lt;link linkend="implementing-renderers"&gt;Implementing Renderers&lt;/link&gt; section.</p>
</div>
<div class="refsect1">
<a name="RygelMediaRenderer.functions_details"></a><h2>Functions</h2>
<div class="refsect2">
<a name="rygel-media-renderer-new"></a><h3>rygel_media_renderer_new ()</h3>
<pre class="programlisting"><a class="link" href="RygelMediaRenderer.html" title="RygelMediaRenderer"><span class="returnvalue">RygelMediaRenderer</span></a> *
rygel_media_renderer_new (<em class="parameter"><code>const <a href="../glib/glib-Basic-Types.html#gchar"><span class="type">gchar</span></a> *title</code></em>,
                          <em class="parameter"><code><a class="link" href="RygelMediaPlayer.html" title="RygelMediaPlayer"><span class="type">RygelMediaPlayer</span></a> *player</code></em>,
                          <em class="parameter"><code><span class="type">RygelPluginCapabilities</span> capabilities</code></em>);</pre>
<p>Create a RygelMediaRenderer to render content via a RygelMediaPlayer.</p>
<div class="refsect3">
<a name="rygel-media-renderer-new.parameters"></a><h4>Parameters</h4>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="parameters_name">
<col class="parameters_description">
<col width="200px" class="parameters_annotations">
</colgroup>
<tbody>
<tr>
<td class="parameter_name"><p>title</p></td>
<td class="parameter_description"><p> </p></td>
<td class="parameter_annotations"> </td>
</tr>
<tr>
<td class="parameter_name"><p>player</p></td>
<td class="parameter_description"><p> </p></td>
<td class="parameter_annotations"> </td>
</tr>
<tr>
<td class="parameter_name"><p>capabilities</p></td>
<td class="parameter_description"><p> </p></td>
<td class="parameter_annotations"> </td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
<div class="refsect1">
<a name="RygelMediaRenderer.other_details"></a><h2>Types and Values</h2>
<div class="refsect2">
<a name="RYGEL-TYPE-MEDIA-RENDERER:CAPS"></a><h3>RYGEL_TYPE_MEDIA_RENDERER</h3>
<pre class="programlisting">#define RYGEL_TYPE_MEDIA_RENDERER (rygel_media_renderer_get_type ())
</pre>
<p>The type for <a class="link" href="RygelMediaRenderer.html" title="RygelMediaRenderer"><span class="type">RygelMediaRenderer</span></a>.</p>
</div>
<hr>
<div class="refsect2">
<a name="RygelMediaRenderer-struct"></a><h3>struct RygelMediaRenderer</h3>
<pre class="programlisting">struct RygelMediaRenderer;</pre>
<p>This class may be used to implement in-process UPnP-AV media renderers.</p>
<p>Call <code class="function">rygel_media_device_add_interface()</code> on the RygelMediaRenderer to allow it to render media from that network interface.</p>
<p>See the &lt;link linkend="implementing-renderers"&gt;Implementing Renderers&lt;/link&gt; section.</p>
</div>
<hr>
<div class="refsect2">
<a name="RygelMediaRendererClass"></a><h3>struct RygelMediaRendererClass</h3>
<pre class="programlisting">struct RygelMediaRendererClass {
	RygelMediaDeviceClass parent_class;
};
</pre>
<p>The class structure for <a class="link" href="RygelMediaRenderer.html#RYGEL-TYPE-MEDIA-RENDERER:CAPS" title="RYGEL_TYPE_MEDIA_RENDERER"><code class="literal">RYGEL_TYPE_MEDIA_RENDERER</code></a>. All the fields in this structure are private and should never be accessed directly.</p>
<div class="refsect3">
<a name="RygelMediaRendererClass.members"></a><h4>Members</h4>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="300px" class="struct_members_name">
<col class="struct_members_description">
<col width="200px" class="struct_members_annotations">
</colgroup>
<tbody></tbody>
</table></div>
</div>
</div>
</div>
<div class="refsect1">
<a name="RygelMediaRenderer.property-details"></a><h2>Property Details</h2>
<div class="refsect2">
<a name="RygelMediaRenderer--player"></a><h3>The <code class="literal">“player”</code> property</h3>
<pre class="programlisting">  “player”                   <a class="link" href="RygelMediaPlayer.html" title="RygelMediaPlayer"><span class="type">RygelMediaPlayer</span></a> *</pre>
<p>player.</p>
<p>Flags: Write / Construct Only</p>
</div>
</div>
</div>
<div class="footer">
<hr>Generated by GTK-Doc V1.28</div>
</body>
</html>